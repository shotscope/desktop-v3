import * as Actions from "../actions";
import WebAPIHandler from "./WebAPIHandler";
import * as util from "./util";
const SerialPort = require("serialport");

// See Sean's V2 Firmware USB API for commands and parameters
// V2 vendor ID
const SHOTSCOPE_VENDOR_ID = "0483";
// V1 vendor ID
const SHOTSCOPE_VENDOR_ID_LE = "0403";

export const PRODUCT_ID_V1 = "6015";
export const PRODUCT_ID_V2 = "5740";

// Packet headers for sending commands to the band
export const GET_PRODUCT_INFO = "$SSGPI";
export const GET_BATTERY_LEVEL = "$SSGBT";
export const GET_HAND = "$SSGHD";
export const GET_COURSE_IDS_COUNT = "$SSGAI";
export const GET_COURSE_IDS = "$SSGID";
export const GET_PERFORMANCE_ENTRIES_COUNT = "$SSGAE";
export const GET_PERFORMANCE_ENTRIES = "$SSGDE";
export const GET_TIMESTAMP = "$SSGTS";
export const GET_BUILD_INFO = "$SSNFO";
export const SET_HAND = "$SSSHD"; //"LEFT" or "RIGHT"
export const SET_DATE_TIME = "$SSSDT";
export const DELETE_PERFORMANCE_DATA = "$SSDEL";
export const BEGIN_FILE_TRANSFER = "$SSBGN";
export const FILE_TRANSFER_CHUNK = "$SSFIL";
export const END_FILE_TRANSFER = "$SSEND";
export const REBOOT = "$SSRBT";
export const DEFAULT_SETTINGS = "$SSDFS";

//Bands with preload capable firmware have some commands which accept an extra optional parameter
const PRELOADED_PARAMETER = "P";
const PRELOADED_COMMANDS = [
	GET_TIMESTAMP,
	GET_COURSE_IDS_COUNT,
	GET_COURSE_IDS
];

// Packet headers for data received from the band
const RECEIVED_PRODUCT_INFO = "$SSSPI";
const RECEIVED_BATTERY_LEVEL = "$SSSBT";
const RECEIVED_HAND = "$SSSHD";
const RECEIVED_COURSE_IDS_COUNT = "$SSSAI";
const RECEIVED_COURSE_IDS = "$SSSID";
const RECEIVED_PERFORMANCE_ENTRIES_COUNT = "$SSSAE";
const RECEIVED_PERFORMANCE_ENTRIES = "$SSSDE";
const RECEIVED_TIMESTAMP = "$SSSTS";
const RECEIVED_BUILD_INFO = "$SSSFO";
const RECEIVED_DEFAULT_SETTINGS = "$SSSFS";
const RECEIVED_SET_DATE_TIME = "$SSSDT";
const RECEIVED_SET_DATE_TIME_ERR = "$SSERR";
const RECEIVED_READY_FILE_TRANSFER = "$SSRDY";
const RECEIVED_END_FILE_TRANSFER = "$SSOVR";

// These commands share the same $SSWIN response and are handled based on the second argument from the band
const RECEIVED_SSWIN = "$SSWIN";
const RECEIVED_DELETE_PERFORMANCE = "DEL";
const RECEIVED_SET_HAND = "SHD";

//The transfer size is the size in bytes of packets to be sent to the band
const TRANSFER_FILE_SIZE = 64;
const TIMEOUT_MILLISECONDS = 5000;

SerialPort.parsers = {
	ByteLength: require("@serialport/parser-byte-length"),
	CCTalk: require("@serialport/parser-cctalk"),
	Delimiter: require("@serialport/parser-delimiter"),
	Readline: require("@serialport/parser-readline"),
	Ready: require("@serialport/parser-ready"),
	Regex: require("@serialport/parser-regex")
};
const Readline = SerialPort.parsers.Readline;
let port = null;
let parser = null;
let currentValue = [];
let currentCoursesValue = [];
let requestAttempts = 3;
let skipCheck = false;

export default class SerialHandler {
	static connectedPort = null;
	static discovering = false;
	static store = null;
	static temporaryEntries = [];
	static courseDataChunks = [];
	static fileType = -1;
	static unsubscribe = null;
	static unsubscribeCourses = null;
	static fileTransferAction = "";
	static coursesToBand = [];
	static coursesToBandLength = 0;
	static coursesToBandTotalDataSent = 0;
	static coursesToBandTotalDataLength = 0;
	static firmwareDataChunks = [];
	static firmwareDataLength = 0;
	static timeout;
	static courseIndex = null;
	static courseIndexDataChunks = [];
	static completionCallback;
	static progressCallback = null;
	static courseIDsIndex = 0;

	//Connect the SerialHandler class to the store
	static initializeStore(_store) {
		SerialHandler.store = _store;
	}

	// Initializes the band listener and performs actions depending on the data received, only initialize this ONCE
	static initializeListener() {
		parser = SerialHandler.connectedPort.pipe(
			new Readline({ delimiter: "\r\n" })
		);
		parser.on("data", response => {
			// Data fields are separated by commas and the checksum is prepended by an asterisk. A typical
			// packet can look like this: $HEADER,HEXDATA*HEXCHECKSUM
			const data = response.split(/[,*]/);
			console.log(data);
			switch (data[0]) {
				// Retrieved firmware version and serial no. strings from the band
				case RECEIVED_PRODUCT_INFO:
					const bandInfo = {
						firmwareVersion: data[1],
						serialNo: data[2]
					};
					SerialHandler.store.dispatch(Actions.setBandInfo(bandInfo));
					break;
				// Response for either performance data being deleted or the hand being set
				case RECEIVED_SSWIN:
					switch (data[1]) {
						// Successfully deleted performance data from the band
						case RECEIVED_DELETE_PERFORMANCE:
							SerialHandler.parseUART(GET_PERFORMANCE_ENTRIES_COUNT);
							break;
						// Response on successful set hand
						case RECEIVED_SET_HAND:
							SerialHandler.parseUART(GET_HAND);
							break;
					}
					break;
				// Retrieved the battery level as a hex value from the band
				case RECEIVED_BATTERY_LEVEL:
					var batteryLevel = SerialHandler.hexToDecimal(data[1]);
					SerialHandler.store.dispatch(Actions.setBatteryLevel(batteryLevel));
					break;
				// Retrieved index hex timestamp from the band. This command should only be used when you want to retrieve courses from the band.
				// The first 8 characters represent the preload index timestamp for preloaded bands (this is "00000000" on non-preload capable bands)
				// The second 8 characters represent the last course sync timestamp
				case RECEIVED_TIMESTAMP:
					console.log(data);
					SerialHandler.store.dispatch(
						Actions.setTimestamp([
							SerialHandler.hexToDecimal(data[1].slice(0, 8)),
							SerialHandler.hexToDecimal(data[1].slice(8))
						])
					);
					SerialHandler.parseUART(GET_COURSE_IDS_COUNT);
					break;
				// Retrieved hand string from the band (either LEFT or RIGHT)
				case RECEIVED_HAND:
					SerialHandler.store.dispatch(Actions.setHand(data[1]));
					break;
				// Retrieved course ID count i.e. number of courses available on the band. On non-preloaded bands this value is a hex number.
				// On preload capable bands this is represented by an array of hex numbers [preloaded_count, added_count, updated_count].
				// The total number of courses on the band is the preloaded count + added count
				case RECEIVED_COURSE_IDS_COUNT:
					console.log(
						"Course ID Count: " + SerialHandler.hexToDecimal(data[1])
					);
					if (data.length == 5)
						SerialHandler.store.dispatch(
							Actions.setCourseIDCount([
								SerialHandler.hexToDecimal(data[1]),
								SerialHandler.hexToDecimal(data[2]),
								SerialHandler.hexToDecimal(data[3])
							])
						);
					else {
						SerialHandler.store.dispatch(
							Actions.setCourseIDCount([SerialHandler.hexToDecimal(data[1])])
						);
					}
					break;
				// Retrieved the number of performance data lines on the band
				case RECEIVED_PERFORMANCE_ENTRIES_COUNT:
					console.log(
						"Performance Entries: " + SerialHandler.hexToDecimal(data[1])
					);
					SerialHandler.store.dispatch(
						Actions.setPerformanceEntriesCount(
							SerialHandler.hexToDecimal(data[1])
						)
					);
					break;
				// Retrieved a batch of (up to) 10 course IDs from the band at the given offset
				case RECEIVED_COURSE_IDS:
					var courseIDsArray = [];
					// Legacy course format 'date'
					if (SerialHandler.store.getState().bandReducer.pLTimestamp == 0) {
						for (let i = 1; i < data.length - 1; i++) {
							// Set the band last update date to 5 August 2097 as this date is not used in legacy bands
							courseIDsArray.push({
								id: SerialHandler.hexToDecimal(data[i]),
								lastUpdated: SerialHandler.hexToDecimal("F0000000")
							});
						}
					} else {
						// Preloaded course format '[ID, date]'
						for (let i = 1; i < data.length - 1; i = i + 2) {
							courseIDsArray.push({
								id: SerialHandler.hexToDecimal(data[i]),
								lastUpdated: SerialHandler.hexToDecimal(data[i + 1])
							});
						}
					}
					// Add the (up to) 10 course IDs to the list of course IDs in the store. Once the store is updated with these values,
					// the next 10 will be retrieved by the handleStoreCoursesUpdated method which is subscribed to changes to the store.bandReducer.courseIDs state
					SerialHandler.store.dispatch(
						Actions.setCourseIDs(
							SerialHandler.store
								.getState()
								.bandReducer.courseIDs.concat(courseIDsArray)
						)
					);

					break;
				// Retrieved a batch of (up to) 10 performance entry lines from the band at the given offset
				case RECEIVED_PERFORMANCE_ENTRIES:
					var performanceEntriesArray = [];
					for (let i = 1; i < data.length - 1; i++) {
						performanceEntriesArray.push({
							data: data[i],
							checksum: data[data.length - 1]
						});
					}
					var updatedPerformanceEntriesArray = SerialHandler.store
						.getState()
						.performanceReducer.performanceEntries.concat(
							performanceEntriesArray
						);
					// Add the (up to) 10 performance data lines to the list of performance entries in the store. Once the store is updated with these values,
					// the next 10 will be retrieved by the handleStoreEntriesUpdated method which is subscribed to changes to the store.performanceReducer.performanceEntries state
					SerialHandler.store.dispatch(
						Actions.setPerformanceEntries(updatedPerformanceEntriesArray)
					);
					break;
				// Response on successful set datetime
				case RECEIVED_SET_DATE_TIME:
					console.log("Successfully set date/time");
					break;
				// Response on failed to set datetime
				case RECEIVED_SET_DATE_TIME_ERR:
					console.log("Failed to set set date/time");
				// Response indicating band is ready to receive data
				case RECEIVED_READY_FILE_TRANSFER:
					// Stop the error timeout as the band is continuing to respond (this will be started again when data is sent to the band)

					if (SerialHandler.timeout.cleared == false) {
						SerialHandler.timeout.reset();

						// File types
						// 0: Course data
						// 1: Index file
						// 2: Firmware data
						switch (SerialHandler.fileType) {
							// Band ready to receive course data
							case 0:
								// If there is still data to send, send the next chunk and pop it off the array of course data to send
								if (SerialHandler.courseDataChunks.length > 0) {
									console.log(SerialHandler.courseDataChunks.length);
									SerialHandler.parseUART(
										FILE_TRANSFER_CHUNK,
										SerialHandler.courseDataChunks[
											SerialHandler.courseDataChunks.length - 1
										]
									);
									SerialHandler.progressCallback({
										percent:
											(SerialHandler.coursesToBandTotalDataSent /
												SerialHandler.coursesToBandTotalDataLength) *
											100,
										action:
											"Syncing courses: " +
											(SerialHandler.coursesToBand.length + 1)
									});
									SerialHandler.courseDataChunks.pop();
									SerialHandler.coursesToBandTotalDataSent += 1;
									// console.log(
									// 	SerialHandler.courseDataChunks.length + " CHUNKS REMAINING"
									// );
									// If there is no more data to send, tell the band we're done sending data
								} else SerialHandler.parseUART(END_FILE_TRANSFER);
								break;
							case 1:
								// If there is still data to send, send the next chunk and pop it off the array of index data to send
								console.log(SerialHandler.courseIndexDataChunks.length);
								if (SerialHandler.courseIndexDataChunks.length > 0) {
									SerialHandler.parseUART(
										FILE_TRANSFER_CHUNK,
										SerialHandler.courseIndexDataChunks[
											SerialHandler.courseIndexDataChunks.length - 1
										]
									);
									SerialHandler.courseIndexDataChunks.pop();
								} else SerialHandler.parseUART(END_FILE_TRANSFER);
								break;
							case 2:
								// If there is still data to send, send the next chunk and pop it off the array of firmware data to send
								if (SerialHandler.firmwareDataChunks.length > 0) {
									SerialHandler.parseUART(
										FILE_TRANSFER_CHUNK,
										SerialHandler.firmwareDataChunks[
											SerialHandler.firmwareDataChunks.length - 1
										]
									);
									SerialHandler.progressCallback({
										percent:
											100 -
											(SerialHandler.firmwareDataChunks.length /
												SerialHandler.firmwareDataLength) *
												100,
										action: "Updating Firmware"
									});
									SerialHandler.firmwareDataChunks.pop();
								} else SerialHandler.parseUART(END_FILE_TRANSFER);
								break;
						}
						break;
					}
				case RECEIVED_END_FILE_TRANSFER:
					console.log("TEST", SerialHandler.timeout.cleared);
					if (SerialHandler.timeout.cleared == false) {
						SerialHandler.timeout.reset();
						console.log("TRANSFER COMPLETE");
						switch (SerialHandler.fileType) {
							case 0:
								SerialHandler.loadCourses();
								break;
							case 1:
								SerialHandler.unsubscribeCourses();
								SerialHandler.getAllBandCourses();
								SerialHandler.progressCallback({
									percent: 0,
									action: "Course transfer complete"
								});
								setTimeout(function() {
									SerialHandler.progressCallback({
										percent: 0,
										action: ""
									});
								}, 5000);
								SerialHandler.break;
							case 2:
								SerialHandler.timeout.clear();
								SerialHandler.completionCallback(true);
								SerialHandler.fileType = -1;
								break;
							default:
								break;
						}
						break;
					} else {
						SerialHandler.completionCallback("CANCEL");
						SerialHandler.progressCallback({
							percent: 0,
							action: ""
						});
					}
				default:
					console.log("Unexpected packet: " + data);
					break;
			}
			// port.close();
		});
	}

	static setProgressCallback(callback) {
		SerialHandler.progressCallback = callback;
	}

	static cancel() {
		console.log("CANCELLED");
		SerialHandler.parseUART(END_FILE_TRANSFER);
		SerialHandler.timeout.clear();
		if (typeof SerialHandler.unsubscribeCourses == "function")
			SerialHandler.unsubscribeCourses();
		if (typeof SerialHandler.unsubscribe == "function")
			SerialHandler.unsubscribe();
		SerialHandler.completionCallback("CANCEL");
		SerialHandler.store.dispatch(Actions.clearPerformanceEntries());
	}

	//Sends a command to the band with optional data
	static parseUART(message, data = null, fileName = null, fileSize = null) {
		if (message == FILE_TRANSFER_CHUNK || message == BEGIN_FILE_TRANSFER) {
			SerialHandler.timeout.reset();
		}
		const port = SerialHandler.connectedPort;
		if (data == null) SerialHandler.SendPacket(message, port);
		else {
			if (fileName == null)
				SerialHandler.SendPacket(`${message},${data}`, port);
			else
				SerialHandler.SendPacket(
					`${message},${data},${fileName},${fileSize}`,
					port
				);
		}
	}

	static loadFirmware(firmwareData, completionCallback) {
		SerialHandler.fileType = 2;
		SerialHandler.completionCallback = completionCallback;
		var view = new Uint8Array(firmwareData);
		const buffer = view.buffer;
		const firmwareHexString = util.buf2hex(buffer).toUpperCase();
		const firmwareFileSize = util.getHexFileSize(firmwareHexString);
		var firmwareDataChunks = [];
		for (var i = 0; i < firmwareHexString.length; i = i + TRANSFER_FILE_SIZE) {
			firmwareDataChunks.push(firmwareHexString.substr(i, TRANSFER_FILE_SIZE));
		}
		SerialHandler.firmwareDataChunks = firmwareDataChunks.reverse();
		SerialHandler.firmwareDataLength = SerialHandler.firmwareDataChunks.length;
		SerialHandler.timeout = new util.Timeout(function() {
			SerialHandler.completionCallback(false);
		}, TIMEOUT_MILLISECONDS);
		console.log("STARTING FIRMWARE TRANSFER TO BAND");
		SerialHandler.parseUART(
			BEGIN_FILE_TRANSFER,
			"2",
			util.addHexPadding(SerialHandler.decimalToHex(2)),
			SerialHandler.decimalToHex(firmwareFileSize)
		);
	}

	// Loads courses with an index file to the band. The parameters are only used for initializing
	// the transfer. These values are stored in the SerialHandler so to get the next course the loadCourses
	// method can be called with no parameters.
	static loadCourses(courses = null, index = null, completionCallback = null) {
		//If an index file is being loaded, prepare the index file for transfer
		if (index != null) {
			SerialHandler.courseIndex = index;
			var courseIndexDataChunks = [];
			for (var i = 0; i < index.length; i = i + TRANSFER_FILE_SIZE) {
				courseIndexDataChunks.push(index.substr(i, TRANSFER_FILE_SIZE));
			}
			//Reverse the course index data chunks so they can be popped off when sending data to the band
			SerialHandler.courseIndexDataChunks = courseIndexDataChunks.reverse();
		}
		//If a courses file is being loaded, prepare the courses for transfer
		if (courses) {
			SerialHandler.timeout = new util.Timeout(function() {
				SerialHandler.completionCallback(false);
			}, TIMEOUT_MILLISECONDS);
			SerialHandler.coursesToBand = [];
			SerialHandler.coursesToBandTotalDataSent = 0;
			SerialHandler.coursesToBandTotalDataLength = 0;
			for (var course of courses) {
				SerialHandler.coursesToBandTotalDataLength +=
					course.courseData.length / TRANSFER_FILE_SIZE;
				if (course.courseData != null) SerialHandler.coursesToBand.push(course);
			}
			SerialHandler.coursesToBandLength = SerialHandler.coursesToBand.length;
		}
		//If a callback has been set for completion, set the callback
		if (completionCallback)
			SerialHandler.completionCallback = completionCallback;

		//If there are more than 0 courses left to transfer to the band, pop one off and start transferring it
		//Only the initial packet needs to be sent, the rest of the transfer is loaded by the serial listener after receiving an OK from the band
		if (SerialHandler.coursesToBand.length > 0) {
			var course = SerialHandler.coursesToBand.pop();
			var hexCourseID = SerialHandler.decimalToHex(course.courseID);
			var courseBytesSize = util.getHexFileSize(course.courseData.toString());
			var courseDataChunks = [];
			for (
				var i = 0;
				i < course.courseData.length;
				i = i + TRANSFER_FILE_SIZE
			) {
				courseDataChunks.push(course.courseData.substr(i, TRANSFER_FILE_SIZE));
			}
			//Reverse the course data chunks so they can be popped off when sending data to the band
			SerialHandler.courseDataChunks = courseDataChunks.reverse();
			SerialHandler.fileType = 0;
			console.log("STARTING COURSE TRANSFER TO BAND");
			SerialHandler.parseUART(
				BEGIN_FILE_TRANSFER,
				"0",
				util.addHexPadding(hexCourseID),
				SerialHandler.decimalToHex(courseBytesSize)
			);
		} else if (
			SerialHandler.courseIndex &&
			SerialHandler.store.getState().bandReducer.pLTimestamp == 0
		) {
			SerialHandler.fileType = 1;
			console.log("TRANSFERRING INDEX FILE");
			//File size and name does not matter here
			SerialHandler.parseUART(
				BEGIN_FILE_TRANSFER,
				"1",
				SerialHandler.decimalToHex(12345),
				SerialHandler.decimalToHex(
					util.getHexFileSize(SerialHandler.courseIndex)
				)
			);
		} else if (
			util.isHigherFirmwareVersion(
				"1.10.0",
				SerialHandler.store.getState().bandReducer.info.firmwareVersion
			) &&
			SerialHandler.store.getState().bandReducer.pLTimestamp > 0
		) {
			SerialHandler.fileType = -1;
			SerialHandler.unsubscribeCourses();
			SerialHandler.getAllBandCourses();
			SerialHandler.timeout.clear();
			SerialHandler.completionCallback(true);
			SerialHandler.progressCallback({
				percent: 0,
				action: "Course transfer complete"
			});
			setTimeout(function() {
				SerialHandler.progressCallback({
					percent: 0,
					action: ""
				});
			}, 5000);
		}
	}

	// Polls to check if the band has been connected
	static async discoverBand() {
		if (!SerialHandler.discovering) {
			SerialHandler.discovering = true;
			let bandPort;
			const discovery = setInterval(() => {
				console.log("Searching...");
				SerialPort.list((err, ports) => {
					ports.forEach(_port => {
						if (_port.vendorId != null) {
							if (
								_port.vendorId == SHOTSCOPE_VENDOR_ID ||
								_port.vendorId == SHOTSCOPE_VENDOR_ID_LE
							) {
								stopDiscovery(discovery);
								// parser = null;
								SerialHandler.connectedPort = null;
								port = _port;
								bandPort = port.comName.toString();
								SerialHandler.connectedPort = new SerialPort(
									bandPort.toString(),
									{
										baudRate: 115200
									}
								);
								SerialHandler.connectedPort.on("open", function() {
									var connected = false;
									var connectionInterval = setInterval(() => {
										console.log("checking connection");
										if (SerialHandler.parseUART(GET_BATTERY_LEVEL)) {
										} else {
											clearInterval(connectionInterval);
											console.log("connected");
											if (_port.productId == PRODUCT_ID_V2) {
												SerialHandler.getBandInfo();
												currentValue = SerialHandler.store.getState()
													.performanceReducer.performanceEntries;
												currentCoursesValue = SerialHandler.store.getState()
													.bandReducer.courseIDs;
												SerialHandler.store.dispatch(
													Actions.setConnected({
														connected: true,
														productId: _port.productId
													})
												);
											}
											SerialHandler.initializeListener();
										}
									}, 500);
								});

								SerialHandler.connectedPort.on("close", function() {
									console.log("disconnected");
									parser = null;
									SerialHandler.store.dispatch(
										Actions.setConnected({ connected: false })
									);
									SerialHandler.connectedPort = null;
									SerialHandler.discoverBand();
								});
							}
						}
					});
				});
			}, 500);
			function stopDiscovery(interval) {
				clearInterval(interval);
				SerialHandler.discovering = false;
			}
		}
	}

	// Gets basic data from the band when it is plugged in
	static getBandInfo() {
		Date.prototype.stdTimezoneOffset = function() {
			var jan = new Date(this.getFullYear(), 0, 1);
			var jul = new Date(this.getFullYear(), 6, 1);
			return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
		};

		Date.prototype.isDstObserved = function() {
			return this.getTimezoneOffset() < this.stdTimezoneOffset();
		};
		var currentDate = new Date(Date.now());
		var timezoneOffset = 0 - new Date().getTimezoneOffset() / 15;
		var DST = new Date().isDstObserved() ? "4" : "0";
		var currentDateString =
			util.addTimePadding(currentDate.getDate()).toString() +
			util.addTimePadding(currentDate.getMonth() + 1).toString() +
			currentDate
				.getFullYear()
				.toString()
				.substring(2, 4);
		var currentTimeString =
			util.addTimePadding(currentDate.getHours()).toString() +
			util.addTimePadding(currentDate.getMinutes()).toString() +
			util.addTimePadding(currentDate.getSeconds()).toString();
		var bandDateTime =
			currentDateString +
			"," +
			currentTimeString +
			"," +
			DST +
			"," +
			timezoneOffset;
		//End file transfer in case the band was unplugged during a transfer
		SerialHandler.parseUART(END_FILE_TRANSFER);
		SerialHandler.parseUART(GET_PRODUCT_INFO);
		SerialHandler.parseUART(GET_BATTERY_LEVEL);
		SerialHandler.store.getState().userReducer.token != ""
			? SerialHandler.store.getState().userReducer.leftHanded
				? SerialHandler.parseUART(SET_HAND, "LEFT")
				: SerialHandler.parseUART(SET_HAND, "RIGHT")
			: SerialHandler.parseUART(GET_HAND);
		SerialHandler.parseUART(SET_DATE_TIME, bandDateTime);
		SerialHandler.parseUART(GET_PERFORMANCE_ENTRIES_COUNT);
		SerialHandler.parseUART(GET_TIMESTAMP);
		SerialHandler.getAllBandCourses();
	}

	static async getAllBandEntries(completionCallback) {
		SerialHandler.completionCallback = completionCallback;
		SerialHandler.unsubscribe = SerialHandler.store.subscribe(
			SerialHandler.handleStoreEntriesUpdated
		);
		SerialHandler.timeout = new util.Timeout(function() {
			SerialHandler.completionCallback(false);
		}, TIMEOUT_MILLISECONDS);
		SerialHandler.parseUART(GET_PERFORMANCE_ENTRIES_COUNT);
		// We only need to request the first 10 entries (0-9) as handleStoreEntriesUpdated will be called
		// on a successful response to retrieve the rest of the entries
		SerialHandler.parseUART(
			GET_PERFORMANCE_ENTRIES,
			"0".toString(16).toUpperCase()
		);

		return true;
	}

	static async getPerformanceChunkString(_data) {
		var newEntries = "$SSSDE";
		var i = _data.length % 10;
		if (i == 0) i = 10;
		//Concatenate the newest (up to) 10 data entries to a single string
		for (i; i > 0; i--) {
			newEntries += _data[_data.length - i].data.toString();
		}
		return newEntries;
	}

	static async getAllBandCourses() {
		SerialHandler.unsubscribeCourses = SerialHandler.store.subscribe(
			SerialHandler.handleStoreCoursesUpdated
		);
		SerialHandler.store.dispatch(Actions.setCourseIDs([]));
		SerialHandler.parseUART(GET_COURSE_IDS_COUNT);
		if (SerialHandler.store.getState().bandReducer.courseIDs.length == 0) {
			SerialHandler.parseUART(GET_COURSE_IDS_COUNT);
			// We only need to request the first 10 entries (0-9) as handleStoreCoursesUpdated will be called
			// on a successful response to retrieve the rest of the courses
			SerialHandler.parseUART(GET_COURSE_IDS, "0".toString(16).toUpperCase());
		}
	}

	static async handleStoreCoursesUpdated() {
		//Subscribe the current performanceEntries redux state to currentCoursesValue
		let previousValue = currentCoursesValue.slice();
		currentCoursesValue = SerialHandler.store.getState().bandReducer.courseIDs;
		if (
			currentCoursesValue !== undefined &&
			currentCoursesValue.length != 0 &&
			currentCoursesValue !== previousValue
		) {
			SerialHandler.courseIDsIndex += 10;
			currentCoursesValue = SerialHandler.store.getState().bandReducer
				.courseIDs;
			// If we don't have all the courses from the band, get the next 10 from the current index
			if (
				SerialHandler.courseIDsIndex <
				SerialHandler.store.getState().bandReducer.courseIDsCount
			) {
				console.log(
					currentCoursesValue,
					SerialHandler.store.getState().bandReducer.courseIDsCount
				);
				SerialHandler.parseUART(
					GET_COURSE_IDS,
					SerialHandler.courseIDsIndex.toString(16).toUpperCase()
				);
			}
		}
	}

	// When data is received (called when the performanceEntries state is updated in the store) validates the data and
	// requests failed data again. Sequentially requests the data chunks one at a time until the end of the data then
	// uploads the data to the server and deletes the data off the band on successful response.
	static async handleStoreEntriesUpdated() {
		//Subscribe the current performanceEntries redux state to currentValue
		let previousValue = currentValue.slice();
		currentValue = SerialHandler.store.getState().performanceReducer
			.performanceEntries;
		// Callback to update the progress bar
		if (
			currentValue !== undefined &&
			currentValue.length !== 0 &&
			currentValue.toString() !== previousValue.toString()
		) {
			if (!SerialHandler.timeout.cleared) {
				SerialHandler.progressCallback({
					percent:
						(currentValue.length /
							SerialHandler.store.getState().performanceReducer
								.performanceEntriesCount) *
						100,
					action: "Syncing Data"
				});
				//Validate the newest (up to) 10 data entries using the checksum, request the data again if it is invalid
				var newEntries = await SerialHandler.getPerformanceChunkString(
					SerialHandler.store.getState().performanceReducer.performanceEntries
				);
				//Calculate the checksum on the string and compare it to the checksum given from the received data
				if (
					SerialHandler.hexToDecimal(
						currentValue[currentValue.length - 1].checksum
					) != SerialHandler.CalculateCkSum(newEntries) &&
					requestAttempts >= 0 &&
					!skipCheck
				) {
					console.log(
						"CHECKSUM MISMATCH AT OFFSET " +
							(currentValue.length - (currentValue.length % 10)) +
							", ACTUAL: " +
							SerialHandler.hexToDecimal(
								currentValue[currentValue.length - 1].checksum
							) +
							" EXPECTED: " +
							SerialHandler.CalculateCkSum(newEntries) +
							"\nDISCARDING CHUNK AND RE-REQUESTING. REMAINING ATTEMPTS: " +
							requestAttempts
					);
					//Discard the last chunk received so it can be requested again
					SerialHandler.store.dispatch(
						Actions.discardPerformanceEntriesChunk()
					);
					requestAttempts -= 1;
					//If no more attempts remain for the chunk, stop retrying to get the same chunk to prevent hanging
					if (requestAttempts == 0) skipCheck = true;
					SerialHandler.parseUART(
						GET_PERFORMANCE_ENTRIES,
						SerialHandler.store
							.getState()
							.performanceReducer.performanceEntries.length.toString(16)
							.toUpperCase()
					);
				} else {
					if (SerialHandler.timeout) SerialHandler.timeout.reset();
					console.log("OK");
					//If the performanceEntries state count is equal to the number of performance entries on the band, upload the data
					if (
						SerialHandler.store.getState().performanceReducer.performanceEntries
							.length >=
						SerialHandler.store.getState().performanceReducer
							.performanceEntriesCount
					) {
						if (
							SerialHandler.store.getState().performanceReducer
								.performanceEntries.length > 10
						) {
							SerialHandler.unsubscribe();
							SerialHandler.timeout.clear();
							console.log("BAND UPLOAD COMPLETE");
							var uploadEntries = [];
							for (var entry of SerialHandler.store.getState()
								.performanceReducer.performanceEntries) {
								uploadEntries.push(entry.data);
							}
							var response = await WebAPIHandler.uploadPerformanceEntries(
								uploadEntries
							);
							//If the upload is successful, delete the performance entries on the band
							if (response.status == 200) {
								SerialHandler.completionCallback(true);
								SerialHandler.parseUART(DELETE_PERFORMANCE_DATA);
								SerialHandler.store.dispatch(Actions.clearPerformanceEntries());
							} else if (response.status == 202) {
								SerialHandler.completionCallback(202);
								SerialHandler.parseUART(DELETE_PERFORMANCE_DATA);
								SerialHandler.store.dispatch(Actions.clearPerformanceEntries());
							} else if (response == false)
								SerialHandler.completionCallback(false);
						} else {
							SerialHandler.parseUART(DELETE_PERFORMANCE_DATA);
							SerialHandler.completionCallback("INVALID_ROUND");
						}
					} else {
						// else continue requesting data
						if (previousValue.length < currentValue.length) {
							console.log(
								SerialHandler.store.getState().performanceReducer
									.performanceEntries.length
							);
							SerialHandler.parseUART(
								GET_PERFORMANCE_ENTRIES,
								SerialHandler.decimalToHex(
									SerialHandler.store.getState().performanceReducer
										.performanceEntries.length
								)
							);

							SerialHandler.timeout.reset();
							if (skipCheck) {
								console.log("SKIPPED CHECKSUM VALIDATION");
								skipCheck = false;
								requestAttempts = 3;
							}
						}
					}
				}
			} else {
				SerialHandler.completionCallback("CANCEL");
			}
		}
	}

	static CalculateCkSum(_data) {
		var data = _data.slice();
		let cksum = 0;
		for (let i = 1; i < data.length; i++) cksum ^= data.charCodeAt(i);
		return cksum;
	}

	static decimalToHex(number) {
		return number.toString(16).toUpperCase();
	}

	static hexToDecimal(hex) {
		return parseInt(`0x${hex}`);
	}

	static async SendPacket(data, port) {
		const packetsArray = [];
		// Append the preloaded parameter if the command is in the list of preloaded commands
		if (PRELOADED_COMMANDS.indexOf(data.slice(0, 6)) != -1) {
			data += "," + PRELOADED_PARAMETER;
		}
		packetsArray.push(data);

		console.log(packetsArray);
		const cksum = SerialHandler.CalculateCkSum(data);
		if (packetsArray.length == 1) {
			try {
				port.write(
					`${packetsArray[0]}*${SerialHandler.decimalToHex(
						Math.floor(cksum / 16)
					)}${SerialHandler.decimalToHex(cksum % 16)}\r\n`
				);
			} catch (err) {
				console.log("Error. Can't communicate with serial: ", err.message);
			}
		}
	}
}
