"use strict";
const creds = require("../config/config");
var mandrill = require("../../js/mandrill.min.js");
// var mandrill_client = new mandrill.Mandrill('YOUR_API_KEY');

export default class Mailer {
	static mailer = new mandrill.mandrill.Mandrill("EdTLSk7tdHvr7uuyxojHZA"); // This will be public
	static requestCourse(courseObj) {
		Mailer.mailer.messages.send({
			message: {
				from_email: "courses@shotscope.com",
				from_name: "Course Request (Desktop)",
				to: [
					{ email: "alistairmillar@shotscope.com", name: "Alistair Millar" }
				], // Array of recipients
				subject: `New course request: ${courseObj.requestCourseName}`,
				text: `Course Name: ${courseObj.requestCourseName}\nCourse URL: ${
					courseObj.requestCourseURL
				}\nCourse City: ${
					courseObj.requestCourseCity
				}\nCourse State/Province: ${courseObj.requestCourseState}\nHoles: ${
					courseObj.requestCourseHoles
				}\n\nRequested by: ${courseObj.userEmail}` // Alternatively, use the "html" key to send HTML emails rather than plaintext
			}
		});
		return;
	}
}
