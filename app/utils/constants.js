//Max file size in bytes
export var MAX_PROFILE_IMAGE_SIZE = 100000;
export var MAX_PROFILE_IMAGE_HEIGHT = 256;

export var months = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];

export var handicaps = [
	{
		name: "FiftyFour",
		value: -54
	},
	{
		name: "FiftyThree",
		value: -53
	},
	{
		name: "FiftyTwo",
		value: -52
	},
	{
		name: "FiftyOne",
		value: -51
	},
	{
		name: "Fifty",
		value: -50
	},
	{
		name: "FortyNine",
		value: -49
	},
	{
		name: "FortyEight",
		value: -48
	},
	{
		name: "FortySeven",
		value: -47
	},
	{
		name: "FortySix",
		value: -46
	},
	{
		name: "FortyFive",
		value: -45
	},
	{
		name: "FortyFour",
		value: -44
	},
	{
		name: "FortyThree",
		value: -43
	},
	{
		name: "FortyTwo",
		value: -42
	},
	{
		name: "FortyOne",
		value: -41
	},
	{
		name: "Forty",
		value: -40
	},
	{
		name: "ThirtyNine",
		value: -39
	},
	{
		name: "ThirtyEight",
		value: -38
	},
	{
		name: "ThirtySeven",
		value: -37
	},
	{
		name: "ThirtySix",
		value: -36
	},
	{
		name: "ThirtyFive",
		value: -35
	},
	{
		name: "ThirtyFour",
		value: -34
	},
	{
		name: "ThirtyThree",
		value: -33
	},
	{
		name: "ThirtyTwo",
		value: -32
	},
	{
		name: "ThirtyOne",
		value: -31
	},
	{
		name: "Thirty",
		value: -30
	},
	{
		name: "TwentyNine",
		value: -29
	},
	{
		name: "TwentyEight",
		value: -28
	},
	{
		name: "TwentySeven",
		value: -27
	},
	{
		name: "TwentySix",
		value: -26
	},
	{
		name: "TwentyFive",
		value: -25
	},
	{
		name: "TwentyFour",
		value: -24
	},
	{
		name: "TwentyThree",
		value: -23
	},
	{
		name: "TwentyTwo",
		value: -22
	},
	{
		name: "TwentyOne",
		value: -21
	},
	{
		name: "Twenty",
		value: -20
	},
	{
		name: "Nineteen",
		value: -19
	},
	{
		name: "Eighteen",
		value: -18
	},
	{
		name: "Seventeen",
		value: -17
	},
	{
		name: "Sixteen",
		value: -16
	},
	{
		name: "Fifthteen",
		value: -15
	},
	{
		name: "Fourteen",
		value: -14
	},
	{
		name: "Thirteen",
		value: -13
	},
	{
		name: "Twelve",
		value: -12
	},
	{
		name: "Eleven",
		value: -11
	},
	{
		name: "Ten",
		value: -10
	},
	{
		name: "Nine",
		value: -9
	},
	{
		name: "Eight",
		value: -8
	},
	{
		name: "Seven",
		value: -7
	},
	{
		name: "Six",
		value: -6
	},
	{
		name: "Five",
		value: -5
	},
	{
		name: "Four",
		value: -4
	},
	{
		name: "Three",
		value: -3
	},
	{
		name: "Two",
		value: -2
	},
	{
		name: "One",
		value: -1
	},
	{
		name: "Nought",
		value: 0
	},
	{
		name: "PlusOne",
		value: 1
	},
	{
		name: "PlusTwo",
		value: 2
	},
	{
		name: "PlusThree",
		value: 3
	},
	{
		name: "PlusFour",
		value: 4
	},
	{
		name: "PlusFive",
		value: 5
	},
	{
		name: "PlusSix",
		value: 6
	},
	{
		name: "PlusSeven",
		value: 7
	}
];
