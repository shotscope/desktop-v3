import * as Actions from "../actions";
import {
	isHigherFirmwareVersion,
	addTimePadding,
	sanitizeCommas
} from "../utils/util";
import { months } from "../utils/constants";
import { PRODUCT_ID_V2 } from "./SerialHandler";
import packageJson from "../../package";
const os = require("os");

const prod = "https://dashboard.shotscope.com/";
const staging = "https://staging-dashboard.shotscope.com/";
const bob = "http://192.168.0.34:44303/";
const betaFWLocation = "https://shotscope.com/automatedAccess/firmwareBETA/";
const fwLocation = "https://shotscope.com/automatedAccess/firmware/";
const fwVersion = "v2fwVersion.txt";

export const url = prod;

export default class WebAPIHandler {
	static store = null;

	static initializeStore(_store) {
		WebAPIHandler.store = _store;
	}

	static async getUser() {
		return new Promise((resolve, reject) => {
			fetch(`${url}api/v3/settings/player`, {
				method: "GET",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: `Bearer ${
						WebAPIHandler.store.getState().userReducer.token
					}`
				}
			})
				.then(response => {
					if (response.status != 200)
						WebAPIHandler.store.dispatch(Actions.logout());
					else return response.json();
				})
				.then(responseJSON => {
					console.log(responseJSON);
					WebAPIHandler.store.dispatch(Actions.setUserDetails(responseJSON));
					resolve(true);
				})
				.catch(error => {
					console.error(error);
					reject(error);
				});
		});
	}

	static async updateUser(userObj) {
		console.log(JSON.stringify({ userObj }));
		try {
			const request = fetch(`${url}api/settings/player/v4`, {
				method: "POST",
				headers: {
					Accept: "application/JSON",
					"Content-Type": "application/JSON",
					Authorization: `Bearer ${
						WebAPIHandler.store.getState().userReducer.token
					}`
				},
				body: JSON.stringify(userObj)
			});
			return request
				.then(response => {
					console.log(response);
					return response;
				})
				.catch(error => {
					console.error(error);
					return false;
				});
		} catch (error) {
			console.error(error);
			return false;
		}
	}

	static async getFirmwareVersion() {
		var getFwVersion;
		if (WebAPIHandler.store.getState().userReducer.betaAccess)
			getFwVersion = betaFWLocation + fwVersion;
		else getFwVersion = fwLocation + fwVersion;
		return fetch(`${getFwVersion}`, {
			method: "GET",
			headers: {
				Accept: "text/plain",
				"Content-Type": "text/plain",
				"Cache-Control": "no-cache"
			}
		})
			.then(response => response.text())
			.then(response => {
				WebAPIHandler.store.dispatch(
					Actions.setServerFirmwareVersion(response.split("\n")[0])
				);
				return true;
			})
			.catch(error => {
				console.error(error);
				return false;
			});
	}

	static async getFirmware(versionNumber) {
		var firmwareBin = null;
		var firmwareLocation;
		if (WebAPIHandler.store.getState().userReducer.betaAccess)
			firmwareLocation = betaFWLocation;
		else firmwareLocation = fwLocation;
		return fetch(`${firmwareLocation}v2fw.${versionNumber}.bin`, {
			method: "GET",
			headers: {
				Accept: "application/octet-stream",
				"Content-Type": "application/octet-stream"
			}
		})
			.then(response => response.blob())
			.then(response => {
				var reader = new FileReader();
				return new Promise((resolve, reject) => {
					reader.addEventListener("loadend", function() {
						resolve(reader.result);
					});
					reader.readAsArrayBuffer(response);
				});
			})
			.catch(error => {
				console.error(error);
			});
	}

	static async getNearbyCourses(courseID) {
		return new Promise((resolve, reject) => {
			fetch(
				`${url}api/v2/courses/availablecourses/nearcourse?courseID=${courseID}`,
				{
					method: "GET",
					headers: {
						Accept: "application/json",
						"Content-Type": "application/json",
						Authorization: `Bearer ${
							WebAPIHandler.store.getState().userReducer.token
						}`
					}
				}
			)
				.then(response => {
					return response.json();
				})
				.then(response => {
					console.log(response);
					resolve(response);
				})
				.catch(error => {
					console.error(error);
					reject(response);
				});
		});
	}

	static async getAllCourses() {
		return new Promise((resolve, reject) => {
			var lastSyncTimestamp = WebAPIHandler.store.getState().persistentReducer
				.lastCourseSyncDateTime;
			var tempDate = new Date(lastSyncTimestamp);
			var date = new Date(tempDate.getTime() + 1000);
			var dateString = date.toISOString();
			var dateNow = Date.now();
			console.log(dateString);
			return fetch(
				`${url}api/v2/courses/availablecourses/completev2?modifiedSince=${dateString}`,
				{
					method: "GET",
					headers: {
						Accept: "application/json",
						"Content-Type": "application/json"
					}
				}
			)
				.then(response => {
					console.log("hello");
					return response.json();
				})
				.then(responseJSON => {
					console.log(responseJSON);
					WebAPIHandler.store.dispatch(Actions.setAllCoursesList(responseJSON));
					// If this is the first time getting courses, also set the countries and states
					if (lastSyncTimestamp == 0) {
						var countriesList = [];
						var statesList = [];
						for (var course of responseJSON) {
							if (countriesList.indexOf(course.country) === -1) {
								countriesList.push(course.country);
							}
							if (course.country == "United States")
								if (statesList.indexOf(course.state) === -1) {
									statesList.push(course.state);
								}
						}
						WebAPIHandler.store.dispatch(
							Actions.setAllCountriesList(countriesList.sort())
						);
						WebAPIHandler.store.dispatch(
							Actions.setUSStates(statesList.sort())
						);
					}
					console.log("hello");
					WebAPIHandler.store.dispatch(
						Actions.setLastCourseSyncDateTime(dateNow)
					);
					resolve(true);
				})

				.catch(error => {
					console.error(error);
					reject(error);
				});
		});
	}

	static async getAllNationalities() {
		return new Promise((resolve, reject) => {
			if (
				WebAPIHandler.store.getState().persistentReducer.nationalities.length ==
				0
			) {
				return fetch(`${url}api/v2/settings/nationalities`, {
					method: "GET",
					headers: {
						Accept: "application/json",
						"Content-Type": "application/json"
					}
				})
					.then(response => response.json())
					.then(responseJSON => {
						console.log(responseJSON);
						return new Promise((resolve, reject) => {
							WebAPIHandler.store.dispatch(
								Actions.setAllNationalitiesList(responseJSON)
							);
							resolve(true);
						});
					})

					.catch(error => {
						console.error(error);
						reject(error);
					});
			} else resolve(true);
		});
	}

	static async getUserCourses() {
		return new Promise((resolve, reject) => {
			var bandTimestamp = WebAPIHandler.store.getState().bandReducer.timestamp;

			var date = new Date(bandTimestamp * 1000 + 1000);
			var dateString = date.toISOString();

			var fetchURL = `${url}api/v2/courses/userscourses/sync?modifiedSince=${dateString}`;
			if (
				isHigherFirmwareVersion(
					"1.10.0",
					WebAPIHandler.store.getState().bandReducer.info.firmwareVersion
				)
			)
				fetchURL = `${url}api/v2/courses/userscourses/sync?modifiedSince=${dateString}&version=1`;
			console.log(fetchURL);

			return fetch(`${fetchURL}`, {
				method: "GET",
				headers: {
					Accept: "application/json",
					Authorization: `Bearer ${
						WebAPIHandler.store.getState().userReducer.token
					}`
				}
			})
				.then(response => {
					return response.json();
				})
				.then(responseJSON => {
					// TODO continue here
					console.log(responseJSON);
					var downloadedCourses = [];
					for (var course of responseJSON.userCourses) {
						if (course.courseData) {
							downloadedCourses.push(course);
						}
					}
					var storeObject = {
						downloadedCoursesIndex: responseJSON.index,
						downloadedCourses: downloadedCourses
					};
					WebAPIHandler.store.dispatch(Actions.setUserCourses(storeObject));
					resolve(true);
				})

				.catch(error => {
					console.error(error);
					reject(error);
				});
		});
	}

	static async getUserCoursesLite() {
		return new Promise((resolve, reject) => {
			var modifiedSince = "2100-01-01T00:00:01.000Z";
			return fetch(
				`${url}api/v2/courses/userscourses/sync?modifiedSince=${modifiedSince}`,
				{
					method: "GET",
					headers: {
						Accept: "application/json",
						Authorization: `Bearer ${
							WebAPIHandler.store.getState().userReducer.token
						}`
					}
				}
			)
				.then(response => response.json())
				.then(responseJSON => {
					console.log(responseJSON);
					WebAPIHandler.store.dispatch(
						Actions.setUserCoursesLite(responseJSON.userCourses)
					);
					resolve(true);
				})

				.catch(error => {
					console.error(error);
					reject(error);
				});
		});
	}

	static async addCourse(courseID) {
		const request = fetch(
			`${url}api/v2/courses/userscourses/add?courseID=${courseID}`,
			{
				method: "POST",
				headers: {
					Accept: "text/plain; charset=utf-8",
					Authorization: `Bearer ${
						WebAPIHandler.store.getState().userReducer.token
					}`
				}
			}
		);
		return request
			.then(response => {
				console.log(response);
				return response;
			})
			.catch(error => {
				console.error(error);
			});
	}

	static async deleteCourse(courseID) {
		console.log(WebAPIHandler.store.getState());
		const request = fetch(
			`${url}api/v2/courses/userscourses/delete?courseID=${courseID}`,
			{
				method: "POST",
				headers: {
					Accept: "text/plain; charset=utf-8",
					Authorization: `Bearer ${
						WebAPIHandler.store.getState().userReducer.token
					}`
				}
			}
		);
		return request
			.then(response => {
				console.log(response);
				return response;
			})
			.catch(error => {
				console.error(error);
			});
	}

	static async uploadPerformanceEntries() {
		var bandState = WebAPIHandler.store.getState().bandReducer;
		var userState = WebAPIHandler.store.getState().userReducer;
		var appState = WebAPIHandler.store.getState().dataReducer;
		var model = "UNKNOWN";
		var platform = "UNKNOWN";
		var preloaded = "N";
		var betaApp = "N";
		var hand = "R";
		var tempDate = new Date();
		var timezoneOffset = (tempDate.getTimezoneOffset() / 60).toString();
		let data = WebAPIHandler.store.getState().performanceReducer
			.performanceEntries;
		var dataString = "";

		if (os.platform() == "win32") {
			platform = "W_DESKTOP";
		} else if (os.platform() == "darwin") {
			platform = "M_DESKTOP";
		}
		if (bandState.productId == PRODUCT_ID_V2) model = "V2";
		if (bandState.pLTimestamp > 0) preloaded = "Y";
		if (userState.betaAccess) betaApp = "Y";
		if (bandState.hand == "LEFT") hand = "L";

		dataString +=
			"## APP_T: " +
			platform +
			", APP_V: DV3." +
			packageJson.version +
			", APP_BETA: " +
			betaApp +
			", APP_OS: " +
			os.release() +
			", APP_HW: placeholder hardware" +
			", APP_TZ: " +
			timezoneOffset +
			", SS_SERIAL: " +
			bandState.info.serialNo +
			", SS_FW: " +
			bandState.info.firmwareVersion +
			", SS_PL: " +
			preloaded +
			", SS_BATT: " +
			bandState.batteryLevel +
			"%, SS_HAND: " +
			hand +
			", SS_MODEL: " +
			model +
			"\n";

		for (const item of data) {
			dataString += item.data + "\n";
		}
		const request = fetch(`${url}api/Mobile`, {
			method: "POST",
			headers: {
				Accept: "application/JSON",
				"Content-Type": "application/JSON",
				Authorization: `Bearer ${
					WebAPIHandler.store.getState().userReducer.token
				}`
			},
			body: JSON.stringify({ data: dataString })
		});
		return request
			.then(response => {
				console.log(response);
				return response;
			})
			.catch(error => {
				console.error(error);
				return false;
			});
	}

	static async login(email, _password) {
		const details = {
			grant_type: "password",
			username: email,
			password: _password
		};
		const formBody = Object.keys(details)
			.map(
				key => `${encodeURIComponent(key)}=${encodeURIComponent(details[key])}`
			)
			.join("&");
		try {
			const request = fetch(`${url}Token`, {
				method: "POST",
				headers: {
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
				},
				body: formBody
			});
			return request
				.then(response => {
					console.log(response);
					if (response.status == 200) return response.json();
					else return false;
				})
				.then(jsonResponse => {
					console.log(jsonResponse);
					if (jsonResponse.access_token) {
						WebAPIHandler.store.dispatch(
							Actions.setToken(jsonResponse.access_token)
						);
						return true;
					} else return false;
				})
				.catch(error => {
					console.error(error);
					return false;
				});
		} catch (error) {
			return false;
		}
	}

	static async registerAndValidate(registerObj) {
		const request = fetch(
			`${url}api/v2/Authentication/RegisterAndValidatePlayerV2`,
			{
				method: "POST",
				headers: {
					Accept: "application/JSON",
					"Content-Type": "application/JSON"
				},
				body: JSON.stringify(registerObj)
			}
		);
		return request
			.then(response => {
				console.log(response);
				return response;
			})
			.catch(error => {
				console.error(error);
				return false;
			});
	}

	static async logout() {
		const request = fetch(`${url}api/Authentication/Logout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
				Authorization: `Bearer ${
					WebAPIHandler.store.getState().userReducer.token
				}`
			}
		});
		return request
			.then(response => WebAPIHandler.store.dispatch(Actions.logout()))
			.catch(error => {
				console.error(error);
			});
	}
}
