//Returns the file size of a hex string in Bytes
export function getHexFileSize(hexString) {
	return hexString.length / 2;
}

//check if a string follows the name@domain.x format
export function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

export function getNationalityFromID(nationalitiesList, nationalityID) {
	if (nationalitiesList != undefined && nationalityID > -1)
		for (var nationalityObj of nationalitiesList) {
			if (nationalityObj.nationality == nationalityID)
				return nationalityObj.name;
		}
}

// Removes commas from a string
export function sanitizeCommas(string) {
	return string.replace(/[,]+/g, "").trim();
}

// Executes fn after interval milliseconds and can be checked to see if is cleared or not
export function Timeout(fn, interval) {
	var id = setTimeout(fn, interval);
	this.cleared = false;
	this.clear = function() {
		this.cleared = true;
		clearTimeout(id);
	};
	this.reset = function() {
		clearTimeout(id);
		id = setTimeout(fn, interval);
	};
}

export function compressToLimit(image) {
	console.log(image);
	//Rough approximation of file size, don't need to be exact
	var fileSize = image.bitmap.data.length;
	if (fileSize > 100000) return (100000 / fileSize) * 100;
	else return 100;
}

export function getNationalityIndexFromID(nationalitiesList, nationalityID) {
	if (nationalitiesList != undefined && nationalityID > -1)
		for (var nationalityObj of nationalitiesList) {
			if (nationalityObj.nationality == nationalityID)
				return nationalitiesList.indexOf(nationalityObj);
		}
}

// Checks if a version of format X.X.X is higher than another
export function isHigherFirmwareVersion(lower, higher) {
	var version1 = lower.toString().split(".");
	var version2 = higher.toString().split(".");
	if (parseInt(version1[0]) < parseInt(version2[0])) return true;

	if (
		parseInt(version1[0]) <= parseInt(version2[0]) &&
		parseInt(version1[1]) < parseInt(version2[1])
	)
		return true;
	if (
		parseInt(version1[0]) <= parseInt(version2[0]) &&
		parseInt(version1[1]) <= parseInt(version2[1]) &&
		parseInt(version1[2]) < parseInt(version2[2])
	)
		return true;
	return false;
}

//Appends 0s to the end of a string until it has 4 characters length
export function addHexPadding(hexString) {
	if (hexString.length < 4) {
		var padding = 4 - hexString.length;
		var paddedString = "";
		for (padding; padding > 0; padding--) {
			paddedString = paddedString.concat("0");
		}
		paddedString = paddedString.concat(hexString);
		return paddedString;
	} else return hexString;
}

// Prepends a 0 to string if its length is <2
export function addTimePadding(timeUnit) {
	if (timeUnit.toString().length < 2) {
		return "0" + timeUnit.toString();
	} else return timeUnit.toString();
}

// Converts a Uint8Array to hex
export function buf2hex(buffer) {
	// buffer is an ArrayBuffer
	return Array.prototype.map
		.call(new Uint8Array(buffer), x => ("00" + x.toString(16)).slice(-2))
		.join("");
}

export function addDatePadding(value) {
	if (value.length < 2) {
		var padding = 2 - value.length;
		var paddedString = "";
		for (padding; padding > 0; padding--) {
			paddedString = paddedString.concat("0");
		}
		paddedString = paddedString.concat(value);
		return paddedString;
	} else return value;
}
