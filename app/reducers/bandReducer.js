import {
	SET_BAND_INFO,
	SET_BATTERY_LEVEL,
	SET_HAND,
	SET_COURSE_IDS_COUNT,
	SET_COURSE_IDS,
	SET_TIMESTAMP,
	SET_PORT,
	SET_CONNECTED,
	SET_BAND_PL_COURSES
} from "../actions/index";

const initialBandState = {
	connected: false,
	info: {
		firmwareVersion: "",
		serialNo: ""
	},
	batteryLevel: -1,
	hand: "",
	courseIDsCount: 0,
	totalCourseIDsCount: 0,
	courseIDs: [],
	productId: "",
	timestamp: 0,
	pLTimestamp: 0,
	updatedCourseCount: -1,
	pLCourses: []
};

const bandReducer = (state = initialBandState, action) => {
	switch (action.type) {
		case SET_CONNECTED:
			if (action.data.connected == false) {
				return initialBandState;
			}
			return {
				...state,
				connected: true,
				productId: action.data.productId
			};
			break;
		case SET_BAND_INFO:
			return {
				...state,
				info: {
					firmwareVersion: action.data.firmwareVersion,
					serialNo: action.data.serialNo
				}
			};
			break;
		case SET_BAND_PL_COURSES:
			return {
				...state,
				pLCourses: action.data
			};
			break;
		case SET_BATTERY_LEVEL:
			if (state.batteryLevel != action.data)
				return {
					...state,
					batteryLevel: action.data
				};
			break;
		case SET_HAND:
			return {
				...state,
				hand: action.data
			};
			return state;
			break;
		case SET_COURSE_IDS_COUNT:
			console.log(action.data);
			if (action.data.length == 1)
				return {
					...state,
					courseIDsCount: action.data[0]
				};
			else
				return {
					...state,
					courseIDsCount: action.data[1] + action.data[2],
					totalCourseIDsCount: action.data[0] + action.data[1],
					updatedCourseCount: action.data[2]
				};
			break;
		case SET_COURSE_IDS:
			return {
				...state,
				courseIDs: action.data
			};
			break;
		case SET_TIMESTAMP:
			return {
				...state,
				pLTimestamp: action.data[0],
				timestamp: action.data[1]
			};
		default:
			return state;
	}
};

export default bandReducer;
