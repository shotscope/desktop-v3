import {
	COURSES_AVAILABLE,
	COUNTRIES_AVAILABLE,
	SET_LAST_COURSE_SYNC_DATE_TIME,
	SET_US_STATES,
	NATIONALITIES_AVAILABLE
} from "../actions/index";

const dataState = {
	courses: [],
	lastCourseSyncDateTime: 0,
	countries: [],
	USStates: [],
	coursesInitialized: false,
	nationalities: []
};

//This reducer stores data persistently between sessions (along with the userReducer).
//Do not exceed the storage quota of 50MB or the persistent store will be dumped with no error or warning.
//As of writing the store size is ~8MB so you can persist whatever as long as it's not course data, firmware data etc.

const persistentReducer = (state = dataState, action) => {
	switch (action.type) {
		case COURSES_AVAILABLE:
			if (action.data.length > 0) {
				if (state.coursesInitialized) {
					var newState = state.courses.slice(0);
					var notFound = true;
					newDataLoop: for (var newCourse of action.data) {
						notFound = true;
						courseLoop: for (var course of newState) {
							if (course.id == newCourse.id) {
								notFound = false;
								break courseLoop;
							}
						}
						if (notFound) {
							newState.push(newCourse);
							console.log("Added new course", newCourse);
						}
					}
					return {
						...state,
						courses: newState
					};
				} else
					return {
						...state,
						courses: action.data,
						coursesInitialized: true
					};
			} else return state;
			break;
		case COUNTRIES_AVAILABLE:
			return {
				...state,
				countries: action.data
			};
		case SET_US_STATES:
			return {
				...state,
				USStates: action.data
			};
		case SET_LAST_COURSE_SYNC_DATE_TIME:
			return {
				...state,
				lastCourseSyncDateTime: action.data
			};
		case NATIONALITIES_AVAILABLE:
			return {
				...state,
				nationalities: action.data
			};
		default:
			return state;
	}
};

export default persistentReducer;
