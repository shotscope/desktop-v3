// @flow
import { combineReducers } from "redux";
import userReducer from "./userReducer";
import bandReducer from "./bandReducer";
import persistentReducer from "./persistentReducer";
import performanceReducer from "./performanceReducer";
import {
	SET_USER_COURSES_LITE,
	SET_USER_COURSES,
	SET_LOADING,
	SET_FIRMWARE_DATA,
	SET_SERVER_FIRMWARE_VERSION,
	CLEAR_DOWNLOADED_COURSE_DATA,
	SET_NEARBY_COURSES,
	SET_DISABLE_FIRMWARE_ALERT
} from "../actions/index";

const dataState = {
	userCourses: [],
	downloadedCoursesIndex: "",
	downloadedCourses: [],
	firmwareData: null,
	loading: true,
	serverFirmwareVersion: "",
	nearbyCourses: [],
	disableFirmwareAlert: false
};

const dataReducer = (state = dataState, action) => {
	switch (action.type) {
		case SET_DISABLE_FIRMWARE_ALERT:
			return {
				...state,
				disableFirmwareAlert: action.data
			};
		case SET_NEARBY_COURSES:
			return {
				...state,
				nearbyCourses: action.data
			};
		case CLEAR_DOWNLOADED_COURSE_DATA:
			var clearedUserCourses = state.userCourses;
			for (var userCourse of clearedUserCourses) {
				userCourse.courseData = null;
				userCourse.lastUpdateDate = "2000-05-09T08:05:02.047+00:00";
			}
			return {
				...state,
				userCourses: clearedUserCourses,
				downloadedCourses: [],
				downloadedCoursesIndex: ""
			};
		case SET_LOADING:
			return {
				...state,
				loading: action.data
			};
		case SET_SERVER_FIRMWARE_VERSION:
			return {
				...state,
				serverFirmwareVersion: action.data
			};
		case SET_USER_COURSES_LITE:
			var newCourseState = [];
			var found = false;
			for (var liteCourse of action.data) {
				for (var course of state.userCourses) {
					if (course.courseID == liteCourse.courseID) {
						found = true;
						newCourseState.push({
							courseID: course.courseID,
							lastUpdateDate: liteCourse.lastUpdateDate,
							courseData: course.courseData
						});
					}
				}
				if (!found) newCourseState.push(liteCourse);
				found = false;
			}
			return {
				...state,
				userCourses: newCourseState
			};
		case SET_FIRMWARE_DATA:
			return {
				...state,
				firmwareData: action.data
			};
		case SET_USER_COURSES: {
			console.log(action.data);
			return {
				...state,
				downloadedCoursesIndex: action.data.downloadedCoursesIndex,
				downloadedCourses: action.data.downloadedCourses
			};
		}
		default:
			return state;
	}
};

// Combine all the reducers
const rootReducer = combineReducers({
	dataReducer,
	userReducer,
	performanceReducer,
	bandReducer,
	persistentReducer
	// ,[ANOTHER REDUCER], [ANOTHER REDUCER] ....
});

export default rootReducer;
