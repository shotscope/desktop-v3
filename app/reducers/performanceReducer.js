import {
	SET_PERFORMANCE_ENTRIES_COUNT,
	SET_PERFORMANCE_ENTRIES,
	DISCARD_PERFORMANCE_ENTRIES_CHUNK,
	CLEAR_PERFORMANCE_ENTRIES,
	SET_CONNECTED
} from "../actions/index";

// This reducer is used for storing temporary data such as performance data being retrieved from the band.
//It has been separated into its own reducer as redux can slow down the main thread if you're updating a large state very often.

const initialState = {
	performanceEntriesCount: -1,
	performanceEntries: []
};

const performanceReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_CONNECTED:
			return initialState;
			break;
		case SET_PERFORMANCE_ENTRIES_COUNT:
			return {
				...state,
				performanceEntriesCount: action.data
			};
			break;
		case SET_PERFORMANCE_ENTRIES:
			return {
				...state,
				performanceEntries: action.data
			};
			break;
		case DISCARD_PERFORMANCE_ENTRIES_CHUNK:
			var newPerformanceEntries = state.performanceEntries.slice(0);
			var removeEntriesLength = newPerformanceEntries.length % 10;
			if (removeEntriesLength == 0) removeEntriesLength = 10;
			for (var i = 0; i < removeEntriesLength; i++) {
				newPerformanceEntries.pop();
			}
			return {
				...state,
				performanceEntries: newPerformanceEntries
			};
			break;
		case CLEAR_PERFORMANCE_ENTRIES:
			return {
				...state,
				performanceEntries: []
			};
			break;
		default:
			return state;
	}
};

export default performanceReducer;
