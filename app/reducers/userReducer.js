import {
	SET_TOKEN,
	SET_USER_DETAILS,
	CLEAR_USER,
	SET_USER_COURSES,
	SET_LAST_COURSE_SYNC_DATE_TIME,
	SET_EMAIL,
	SET_ONBOARDING,
	SET_FIRMWARE_ALERT
} from "../actions/index";

const initialUserState = {
	token: "",
	homeCourse: "",
	optInPublic: false,
	homeCourseID: null,
	playerType: -1,
	handicap: 0,
	leftHanded: false,
	band1: false,
	band2: false,
	city: "",
	country: "",
	firstName: "",
	lastName: "",
	gender: -1,
	units: -1,
	dob: null,
	nationality: "",
	betaAccess: false,
	photoData: "",
	email: "",
	lastCourseSync: "",
	displayOnBoarding: false,
	showNewFirmwareUpdateAlert: true
};

const userReducer = (state = initialUserState, action) => {
	switch (action.type) {
		case SET_TOKEN:
			return {
				...state,
				token: action.data
			};
		case SET_ONBOARDING:
			return {
				...state,
				displayOnBoarding: action.data
			};
		case CLEAR_USER:
			state = initialUserState;
		case SET_FIRMWARE_ALERT:
			return {
				...state,
				showNewFirmwareUpdateAlert: action.data
			};
		case SET_USER_COURSES:
			return {
				...state
			};
		case SET_LAST_COURSE_SYNC_DATE_TIME:
			return {
				...state,
				lastCourseSync: action.data
			};
		case SET_EMAIL:
			return {
				...state,
				email: action.data
			};
		case SET_USER_DETAILS:
			return {
				...state,
				homeCourse: action.data.homeCourse,
				homeCourseID: action.data.homeCourseID,
				optInPublic: action.data.optInPublic,
				playerType: action.data.playerType,
				handicap: action.data.handicap,
				leftHanded: action.data.leftHanded,
				band1: action.data.band1,
				band2: action.data.band2,
				city: action.data.city,
				country: action.data.country,
				firstName: action.data.firstName,
				lastName: action.data.lastName,
				gender: action.data.gender,
				units: action.data.units,
				dob: action.data.dob,
				nationality: action.data.nationality,
				betaAccess: action.data.betaAccess,
				photoData: action.data.photoData
			};
		default:
			return state;
	}
};

export default userReducer;
