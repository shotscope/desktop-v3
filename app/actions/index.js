import { getProductInfo } from "../utils/SerialHandler";

export const SET_BAND_INFO = "SET_INFO";
export const SET_BATTERY_LEVEL = "SET_BATTERY_LEVEL";
export const SET_HAND = "SET_HAND";
export const SET_COURSE_IDS_COUNT = "SET_COURSE_IDS_COUNT";
export const SET_COURSE_IDS = "SET_COURSE_IDS";
export const SET_TIMESTAMP = "SET_TIMESTAMP";
export const SET_PERFORMANCE_ENTRIES = "SET_PERFORMANCE_ENTRIES";
export const SET_PERFORMANCE_ENTRIES_TEMP = "SET_PERFORMANCE_ENTRIES_TEMP";
export const DISCARD_PERFORMANCE_ENTRIES_CHUNK =
	"DISCARD_PERFORMANCE_ENTRIES_CHUNK";
export const CLEAR_PERFORMANCE_ENTRIES = "CLEAR_PERFORMANCE_ENTRIES";
export const SET_PERFORMANCE_ENTRIES_COUNT = "SET_PERFORMANCE_ENTRIES_COUNT";
export const SET_TOKEN = "SET_TOKEN";
export const SET_CONNECTED = "SET_CONNECTED";
export const COURSES_AVAILABLE = "COURSES_AVAILABLE";
export const NATIONALITIES_AVAILABLE = "NATIONALITIES_AVAILABLE";
export const COUNTRIES_AVAILABLE = "COUNTRIES_AVAILABLE";
export const SET_USER_DETAILS = "SET_USER_DETAILS";
export const CLEAR_USER = "CLEAR_USER";
export const SET_LOADING = "SET_LOADING";
export const SET_USER_COURSES_LITE = "SET_USER_COURSES_LITE";
export const SET_USER_COURSES = "SET_USER_COURSES";
export const SET_FIRMWARE_DATA = "SET_FIRMWARE_DATA";
export const SET_SERVER_FIRMWARE_VERSION = "SET_SERVER_FIRMWARE_VERSION";
export const SET_LAST_COURSE_SYNC_DATE_TIME = "SET_LAST_COURSE_SYNC_DATE_TIME";
export const SET_LAST_PL_COURSE_SYNC_DATE_TIME =
	"SET_LAST_PL_COURSE_SYNC_DATE_TIME";
export const SET_EMAIL = "SET_EMAIL";
export const SET_ONBOARDING = "SET_ONBOARDING";
export const SET_PL_COURSES = "SET_PL_COURSES";
export const SET_BAND_PL_COURSES = "SET_BAND_PL_COURSES";
export const SET_FIRMWARE_ALERT = "SET_FIRMWARE_ALERT";
export const CLEAR_DOWNLOADED_COURSE_DATA = "CLEAR_DOWNLOADED_COURSE_DATA";
export const SET_NEARBY_COURSES = "SET_NEARBY_COURSES";
export const SET_DISABLE_FIRMWARE_ALERT = "SET_DISABLE_FIRMWARE_ALERT";
export const SET_US_STATES = "SET_US_STATES";

// Sets the band firmware version and serial number
export function setBandInfo(data) {
	return {
		type: SET_BAND_INFO,
		data
	};
}

// Sets whether or not to display the first time setup
export function setOnBoarding(data) {
	return {
		type: SET_ONBOARDING,
		data
	};
}

export function setNearbyCourses(data) {
	return {
		type: SET_NEARBY_COURSES,
		data
	};
}

export function setUSStates(data) {
	return {
		type: SET_US_STATES,
		data
	};
}

export function setDisableFirmwareAlertForSession(data) {
	return {
		type: SET_DISABLE_FIRMWARE_ALERT,
		data
	};
}

export function clearDownloadedCourseData() {
	return {
		type: CLEAR_DOWNLOADED_COURSE_DATA
	};
}

// Sets whether or not to display the firmware alert if the band firmware is out of date
export function setFirmwareAlert(data) {
	return {
		type: SET_FIRMWARE_ALERT,
		data
	};
}

// Sets the preloaded course list
export function setPLCourses(data) {
	return {
		type: SET_PL_COURSES,
		data
	};
}

// Sets the courses available on the band via preloading
export function setBandPLCourses(data) {
	return {
		type: SET_BAND_PL_COURSES,
		data
	};
}

// Clears the user's token
export function logout() {
	return {
		type: CLEAR_USER
	};
}

export function setUserDetails(data) {
	return {
		type: SET_USER_DETAILS,
		data
	};
}

export function setEmail(data) {
	return {
		type: SET_EMAIL,
		data
	};
}

export function setLastCourseSyncDateTime(data) {
	return {
		type: SET_LAST_COURSE_SYNC_DATE_TIME,
		data
	};
}

export function setLastPLCourseSyncDateTime(data) {
	return {
		type: SET_LAST_PL_COURSE_SYNC_DATE_TIME,
		data
	};
}

export function setServerFirmwareVersion(data) {
	return {
		type: SET_SERVER_FIRMWARE_VERSION,
		data
	};
}

export function setLoading(data) {
	return {
		type: SET_LOADING,
		data
	};
}

export function setUserCourses(data) {
	return {
		type: SET_USER_COURSES,
		data
	};
}

export function setFirmwareData(data) {
	return {
		type: SET_FIRMWARE_DATA,
		data
	};
}

// Sets the user's course IDs without course data
export function setUserCoursesLite(data) {
	return {
		type: SET_USER_COURSES_LITE,
		data
	};
}

export function setBatteryLevel(data) {
	return {
		type: SET_BATTERY_LEVEL,
		data
	};
}

export function setAllCoursesList(data) {
	return {
		type: COURSES_AVAILABLE,
		data
	};
}

export function setAllNationalitiesList(data) {
	return {
		type: NATIONALITIES_AVAILABLE,
		data
	};
}

export function setAllCountriesList(data) {
	return {
		type: COUNTRIES_AVAILABLE,
		data
	};
}

export function setHand(data) {
	return {
		type: SET_HAND,
		data
	};
}

export function setConnected(data) {
	return {
		type: SET_CONNECTED,
		data
	};
}

export function setCourseIDCount(data) {
	return {
		type: SET_COURSE_IDS_COUNT,
		data
	};
}

export function setPerformanceEntriesCount(data) {
	return {
		type: SET_PERFORMANCE_ENTRIES_COUNT,
		data
	};
}

export function setCourseIDs(data) {
	return {
		type: SET_COURSE_IDS,
		data
	};
}
export function setPerformanceEntries(data) {
	return {
		type: SET_PERFORMANCE_ENTRIES,
		data
	};
}

export function discardPerformanceEntriesChunk() {
	return {
		type: DISCARD_PERFORMANCE_ENTRIES_CHUNK
	};
}

export function clearPerformanceEntries() {
	return {
		type: CLEAR_PERFORMANCE_ENTRIES
	};
}

export function setTimestamp(data) {
	return {
		type: SET_TIMESTAMP,
		data
	};
}

export function setToken(data) {
	return {
		type: SET_TOKEN,
		data
	};
}
