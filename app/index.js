import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Router, Route, hashHistory } from "react-router";
import { bindActionCreators } from "redux";
import LoadingPage from "./components/LoadingPage";
import UpdatePage from "./components/UpdatePage";
import * as Actions from "./actions"; // Import your actions
import { connect } from "react-redux";
import App from "./components/Home";
import configureStore from "./store";
import { PersistGate } from "redux-persist/integration/react";
import { persistor, store } from "./store";
const {
	app,
	BrowserWindow,
	Menu,
	protocol,
	ipcMain,
	ipcRenderer,
	remote
} = require("electron");
import packageJson from "./package";

import SerialHandler from "./utils/SerialHandler";
import WebAPIHandler from "./utils/WebAPIHandler";

SerialHandler.initializeStore(store);
WebAPIHandler.initializeStore(store);

var unsubscribe = null;
var currentPLTimestampValue;
var currentPLCoursesValue;
var PLTimestampReady = false;
var PLCoursesReady = false;
var PLInitialized = false;
let win = remote.getCurrentWindow();

class RootApp extends React.PureComponent {
	constructor(props) {
		super(props);
		var context = this;
		this.state = {
			updateAvailable: false
		};
		this.handlePreloadReady = this.handlePreloadReady.bind(this);
		this.setUpdateAvailable = this.setUpdateAvailable.bind(this);
		this.renderUpdate = this.renderUpdate.bind(this);

		var updateListener = ipcRenderer.on("message", (e, message) => {
			console.log(e, message);
			switch (message) {
				case "Update Available.":
					updateState.updateValue = true;
					break;
				case "Update not available.":
					break;
				default:
					console.log(message);
					break;
			}
		});

		var updateState = {
			setUpdateAvailable: context.setUpdateAvailable,
			set updateValue(value) {
				this.setUpdateAvailable(value);
			}
		};
		unsubscribe = store.subscribe(this.handlePreloadReady);
		SerialHandler.discoverBand();
	}

	setUpdateAvailable(value) {
		this.setState({ updateAvailable: value });
	}

	// Subscribes to the store to listen for the band timestamp and the preloaded course list to be downloaded. Once both are ready, the preloaded courses
	// on the band are calculated and set
	handlePreloadReady() {
		//Subscribe the current pLTimestamp redux state to currentPLTimestampValue
		let previousTimestampValue = currentPLTimestampValue;
		let previousCoursesValue = currentPLCoursesValue;
		currentPLTimestampValue = this.props.band.pLTimestamp;
		currentPLCoursesValue = this.props.persistent.courses;
		if (
			currentPLTimestampValue != previousTimestampValue &&
			currentPLTimestampValue != undefined
		) {
			if (currentPLTimestampValue == 0) {
				PLTimestampReady = false;
				PLInitialized = false;
			} else {
				PLTimestampReady = true;
				console.log("Preload timestamp ready");
			}
		}
		if (
			currentPLCoursesValue != previousCoursesValue &&
			currentPLCoursesValue.length > 0
		) {
			PLCoursesReady = true;
			console.log("Preload courses list ready");
		}
		if (PLTimestampReady && PLCoursesReady && !PLInitialized) {
			PLInitialized = true;
			console.log("Preload band courses ready");
			var PLCourseIDs = [];
			// var bandPLDateTime = new Date(this.props.band.pLTimestamp);
			for (var course of this.props.persistent.courses) {
				if (course.preLoaded) {
					if (
						Date.parse(course.preLoaded) <=
						this.props.band.pLTimestamp * 1000
					)
						PLCourseIDs.push(course.id);
				}
			}
			// console.log(PLCourseIDs);
			this.props.setBandPLCourses(PLCourseIDs);
			unsubscribe();
		}
	}

	render() {
		if (this.state.updateAvailable) return this.renderUpdate();
		else
			return (
				<div>
					<App />
				</div>
			);
	}

	renderUpdate() {
		win.setMinimumSize(350, 300);
		win.setMaximumSize(350, 300);
		win.setBounds({
			width: 350,
			height: 300,
			x: win.getBounds().x,
			y: win.getBounds().y
		});
		win.center();
		return (
			<div>
				<UpdatePage />
			</div>
		);
	}
}

// if (process.env.NODE_ENV !== "production") {
// 	const { whyDidYouUpdate } = require("why-did-you-update");
// 	whyDidYouUpdate(React);
// }

function mapStateToProps(state, props) {
	return {
		dataReducer: state.dataReducer,
		band: state.bandReducer,
		persistent: state.persistentReducer
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

RootApp = connect(
	mapStateToProps,
	mapDispatchToProps
)(RootApp);
module.exports = RootApp;

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<RootApp />
		</PersistGate>
	</Provider>,
	document.getElementById("root")
);
