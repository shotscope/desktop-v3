import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import createIdbStorage from "@piotr-cz/redux-persist-idb-storage/src";
import { openDb } from "idb";
// See https://github.com/piotr-cz/redux-persist-idb-storage for redux persist integration with IndexedDB
import reducers from "./reducers/index";

const persistConfig = {
	key: "root",
	whitelist: ["persistentReducer", "userReducer"],
	storage: createIdbStorage({
		name: "ShotScopeDB",
		storeName: "persistedStore",
		version: 1
	}),
	serialize: false // Data serialization is not required and helps allows DevTools to inspect storage value
};
// See here for migrating the DB: https://github.com/jakearchibald/idb/tree/v3.0.2#api
// When in doubt, just delete the database
const dbPromise = openDb("ShotScopeDB", 1, upgradeDB => {
	// Note: we don't use 'break' in this switch statement,
	// the fall-through behaviour is what we want.
	switch (upgradeDB.oldVersion) {
		case 0:
			upgradeDB.createObjectStore("persistedStore");
		// case 1:
		// 	upgradeDB.createObjectStore("objs", { keyPath: "id" });
	}
});

const persistedReducer = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducer, applyMiddleware(thunk));
let persistor = persistStore(store);
export { store, persistor };
