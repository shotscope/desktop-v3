/* eslint global-require: 0, flowtype-errors/show-errors: 0 */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import { app, BrowserWindow, Notification } from "electron";
import MenuBuilder from "./menu";
const log = require("electron-log");
const { autoUpdater } = require("electron-updater");
const path = require("path");

// Used to ensure that only one instance of the app can be open at a time
const gotTheLock = app.requestSingleInstanceLock();
let mainWindow = null;
app.setAppUserModelId("com.shotscope");
// webContents.on("new-window", function(event, url) {
// 	event.preventDefault();
// 	open(url);
// });

if (process.env.NODE_ENV === "production") {
	const sourceMapSupport = require("source-map-support");
	sourceMapSupport.install();
}

if (
	process.env.NODE_ENV === "development" ||
	process.env.DEBUG_PROD === "true"
) {
	require("electron-debug")();
	const p = path.join(__dirname, "..", "app", "node_modules");
	require("module").globalPaths.push(p);
}

const installExtensions = async () => {
	const installer = require("electron-devtools-installer");
	const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
	const extensions = ["REACT_DEVELOPER_TOOLS", "REDUX_DEVTOOLS"];

	return Promise.all(
		extensions.map(name => installer.default(installer[name], forceDownload))
	).catch(console.log);
};

/**
 * Add event listeners...
 */

autoUpdater.requestHeaders = { authorization: "" };
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = "info";
log.info("App starting...");

function sendStatusToWindow(text) {
	log.info(text);

	mainWindow.webContents.send("message", text);
}
// function createDefaultWindow() {
//   mainWindow = new BrowserWindow();
//   mainWindow.webContents.openDevTools();
//   mainWindow.on('closed', () => {
//     win = null;
//   });
//   sendStatusToWindow(`file://${__dirname}/version.html#v${app.getVersion()}`);
//   mainWindow.loadURL(`file://${__dirname}/version.html#v${app.getVersion()}`);
//   return mainWindow;
// }
autoUpdater.on("checking-for-update", () => {
	sendStatusToWindow("Checking for update...");
});
autoUpdater.on("update-available", (ev, info) => {
	sendStatusToWindow("Update available.");
});
autoUpdater.on("update-not-available", (ev, info) => {
	sendStatusToWindow("Update not available.");
});
autoUpdater.on("error", (ev, err) => {
	sendStatusToWindow("Error in auto-updater.");
});
autoUpdater.on("download-progress", (ev, progressObj) => {
	sendStatusToWindow("Download progress...");
});
autoUpdater.on("update-downloaded", (ev, info) => {
	sendStatusToWindow("Update downloaded; will install in 5 seconds");
});

autoUpdater.on("update-downloaded", (ev, info) => {
	autoUpdater.quitAndInstall();
});

app.on("window-all-closed", () => {
	// Respect the OSX convention of having the application in memory even
	// after all windows have been closed
	if (process.platform !== "darwin") {
		app.quit();
	}
});

if (!gotTheLock) {
	app.quit();
} else {
	app.on("second-instance", (event, commandLine, workingDirectory) => {
		// Someone tried to run a second instance, we should focus our window.
		if (mainWindow) {
			if (mainWindow.isMinimized()) mainWindow.restore();
			mainWindow.focus();
		}
	});

	app.on("ready", async () => {
		if (
			process.env.NODE_ENV === "development" ||
			process.env.DEBUG_PROD === "true"
		) {
			autoUpdater.updateConfigPath = path.join(__dirname, "dev-app-update.yml");
			await installExtensions();
		}

		mainWindow = new BrowserWindow({
			show: false,
			width: 1024,
			height: 768,
			minHeight: 600,
			minWidth: 800,
			backgroundColor: "#fff",
			icon: path.join(__dirname, "icons/icon.png")
		});
		// mainWindow.openDevTools();

		sendStatusToWindow(`file://${__dirname}/app.html`);
		mainWindow.loadURL(`file://${__dirname}/app.html`);

		// @TODO: Use 'ready-to-show' event
		//        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
		mainWindow.webContents.on("did-finish-load", () => {
			if (!mainWindow) {
				throw new Error('"mainWindow" is not defined');
			}
			autoUpdater.checkForUpdates();
			if (process.env.START_MINIMIZED) {
				mainWindow.minimize();
			} else {
				mainWindow.show();
				mainWindow.focus();
			}
		});

		mainWindow.on("closed", () => {
			mainWindow = null;
		});

		const menuBuilder = new MenuBuilder(mainWindow);
		menuBuilder.buildMenu();
	});
}
