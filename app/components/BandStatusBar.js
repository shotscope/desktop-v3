import React from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions
import SerialHandler from "../utils/SerialHandler";

class BandStatusBar extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			previousValue: 0,
			currentValue: 0,
			timeLeft: "Infinity",
			progressPercent: 0,
			action: ""
		};
		this.estimateTimeLeft = this.estimateTimeLeft.bind(this);
		this.renderTimeLeft = this.renderTimeLeft.bind(this);
		this.progressUpdateCallback = this.progressUpdateCallback.bind(this);
	}

	componentDidMount() {
		this.estimateTimeLeft();
		SerialHandler.setProgressCallback(this.progressUpdateCallback);
	}

	progressUpdateCallback(data) {
		this.setState({ progressPercent: data.percent, action: data.action });
		this.forceUpdate(); // Need to force the update as the only thing that is changing is CSS
	}

	render() {
		return (
			<div style={this.styles.barStyle}>
				<div
					style={{
						height: "100%",
						zIndex: 1,
						display: "flex",
						alignItems: "center",
						position: "static",
						width: this.state.progressPercent + "%",
						backgroundColor: "#26CA85"
					}}
				/>
				<div
					style={{
						fontSize: 14,
						fontWeight: "bold",
						position: "absolute",
						display: "flex",
						alignItems: "center",
						zIndex: 2,
						left: 0
					}}
				>
					<div
						style={{
							display: "flex",
							marginLeft: 8,
							alignItems: "center"
						}}
					>
						{this.state.progressPercent != 0 ? (
							<div>
								<img
									style={{ marginRight: 8 }}
									className="statusActionImage"
									src={require("../../resources/Syncing/Update.png")}
								/>
							</div>
						) : null}
						{this.state.action} {this.renderTimeLeft()}
					</div>
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "flex-end",
						position: "absolute",
						alignItems: "center",
						right: 0
					}}
				>
					{this.props.band.connected ? (
						<div
							style={{
								marginRight: 8,
								fontSize: 14,
								fontWeight: "bold",
								display: "flex",
								alignItems: "center",
								zIndex: 2
							}}
						>
							<div>
								{this.props.band.batteryLevel}%
								<img
									style={{ marginLeft: 4 }}
									src={require("../../resources/Battery/BatteryCharging.png")}
								/>
							</div>
						</div>
					) : null}
					<div
						style={{
							marginRight: 16,
							fontSize: 14,
							fontWeight: "bold",
							zIndex: 2
						}}
					>
						{this.props.band.connected ? (
							<div>
								Band Connected{" "}
								<img
									src={require("../../resources/Syncing/Band_Connected.png")}
									style={{ marginLeft: 4 }}
								/>
							</div>
						) : (
							<div>Band Disconnected</div>
						)}
					</div>
				</div>
			</div>
		);
	}

	renderTimeLeft() {
		if (
			(this.state.timeLeft > 0 && this.state.timeLeft != "Infinity") ||
			(this.state.currentValue == this.state.previousValue &&
				this.state.currentValue < 100 &&
				this.state.timeLeft != "Infinity" &&
				this.state.timeLeft > 0)
		)
			return (
				<div style={{ marginLeft: 8 }}>
					{this.state.timeLeft < 60 ? (
						<div>
							- Estimated time left: {Math.round(this.state.timeLeft)} seconds
						</div>
					) : (
						<div>
							- Estimated time left: {Math.round(this.state.timeLeft / 60)}{" "}
							minute{Math.round(this.state.timeLeft / 60) > 1 ? "s" : null}
						</div>
					)}
				</div>
			);
	}

	estimateTimeLeft() {
		setInterval(() => {
			this.setState(
				{
					previousValue: this.state.currentValue
				},
				() => {
					this.setState(
						{
							currentValue: this.state.progressPercent
						},
						() => {
							if (this.state.previousValue != this.state.currentValue)
								this.setState({
									timeLeft:
										(2 / (this.state.currentValue - this.state.previousValue)) *
										(100 - this.state.currentValue)
								});
						}
					);
				}
			);
		}, 2000);
	}

	styles = {
		barStyle: {
			color: "white",
			display: "flex",
			alignItems: "center",
			height: 30,
			backgroundColor: "#292929",
			zIndex: 0
		}
	};
}

function mapStateToProps(state, props) {
	return {
		band: state.bandReducer,
		dataReducer: state.dataReducer
	};
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

BandStatusBar = connect(
	mapStateToProps,
	mapDispatchToProps
)(BandStatusBar);
module.exports = BandStatusBar;
