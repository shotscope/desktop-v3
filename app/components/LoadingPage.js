import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions

class LoadingPage extends React.PureComponent {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div
				style={{
					position: "absolute",
					top: 0,
					left: 0,
					height: "100%",
					width: "100%",
					backgroundColor: "red",
					justifyContent: "center",
					display: "flex",
					flexDirection: "column",
					zIndex: 99,
					fontSize: 24
				}}
			>
				<div style={{ alignSelf: "center" }}>
					<img
						style={{ width: 230, height: 50 }}
						src={require("../../resources/ShotScopeLogo_White.png")}
					/>
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "center"
					}}
				>
					<div className="actionSpinnerImage">
						<img
							style={{ width: 100, height: 100 }}
							src={require("../../resources/Syncing/Sync.png")}
						/>
					</div>
				</div>
				<div style={{ alignSelf: "center", marginTop: 12 }}>Loading...</div>
			</div>
		);
	}
}

function mapStateToProps(state, props) {
	return {};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

LoadingPage = connect(
	mapStateToProps,
	mapDispatchToProps
)(LoadingPage);
module.exports = LoadingPage;
