import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Autosuggest from "react-autosuggest";
import WebAPIHandler from "../utils/WebAPIHandler";
import * as Actions from "../actions";

let courses = [];

const getSuggestions = value => {
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;

	return inputLength === 0
		? []
		: courses.filter(
				course => course.name.toLowerCase().slice(0, inputLength) === inputValue
		  );
};

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
	<div
		style={{
			backgroundColor: "gray"
		}}
	>
		{suggestion.name}
	</div>
);
class FindCourses extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			value: "",
			suggestions: []
		};
	}

	onChange = (event, { newValue }) => {
		this.setState({
			value: newValue
		});
	};

	// Autosuggest will call this function every time you need to update suggestions.
	// You already implemented this logic above, so just use it.
	onSuggestionsFetchRequested = ({ value }) => {
		this.setState({
			suggestions: getSuggestions(value)
		});
	};

	// Autosuggest will call this function every time you need to clear suggestions.
	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

	async componentDidMount() {
		await WebAPIHandler.getAllCourses().then(
			response => (courses = this.props.courses)
		);
	}

	render() {
		const { value, suggestions } = this.state;
		const inputProps = {
			placeholder: "Enter course name",
			value,
			onChange: this.onChange
		};
		return (
			<div>
				<p>Find Courses lol</p>
				<Autosuggest
					suggestions={suggestions}
					onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested={this.onSuggestionsClearRequested}
					getSuggestionValue={getSuggestionValue}
					renderSuggestion={renderSuggestion}
					inputProps={inputProps}
				/>
			</div>
		);
	}
}

function mapStateToProps(state, props) {
	return {
		courses: state.persistent.courses
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

FindCourses = connect(
	mapStateToProps,
	mapDispatchToProps
)(FindCourses);
module.exports = FindCourses;
