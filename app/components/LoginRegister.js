import React from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions
import DropDown from "./utils/DropDown";
import AlertDialog from "./utils/AlertDialog";
import WebAPIHandler, { url } from "../utils/WebAPIHandler";
import { handicaps } from "../utils/constants";
import { shell } from "electron";
import { validateEmail } from "../utils/util";

class LoginRegister extends React.PureComponent {
	constructor(props) {
		super(props);
		props.setLoading(true);
		var nationalityNameArray = [];
		this.props.persistent.nationalities.map(nationality =>
			nationalityNameArray.push(nationality.name)
		);

		var handicapArray = [];
		for (var i = -54; i < 8; i++) handicapArray.push(i);

		var coursesArray = [];
		for (var course of this.props.persistent.courses)
			coursesArray.push(course.name);

		var discovery;

		this.state = {
			slideshowImages: [
				require("../../resources/loginSlideshow/1.jpg"),
				require("../../resources/loginSlideshow/2.jpg"),
				require("../../resources/loginSlideshow/3.png"),
				require("../../resources/loginSlideshow/4.jpg"),
				require("../../resources/loginSlideshow/5.jpg"),
				require("../../resources/loginSlideshow/6.jpg")
			],
			currentImageIndex: 0,
			username: "",
			password: "",
			renderLoginFailed: false,
			renderRegisterFailed: false,
			renderNoServerResponse: false,
			register: "",
			registerFirstName: "",
			registerSurname: "",
			registerEmail: "",
			registerPassword: "",
			registerConfirmPassword: "",
			registerPasswordMatches: null,
			registerPasswordValid: null,
			registerHomeCourse: 0,
			registerHomeCourseArray: coursesArray,
			registerDOB: "",
			registerGender: 0,
			registerGenderArray: ["Male", "Female", "Other"],
			registerNationality: 0,
			registerNationalityArray: nationalityNameArray,
			registerPlayerType: 0,
			registerPlayerTypeArray: ["Amateur", "Professional", "College"],
			registerHandicap: 0,
			registerHandicapArray: handicapArray,
			renderRegisterPageTwo: false,
			registerTOC: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleRegister = this.handleRegister.bind(this);
		this.toggleDropDown = this.toggleDropDown.bind(this);
		this.registerPageOneReady = this.registerPageOneReady.bind(this);
		this.handleDropDownChange = this.handleDropDownChange.bind(this);
		this.toggleDisplayLoginFailed = this.toggleDisplayLoginFailed.bind(this);
		this.toggleDisplayRegisterFailed = this.toggleDisplayRegisterFailed.bind(
			this
		);
		this.toggleDisplayNoServerResponse = this.toggleDisplayNoServerResponse.bind(
			this
		);
	}

	componentDidMount() {
		this.props.setLoading(false);
		this.discovery = setInterval(() => {
			if (this.state.currentImageIndex != this.state.slideshowImages.length - 1)
				this.setState({ currentImageIndex: this.state.currentImageIndex + 1 });
			else this.setState({ currentImageIndex: 0 });
		}, 5000);
	}

	componentWillUnmount() {
		clearInterval(this.discovery);
	}

	toggleDropDown(event) {
		const target = event.target;
		const name = target.name;
		if (this.state.dropDownEnabled == "none")
			this.setState({ dropDownEnabled: "block" });
		else this.setState({ dropDownEnabled: "none" });
	}

	handleChange(event) {
		const target = event.target;
		const name = target.name;
		this.setState({ [name]: target.value });
	}

	async handleSubmit(event) {
		event.preventDefault();
		this.props.setEmail(this.state.username);
		await WebAPIHandler.login(this.state.username, this.state.password).then(
			response => {
				console.log(response);
				if (response == false) this.toggleDisplayLoginFailed();
				else if (response == true) WebAPIHandler.getUser();
				else this.toggleDisplayLoginFailed();
			}
		);
	}

	async handleRegister(event) {
		event.preventDefault();
		var handicapString;

		for (var handicap of handicaps) {
			if (
				handicap.value ==
				this.state.registerHandicapArray[this.state.registerHandicap]
			) {
				handicapString = handicap.name;
				break;
			}
		}

		var homeCourseID;
		var nationalityID;
		for (var course of this.props.persistentReducer.courses) {
			if (course.name == this.state.registerHomeCourse) {
				homeCourseID = course.id;
				break;
			}
		}
		for (var nationality of this.props.persistent.nationalities) {
			if (nationality.name == this.state.registerNationality) {
				nationalityID = nationality.nationality;
				break;
			}
		}

		var registerObj = {
			homeCourseID: homeCourseID,
			playerType: this.state.registerPlayerType,
			handicap: handicapString,
			gender: this.state.registerGender,
			nationality: nationalityID,
			email: this.state.registerEmail,
			password: this.state.registerPassword,
			confirmPassword: this.state.registerConfirmPassword,
			firstName: this.state.registerFirstName,
			lastName: this.state.registerSurname,
			dob: this.state.registerDOB + "T00:00:00.000+00:00"
		};
		console.log(registerObj);
		var registerSuccess = false;
		await WebAPIHandler.registerAndValidate(registerObj).then(response => {
			if (response.status == 200) {
				registerSuccess = true;
				console.log("registered");
				this.props.setOnBoarding(true);
			} else this.toggleDisplayRegisterFailed();
		});
		if (registerSuccess) {
			this.props.setEmail(this.state.registerEmail);
			this.props.setOnBoarding(true);
			await WebAPIHandler.login(
				this.state.registerEmail,
				this.state.registerPassword
			).then(response => {
				if (response.status != 200) {
					this.setState({ register: false });
					this.toggleDisplayLoginFailed();
				} else {
					this.props.setFirmwareAlert(true);
				}
			});
			await WebAPIHandler.getUser();
		}
	}

	handleDropDownChange(key, value) {
		var stateObj = { [key]: value };
		this.setState(stateObj);
	}

	renderLogin() {
		return (
			<form
				style={{
					display: "flex",
					flexDirection: "column",
					textAlign: "left",
					justifyContent: "space-between"
				}}
				onSubmit={this.handleSubmit}
			>
				<div style={styles.inputHeader}>E-mail</div>
				<input
					style={styles.inputBox}
					type="email"
					name="username"
					placeholder="Email"
					required
					value={this.state.username}
					onChange={this.handleChange}
				/>
				<div style={styles.inputHeader}>Password</div>
				<input
					style={styles.inputBox}
					type="password"
					name="password"
					placeholder="Password"
					required
					value={this.state.password}
					onChange={this.handleChange}
				/>
				<a
					style={{
						marginTop: 14,
						fontFamily: "Roboto",
						fontSize: 12,
						fontWeight: "bold",
						color: "#8C8C8C",
						alignSelf: "flex-end"
					}}
					onClick={() => shell.openExternal(url + "Account/ForgotPassword")}
				>
					Forgotten Password?
				</a>
				<div style={styles.buttonsContainer}>
					<input
						style={{
							width: 126,
							height: 30,
							border: "none",
							borderRadius: 2,
							backgroundColor: "#26CA85",
							color: "white",
							fontFamily: "Roboto",
							fontSize: 14,
							fontWeight: "bold",
							cursor: "pointer"
						}}
						type="submit"
						value="Log in"
					/>
					<button
						style={styles.button}
						onClick={() => {
							this.setState({ register: true });
						}}
					>
						Register
					</button>
				</div>
			</form>
		);
	}

	renderRegister() {
		return (
			<form
				style={{
					textAlign: "left"
				}}
				id="regForm"
				onSubmit={this.handleRegister}
			>
				{!this.state.renderRegisterPageTwo
					? this.renderRegisterPageOne()
					: this.renderRegisterPageTwo()}
			</form>
		);
	}

	renderRegisterPageTwo() {
		return (
			<div className="registerTab">
				<div style={styles.inputHeader}>Home Course</div>
				<DropDown
					data={this.state.registerHomeCourseArray}
					containerStyle={styles.inputBox}
					stateKey="registerHomeCourse"
					entriesLimit={100}
					showFilter
					changeDropDownSelection={this.handleDropDownChange}
				/>
				<div
					style={{
						display: "flex",
						flexDirection: "row",
						width: "100%",
						justifyContent: "center",
						alignContent: "space-between"
					}}
				>
					<div
						style={{
							display: "flex",
							flexDirection: "column",
							width: "50%",
							alignContent: "space-between",
							marginRight: 30
						}}
					>
						<div style={styles.inputHeader}>Date of Birth</div>
						<input
							type="date"
							style={styles.inputBox}
							placeholder="DOB"
							name="registerDOB"
							required
							value={this.state.registerDOB}
							onChange={this.handleChange}
						/>
						<div style={styles.inputHeader}>Nationality</div>
						<DropDown
							data={this.state.registerNationalityArray}
							containerStyle={styles.inputBox}
							stateKey="registerNationality"
							changeDropDownSelection={this.handleDropDownChange}
						/>
						<div style={styles.inputHeader}>Handicap</div>
						<DropDown
							data={this.state.registerHandicapArray}
							containerStyle={styles.inputBox}
							initialSelection={54}
							displayNumberSign="true"
							stateKey="registerHandicap"
							changeDropDownSelection={this.handleDropDownChange}
						/>
					</div>
					<div
						style={{
							display: "flex",
							width: "50%",
							flexDirection: "column",
							alignContent: "space-between"
						}}
					>
						<div style={styles.inputHeader}>Gender</div>
						<DropDown
							data={this.state.registerGenderArray}
							containerStyle={styles.inputBox}
							stateKey="registerGender"
							changeDropDownSelection={this.handleDropDownChange}
						/>
						<div style={styles.inputHeader}>Player Type</div>
						<DropDown
							data={this.state.registerPlayerTypeArray}
							containerStyle={styles.inputBox}
							stateKey="registerPlayerType"
							changeDropDownSelection={this.handleDropDownChange}
						/>
						<div />
						<div style={{ height: 76 }} />
					</div>
				</div>
				<div style={styles.buttonsContainer}>
					<button
						style={styles.button}
						onClick={() => {
							this.setState({ renderRegisterPageTwo: false });
						}}
					>
						Back
					</button>
					<input
						style={styles.button}
						disabled={!this.submitReady()}
						type="submit"
						value="Register"
					/>
				</div>
				<div
					style={{
						height: 29,
						width: "100%",
						color: "#8C8C8C",
						fontFamily: "Roboto",
						fontSize: 12,
						fontWeight: 500,
						textAlign: "center"
					}}
				>
					<input
						type="checkbox"
						checked={this.state.registerTOC}
						onChange={() =>
							this.setState({ registerTOC: !this.state.registerTOC })
						}
					/>
					I have read and agree to the{" "}
					<a
						style={{
							color: "#8C8C8C",
							opacity: 1,
							textDecoration: "underline"
						}}
						onClick={() => {
							shell.openExternal("https://shotscope.com/uk/legal/privacy/");
						}}
					>
						terms and conditions
					</a>
				</div>
			</div>
		);
	}

	renderRegisterPageOne() {
		return (
			<div className="registerTab">
				<div
					style={{
						display: "flex",
						flexDirection: "row",
						width: "100%",
						alignContent: "space-between"
					}}
				>
					{" "}
					<div
						style={{
							display: "flex",
							flexDirection: "column",
							marginRight: 30,
							flexGrow: 1
						}}
					>
						<div style={styles.inputHeader}>First Name</div>
						<input
							type="text"
							name="registerFirstName"
							style={styles.inputBox}
							placeholder="First Name"
							required
							value={this.state.registerFirstName}
							onChange={this.handleChange}
						/>
					</div>{" "}
					<div
						style={{
							display: "flex",
							flexDirection: "column",
							flexGrow: 1
						}}
					>
						<div style={styles.inputHeader}>Last Name</div>
						<input
							type="text"
							style={styles.inputBox}
							placeholder="Last Name"
							name="registerSurname"
							required
							value={this.state.registerSurname}
							onChange={this.handleChange}
						/>
					</div>
				</div>
				<div
					style={{
						display: "flex",
						flexDirection: "column"
					}}
				>
					<div style={styles.inputHeader}>Email Address</div>
					<input
						type="email"
						style={styles.inputBox}
						placeholder="Email Address"
						name="registerEmail"
						required
						value={this.state.registerEmail}
						onChange={this.handleChange}
					/>
					<div style={styles.inputHeader}>Password</div>
					<input
						type="password"
						style={styles.inputBox}
						placeholder="Password"
						name="registerPassword"
						onBlur={() => {
							if (
								this.state.registerPassword.length < 6 ||
								!this.state.registerPassword.match(/\d+/g)
							)
								this.setState({ registerPasswordValid: false });
							else if (this.state.registerPassword.length == 0)
								this.setState({ registerPasswordValid: null });
							else this.setState({ registerPasswordValid: true });
						}}
						required
						value={this.state.registerPassword}
						onChange={this.handleChange}
					/>
					<div style={styles.inputHeader}>Confirm Password</div>
					<input
						type="password"
						style={styles.inputBox}
						placeholder="Confirm Password"
						name="registerConfirmPassword"
						onBlur={() => {
							if (
								this.state.registerPassword !=
								this.state.registerConfirmPassword
							)
								this.setState({ registerPasswordMatches: false });
							else if (this.state.registerConfirmPassword.length == 0)
								this.setState({ registerPasswordMatches: null });
							else this.setState({ registerPasswordMatches: true });
						}}
						required
						value={this.state.registerConfirmPassword}
						onChange={this.handleChange}
					/>
				</div>
				<div
					style={Object.assign({}, styles.buttonsContainer, {
						marginBottom: 20
					})}
				>
					<button
						style={styles.button}
						type="button"
						onClick={() => {
							this.setState({ register: false });
						}}
					>
						Cancel
					</button>
					<button
						style={styles.button}
						disabled={!this.registerPageOneReady()}
						onClick={() => {
							this.setState({ renderRegisterPageTwo: true });
						}}
					>
						Next
					</button>
				</div>
				{this.state.registerPasswordValid == false ? (
					<div
						style={{
							color: "red",
							fontSize: 12,
							marginLeft: "auto",
							marginRight: "auto",
							marginTop: 6,
							textAlign: "center"
						}}
					>
						Password requirements not met: minimum 6 characters including 1
						digit.
					</div>
				) : null}
				{this.state.registerPasswordMatches == false ? (
					<div
						style={{
							color: "red",
							fontSize: 12,
							marginLeft: "auto",
							marginRight: "auto",
							marginTop: 6,
							textAlign: "center",
							marginBottom: 16
						}}
					>
						Passwords do not match
					</div>
				) : null}
			</div>
		);
	}

	// Validates that the info on the first register page is ready to go to the next page
	registerPageOneReady() {
		if (this.state.registerFirstName == "" || this.state.registerSurname == "")
			return false;
		if (this.state.registerPassword.length < 6) return false;
		if (this.state.registerPassword != this.state.registerConfirmPassword)
			return false;
		// Check if the password contains a number
		if (!/\d/.test(this.state.registerPassword)) return false;
		if (!validateEmail(this.state.registerEmail)) return false;
		return true;
	}

	submitReady() {
		if (
			this.state.registerDOB == "" ||
			this.state.registerTOC == false ||
			this.state.registerGender == 0 ||
			this.state.registerPlayerType == 0 ||
			this.state.registerHomeCourse == 0 ||
			this.state.registerNationality == 0
		)
			return false;
		return true;
	}

	toggleDisplayLoginFailed() {
		this.setState({ renderLoginFailed: !this.state.renderLoginFailed });
	}

	toggleDisplayRegisterFailed() {
		this.setState({ renderRegisterFailed: !this.state.renderRegisterFailed });
	}

	toggleDisplayNoServerResponse() {
		this.setState({
			renderNoServerResponse: !this.state.renderNoServerResponse
		});
	}

	render() {
		return (
			<div
				style={{
					display: "flex",
					width: "100%",
					height: "100vh",
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					backgroundColor: "white"
				}}
			>
				{this.state.renderLoginFailed ? (
					<AlertDialog
						dismissCallback={this.toggleDisplayLoginFailed}
						header="Login Failed"
						content="Unable to log in using your email and password. Please make sure that you have entered your credentials correctly and try again. If you have forgotton or lost your password then click the link on the log in page to recover your password."
					/>
				) : null}
				{this.state.renderRegisterFailed ? (
					<AlertDialog
						dismissCallback={this.toggleDisplayRegisterFailed}
						header="Registration Failed"
						content="Failed to register account using your details. Please make sure all fields have been filled out and try again. If the issue persists please check your internet connection and contact support."
					/>
				) : null}
				{this.state.renderNoServerResponse ? (
					<AlertDialog
						dismissCallback={this.toggleDisplayNoServerResponse}
						header="Login Failed"
						content="Unable to log in due to no response from the server. Please ensure that you are connected to the internet and try again. If the issue persists, contact support."
					/>
				) : null}
				{/* <div
					style={{
						height: "100%",
						width: "50%",
						maxWidth: 1200,
						overflow: "hidden"
					}}
				>
					<img
						style={{ position: "absolute", right: "50%" }}
						src={this.state.slideshowImages[this.state.currentImageIndex]}
					/>
				</div> */}
				<img
					style={{ position: "absolute" }}
					src={require("../../resources/Images/3Golf-Club-cropped2.jpg")}
				/>
				<div
					style={{
						width: 450,
						backgroundColor: "#ebebeb",
						zIndex: 1,
						boxShadow: "0px 8px 16px 0px rgba(0, 0, 0, 0.2)"
					}}
				>
					<div
						style={{
							marginTop: 60,
							marginLeft: 60,
							marginRight: 60
						}}
					>
						<img
							style={{
								display: "block",
								marginLeft: "auto",
								marginRight: "auto",
								marginBottom: 8
							}}
							src={require("../../resources/Logo.png")}
						/>
						{!this.state.register ? this.renderLogin() : this.renderRegister()}
					</div>
				</div>
			</div>
		);
	}
}

const styles = {
	inputBox: {
		height: 39,
		backgroundColor: "white",
		marginTop: 4,
		marginBottom: 0,
		border: 0,
		alignSelf: "left",
		color: "black",
		flexGrow: 1,
		fontSize: 12,
		paddingLeft: 14,
		width: "100%"
	},
	buttonsContainer: {
		justifyContent: "space-between",
		marginLeft: "auto",
		marginRight: "auto",
		display: "flex",
		marginTop: 25,
		width: 264
	},
	button: { width: 126, height: 30, cursor: "pointer" },
	inputHeader: {
		marginTop: 16,
		marginBottom: 0,
		fontSize: 14,
		fontFamily: "Roboto",
		fontWeight: "bold",
		color: "#8C8C8C"
	},
	loginButton: {
		height: 30,
		width: 126,
		backgroundColor: "#26CA85",
		border: 0,
		borderRadius: 2,
		cursor: "pointer"
	}
};

function mapStateToProps(state, props) {
	return {
		user: state.userReducer,
		dataReducer: state.dataReducer,
		persistent: state.persistentReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

LoginRegister = connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginRegister);
module.exports = LoginRegister;
