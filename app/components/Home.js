import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { shell } from "electron";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions
import LoginRegister from "./LoginRegister";
import BandStatusBar from "./BandStatusBar";
import LoadingPage from "./LoadingPage";
import Courses from "./Courses";
import MyShotScope from "./MyShotScope/MyShotScope";
import Profile from "./Profile";
import FindCourses from "./FindCourses";
import NavPanel from "./NavPanel";
import AlertDialog from "./utils/AlertDialog";
import WebAPIHandler from "../utils/WebAPIHandler";
import "../app.global.css";
import SerialHandler, { PRODUCT_ID_V1 } from "../utils/SerialHandler";
import OnBoarding from "./OnBoarding/OnBoarding";
import { isHigherFirmwareVersion } from "../utils/util";
import { store } from "../store";

var unsubscribe = null;
var currentTokenValue;

class App extends React.PureComponent {
	constructor(props) {
		super(props);
		props.setLoading(true);
		this.state = {
			currentComponent: "MyShotScope",
			currentMyShotScopeWindow: "Connect",
			renderFirmwareSuccess: false,
			renderFirmwareFailed: false,
			firmwareUpdating: false
		};
		if (props.user.showNewFirmwareUpdateAlert == undefined)
			props.setFirmwareAlert(true);
		this.props.setDisableFirmwareAlertForSession(false);
		this.handleUserReady = this.handleUserReady.bind(this);
		unsubscribe = store.subscribe(this.handleUserReady);
		this.navCallbackHandler = this.navCallbackHandler.bind(this);
		this.renderV1Connected = this.renderV1Connected.bind(this);
		this.firmwareCompletionCallback = this.firmwareCompletionCallback.bind(
			this
		);
	}

	// Subscribes to the store to listen for the user timestamp to retrieve user details if they log in
	async handleUserReady() {
		//Subscribe the current user token redux state to currentTokenValue
		let previousTokenValue = currentTokenValue;
		currentTokenValue = this.props.user.token;
		if (currentTokenValue != previousTokenValue && currentTokenValue != "") {
			console.log("New user token found");

			Promise.all([
				WebAPIHandler.getUser(),
				WebAPIHandler.getUserCoursesLite(),
				WebAPIHandler.getUserCourses()
			]).then(response => {
				console.log(response);
				WebAPIHandler.getNearbyCourses(this.props.user.homeCourseID).then(
					response => {
						this.props.setNearbyCourses(response);
					}
				);
			});
		}
	}

	componentWillUnmount() {
		if (unsubscribe != null) unsubscribe();
	}

	handleBandConnected() {
		currentState = this.props.band;
	}

	async componentDidMount() {
		if (this.props.user.token != "") {
			Promise.all([
				WebAPIHandler.getAllCourses(),
				WebAPIHandler.getAllNationalities()
			]).then(result => {
				console.log("Home ready", result);
				this.props.setLoading(false);
				for (var results of result) {
					if (results != true) this.props.setToken(null);
				}
				// this.props.setOnBoarding(true);
			});
		} else {
			Promise.all([
				WebAPIHandler.getAllCourses(),
				WebAPIHandler.getAllNationalities()
			]).then(result => {
				console.log("Home ready", result);
				this.props.setLoading(false);
				// this.props.setOnBoarding(true);
			});
		}
	}

	firmwareCompletionCallback(success) {
		this.setState({ busy: false, firmwareUpdating: false });
		if (success == true) {
			this.props.setDisableFirmwareAlertForSession(true);
			this.setState({
				renderFirmwareSuccess: true,
				firmwareUpdating: false
			});
			this.props.setFirmwareAlert(true);
		} else if (success == "CANCEL") {
			this.setState({ firmwareUpdating: false });
		} else
			this.setState({ renderFirmwareFailed: true, firmwareUpdating: false });
	}

	async changeHand() {
		this.props.band.hand == "RIGHT"
			? SerialHandler.parseUART(SET_HAND, "LEFT")
			: SerialHandler.parseUART(SET_HAND, "RIGHT");
		SerialHandler.parseUART(GET_HAND);
	}

	uploadPerformance() {
		uploadPerformanceEntries(
			this.props.performance.performanceEntries,
			this.props.user.token
		);
	}

	async getBandEntries() {
		SerialHandler.getAllBandEntries();
		this.uploadPerformance();
	}

	navCallbackHandler(component, MSSWindow = null) {
		MSSWindow
			? this.setState({
					currentComponent: component,
					currentMyShotScopeWindow: MSSWindow
			  })
			: this.setState({ currentComponent: component });
	}

	renderSupportPage() {
		return (
			<div
				style={{
					marginTop: 180,
					textAlign: "center",
					width: 300,
					marginLeft: "auto",
					marginRight: "auto"
				}}
			>
				<div
					style={{
						fontFamily: "Roboto",
						fontWeight: "bold",
						fontSize: 16,
						color: "#8C8C8C"
					}}
				>
					Having issues?
				</div>
				<div
					style={{
						fontFamily: "Roboto",
						fontSize: 14,
						fontWeight: "bold",
						color: "#8C8C8C"
					}}
				>
					Head to our support site for all the information about your Shot
					Scope.
				</div>

				<button
					style={{ marginTop: 30 }}
					onClick={() => {
						this.openSupportWindow();
					}}
				>
					Support Website
				</button>
			</div>
		);
	}

	openSupportWindow() {
		shell.openExternal("https://www.shotscope.com/support/");
	}

	renderV1Connected() {
		if (this.props.band.productId == PRODUCT_ID_V1) {
			return (
				<AlertDialog
					noCancel
					header={"Unsupported Device"}
					content={
						"An unsupported Shot Scope device is connected. Please download the previous version of the desktop app to manage your Shot Scope V1 device.\n\nPlease disconnect the device to close this dialog."
					}
				/>
			);
		}
	}

	render() {
		if (this.props.persistent.nationalities.length > 0) {
			if (this.props.user.token == "") {
				return (
					<LoginRegister
						nationalitiesArray={this.props.persistent.nationalities}
					/>
				);
				// TODO: finish onboarding
				// } else if (this.props.user.displayOnBoarding) return <OnBoarding />;
			} else
				return (
					<div style={{ height: "100vh" }}>
						{this.renderV1Connected()}
						<BandStatusBar />
						<div
							style={{
								display: "flex",
								width: "100vw",
								height: "100%"
							}}
						>
							{this.props.band.connected ? (
								isHigherFirmwareVersion(
									this.props.band.info.firmwareVersion,
									this.props.dataReducer.serverFirmwareVersion
								) &&
								this.props.user.showNewFirmwareUpdateAlert &&
								!this.props.dataReducer.disableFirmwareAlert ? (
									<AlertDialog
										secondaryAction={() => {
											WebAPIHandler.getFirmware(
												this.props.dataReducer.serverFirmwareVersion
											).then(result => {
												this.props.setFirmwareAlert(false);
												this.setState({ firmwareUpdating: true });
												SerialHandler.loadFirmware(
													result,
													this.firmwareCompletionCallback
												);
											});
										}}
										dismissCallback={() => {
											this.props.setFirmwareAlert(false);
										}}
										header="New Firmware Update Available"
										content="A new firmware update is available for your Shot Scope band. It is recommended to update your firmware to improve device performance."
									/>
								) : null
							) : null}

							{this.state.firmwareUpdating ? (
								<AlertDialog
									dismissCallback={() => {
										SerialHandler.cancel();
										this.setState({ firmwareUpdating: false });
									}}
									header="Updating the firmware on your band"
									content={
										<div
											style={{
												display: "flex",
												justifyContent: "center"
											}}
										>
											<div className="actionSpinnerImage">
												<img
													style={{ width: 100, height: 100 }}
													src={require("../../resources/Syncing/Sync.png")}
												/>
											</div>
										</div>
									}
								/>
							) : null}

							{this.state.renderFirmwareSuccess ? (
								<AlertDialog
									dismissCallback={() => {
										SerialHandler.progressCallback({
											percent: 0,
											action: ""
										});
										this.setState({ renderFirmwareSuccess: false });
									}}
									header={
										<div>
											<img
												style={{ marginRight: 8 }}
												src={require("../../resources/GreenCheck.png")}
											/>
											Firmware Update Complete
										</div>
									}
									content="Please wait for the band to finish updating and unplug and replug your band."
								/>
							) : null}
							{this.state.renderFirmwareFailed ? (
								<AlertDialog
									dismissCallback={() => {
										SerialHandler.progressCallback({
											percent: 0,
											action: ""
										});
										this.setState({ renderFirmwareFailed: false });
									}}
									header="Firmware Update Failed"
									content="Something went wrong transferring data to your Shot Scope band. Please restart your band and try again. If the problem persists contact support."
								/>
							) : null}
							<div>
								<NavPanel
									currentComponent={this.state.currentComponent}
									currentMyShotScopeWindow={this.state.currentMyShotScopeWindow}
									navCallback={this.navCallbackHandler}
								/>
							</div>
							<div
								style={{
									marginLeft: 37,
									marginRight: 37,
									marginTop: 25,
									width: "100%",
									height: "100%"
								}}
							>
								{/* <OnBoarding /> */}
								{this.state.currentComponent == "MyShotScope" ? (
									<MyShotScope
										currentMyShotScopeWindow={
											this.state.currentMyShotScopeWindow
										}
									/>
								) : null}
								{this.state.currentComponent == "Courses" ? <Courses /> : null}
								{this.state.currentComponent == "Profile" ? <Profile /> : null}
								{this.state.currentComponent == "Support"
									? this.renderSupportPage()
									: null}
							</div>
						</div>
					</div>
				);
		} else return <LoadingPage />;
	}
}

function mapStateToProps(state, props) {
	return {
		user: state.userReducer,
		dataReducer: state.dataReducer,
		band: state.bandReducer,
		persistent: state.persistentReducer,
		performance: state.performanceReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

App = connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
module.exports = App;
