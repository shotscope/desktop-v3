import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Badge from "./utils/Badge";
import WebAPIHandler from "../utils/WebAPIHandler";
import SerialHandler, { GET_TIMESTAMP } from "../utils/SerialHandler";
import DropDown from "./utils/DropDown";
import * as Actions from "../actions"; // Import your actions
import * as util from "../utils/util";
import AlertDialog from "./utils/AlertDialog";
import Mailer from "../utils/Mailer";
import { store } from "../store";

const COURSE_SYNC_LIMIT = 20;

var dropDownEnums = {
	ALL: 1,
	ON_DEVICE: 2,
	SUBSCRIBED: 3,
	READY: 4,
	properties: {
		1: { name: "All Courses", value: 1 },
		2: { name: "On Device", value: 2 },
		3: { name: "Subscribed", value: 3 },
		4: { name: "Ready To Sync", value: 4 }
	}
};
var unsubscribe = null;
var currentBandPLState = true;

class Courses extends React.PureComponent {
	constructor(props) {
		super(props);
		props.setLoading(true);
		var dropDown = [
			dropDownEnums.properties[dropDownEnums.ALL].name,
			dropDownEnums.properties[dropDownEnums.ON_DEVICE].name,
			dropDownEnums.properties[dropDownEnums.SUBSCRIBED].name,
			dropDownEnums.properties[dropDownEnums.READY].name
		];
		if (props.band.connected)
			if (props.band.pLTimestamp <= 0)
				var dropDown = [
					dropDownEnums.properties[dropDownEnums.ALL].name,
					dropDownEnums.properties[dropDownEnums.ON_DEVICE].name,
					dropDownEnums.properties[dropDownEnums.READY].name
				];
		this.state = {
			currentWindow: "Courses",
			renderSyncFailed: false,
			renderSyncSuccess: false,
			busy: false,
			showRequestCourseModal: false,
			selectedPendingCourses: [],
			selectedUnmappedCourses: [],
			requestCourseName: "",
			requestCourseURL: "",
			requestCourseCity: "",
			requestCourseState: "",
			requestCourseHoles: 18,
			courseFilter: "",
			countryFilter: "",
			stateFilter: "",
			dropDownValues: dropDown,
			dropDownSelection: 0,
			displayNearby: false,
			gettingCourseData: false,
			renderedCourseItems: 0,
			syncLimitReached: false,
			displaySyncLimitReached: false,
			displayNoHomeCourseSelected: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.renderCourseState = this.renderCourseState.bind(this);
		this.renderPendingCourses = this.renderPendingCourses.bind(this);
		this.renderUpdateCourses = this.renderUpdateCourses.bind(this);
		this.renderUnmappedCourses = this.renderUnmappedCourses.bind(this);
		this.handlePendingCourseSelect = this.handlePendingCourseSelect.bind(this);
		this.changeDropDownSelection = this.changeDropDownSelection.bind(this);
		this.checkCourseExistsNearby = this.checkCourseExistsNearby.bind(this);
		this.syncCourses = this.syncCourses.bind(this);
		this.syncCompletionCallback = this.syncCompletionCallback.bind(this);
		this.checkCourseUpdateAvailable = this.checkCourseUpdateAvailable.bind(
			this
		);
		this.addCourse = this.addCourse.bind(this);
		this.removeCourse = this.removeCourse.bind(this);
		this.toggleDisplaySyncFailed = this.toggleDisplaySyncFailed.bind(this);
		this.renderBadge = this.renderBadge.bind(this);
		this.changeCountryDropDownSelection = this.changeCountryDropDownSelection.bind(
			this
		);
		this.changeStateDropDownSelection = this.changeStateDropDownSelection.bind(
			this
		);
		this.renderSubscribe = this.renderSubscribe.bind(this);
		this.onRequestedCourseSubmit = this.onRequestedCourseSubmit.bind(this);
		this.clearFilters = this.clearFilters.bind(this);
		this.enableSync = this.enableSync.bind(this);
		this.handleBandPL = this.handleBandPL.bind(this);
		unsubscribe = store.subscribe(this.handleBandPL);
		this.refreshCourses = this.refreshCourses.bind(this);
	}

	async componentDidMount() {
		for (var userCourse of this.props.dataReducer.userCourses) {
			if (this.checkCourseUpdateAvailable(userCourse)) {
				userCourse.updateAvailable = true;
			} else {
				userCourse.updateAvailable = false;
			}
		}
		this.refreshCourses();
		this.props.setLoading(false);
	}

	componentWillUnmount() {
		if (unsubscribe != null) unsubscribe();
	}

	// Subscribes to the store to listen for the band being PL capable
	handleBandPL() {
		//Subscribe the current user token redux state to currentTokenValue
		let previousBandPLState = currentBandPLState;
		if (this.props.band.connected) {
			currentBandPLState = this.props.band.pLTimestamp > 0;
			if (currentBandPLState != previousBandPLState) {
				if (currentBandPLState > 0)
					this.setState({
						dropDownValues: [
							dropDownEnums.properties[dropDownEnums.ALL].name,
							dropDownEnums.properties[dropDownEnums.ON_DEVICE].name,
							dropDownEnums.properties[dropDownEnums.SUBSCRIBED].name,
							dropDownEnums.properties[dropDownEnums.READY].name
						]
					});
				else
					this.setState({
						dropDownValues: [
							dropDownEnums.properties[dropDownEnums.ALL].name,
							dropDownEnums.properties[dropDownEnums.ON_DEVICE].name,
							dropDownEnums.properties[dropDownEnums.READY].name
						]
					});
			}
		}
	}

	checkCourseExistsOnBand(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (
					this.searchArray(courseItem.id, this.props.band.courseIDs) != -1 ||
					this.props.band.pLCourses.indexOf(courseItem.id) != -1
				)
					return true;
				else return false;
			else if (
				this.searchArray(courseItem.courseID, this.props.band.courseIDs) !=
					-1 ||
				this.props.band.pLCourses.indexOf(courseItem.courseID) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	renderBadge() {
		if (this.props.band.connected) {
			var pendingCount = 0;

			for (var course of this.props.dataReducer.userCourses) {
				if (!this.checkCourseExistsOnBand(course) || course.updateAvailable)
					pendingCount++;
			}
			if (pendingCount >= COURSE_SYNC_LIMIT && !this.state.syncLimitReached)
				this.setState({ syncLimitReached: true });
			else if (pendingCount < COURSE_SYNC_LIMIT && this.state.syncLimitReached)
				this.setState({ syncLimitReached: false });
			if (
				pendingCount > 0 &&
				this.props.band.courseIDsCount == this.props.band.courseIDs.length &&
				this.props.band.courseIDsCount != 0
			)
				return <Badge content={pendingCount} />;
			else return null;
		}
	}

	enableSync() {
		if (this.props.band.connected) {
			for (var course of this.props.dataReducer.downloadedCourses) {
				if (
					!this.checkCourseExistsOnBand(course) ||
					this.checkCourseUpdateAvailable(course)
				)
					return true;
			}
		} else return false;
	}

	checkCourseExistsOnUser(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (
					this.searchArray(courseItem.id, this.props.dataReducer.userCourses) !=
					-1
				)
					return true;
				else return false;
			else if (
				this.searchArray(
					courseItem.courseID,
					this.props.dataReducer.userCourses
				) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	checkCourseUpdateAvailable(courseItem) {
		if (this.props.band.connected) {
			if (this.props.band.pLTimestamp == 0) {
				if (
					this.checkCourseExistsOnBand(courseItem) &&
					this.checkCourseExistsOnUser(courseItem)
				) {
					if (courseItem.id) {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (courseItem.id == userCourse.courseID) {
								if (
									Date.parse(new Date(userCourse.lastUpdateDate)) >
									this.props.band.timestamp * 1000 + 1000
								)
									return true;
							}
						}
						return false;
					} else {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (courseItem.courseID == userCourse.courseID) {
								if (
									Date.parse(new Date(userCourse.lastUpdateDate)) >
									this.props.band.timestamp * 1000 + 1000
								)
									return true;
							}
						}
						return false;
					}
				} else return false;
			} else {
				// Preloaded bands
				if (
					this.checkCourseExistsOnBand(courseItem) &&
					this.checkCourseExistsOnUser(courseItem)
				) {
					if (courseItem.id) {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (userCourse.courseID == courseItem.id) {
								for (var bandCourse of this.props.band.courseIDs) {
									if (userCourse.courseID == bandCourse.id) {
										if (
											Date.parse(new Date(userCourse.lastUpdateDate)) >
											bandCourse.lastUpdated * 1000 + 1000
										)
											return true;
										else return false;
									}
								}
								// check preloaded courses if the course is not on the band
								for (var pLCourse of this.props.band.pLCourses) {
									if (pLCourse == courseItem.id) {
										for (var serverPLCourse of this.props.persistent.courses) {
											if (serverPLCourse.preLoaded != null) {
												if (
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														Date.parse(new Date(serverPLCourse.preLoaded)) &&
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														this.props.band.pLTimestamp * 1000 + 1000
												)
													return true;
											}
										}
									}
								}
							}
						}
						return false;
					} else {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (userCourse.courseID == courseItem.courseID) {
								for (var bandCourse of this.props.band.courseIDs) {
									if (userCourse.courseID == bandCourse.id) {
										if (
											Date.parse(new Date(userCourse.lastUpdateDate)) >
											bandCourse.lastUpdated * 1000 + 1000
										)
											return true;
										else return false;
									}
								}
								// check preloaded courses if the course is not on the band
								for (var pLCourse of this.props.band.pLCourses) {
									if (pLCourse == courseItem.courseID) {
										for (var course of this.props.persistent.courses) {
											if (course.preLoaded != null) {
												if (
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														Date.parse(new Date(course.preLoaded)) &&
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														this.props.band.pLTimestamp * 1000 + 1000
												)
													return true;
											}
										}
									}
								}
							}
						}
						return false;
					}
				} else return false;
			}
		} else return false;
	}

	checkCourseExistsNearby(courseItem) {
		if (this.props.band.connected) {
			if (
				this.searchArray(courseItem, this.props.dataReducer.nearbyCourses) != -1
			) {
				return true;
			} else return false;
		} else return false;
	}

	searchArray(searchElement, array) {
		if (array != undefined) {
			for (var item of array) {
				if (item.id != undefined) {
					if (searchElement == item.id) return true;
					else if (searchElement == item.courseID) return true;
					else if (searchElement.id != undefined) {
						if (searchElement.id == item.id) return true;
					} else if (searchElement == item.courseID) return true;
				} else if (searchElement == item || searchElement == item.courseID)
					return true;
			}
			return -1;
		} else return -1;
	}

	renderBandCourses() {
		if (this.props.persistent.courses != []) {
			const unfilteredCourseItems = this.props.persistent.courses
				.filter(course =>
					course.address
						? (course.name + course.address)
								.toLowerCase()
								.includes(this.state.courseFilter.toLowerCase())
						: course.name
								.toLowerCase()
								.includes(this.state.courseFilter.toLowerCase())
				)
				.filter(course => {
					if (
						this.state.countryFilter == "United States" &&
						course.country == "United States"
					) {
						// There are null course states in the data for some reason
						if (course.state) {
							if (course.state.includes(this.state.stateFilter)) return true;
						}
					} else if (
						course.country
							.toLowerCase()
							.includes(this.state.countryFilter.toLowerCase())
					)
						return true;
				})
				.filter(course => {
					// Filter courses by the drop down selection
					switch (this.state.dropDownSelection) {
						case dropDownEnums.properties[dropDownEnums.ALL].name: {
							// All courses
							return true;
						}
						case dropDownEnums.properties[dropDownEnums.ON_DEVICE].name: // Courses on band
							if (this.checkCourseExistsOnBand(course)) {
								return true;
							} else return false;
						case dropDownEnums.properties[dropDownEnums.SUBSCRIBED].name: // Subscribed courses
							if (this.checkCourseExistsOnUser(course)) {
								return true;
							} else return false;
						case dropDownEnums.properties[dropDownEnums.READY].name: // Ready to sync
							if (
								(!this.checkCourseExistsOnBand(course) &&
									this.checkCourseExistsOnUser(course)) ||
								course.updateAvailable
							) {
								return true;
							} else return false;
						default:
							return false;
					}
				})
				.filter(course => {
					if (course.repeat == true) return false;
					if (!this.state.displayNearby) {
						return true;
					} else if (this.checkCourseExistsNearby(course)) {
						return true;
					} else return false;
				});
			const courseItems = unfilteredCourseItems
				.slice() // Copy the array so we're not modifying the original data
				.splice(0, 100) // Display maximum 100 courses
				.map((course, i) => (
					<div key={i} style={styles.itemContainer}>
						<div
							style={{
								flex: 50,
								marginRight: 8,
								textAlign: "left",
								marginTop: 4,
								marginBottom: 4
							}}
						>
							<div style={{ fontWeight: "bold", fontSize: 16 }}>
								{course.name}
							</div>
							{course.address}
						</div>

						{course.updateAvailable ? (
							<div
								style={{
									flex: 16,
									marginRight: 8,
									width: 120,
									display: "flex",
									alignItems: "center"
								}}
							>
								<Badge content={"!"} />
								<div style={{ marginLeft: 6, fontWeight: "bold" }}>
									Update available
								</div>
							</div>
						) : (
							<div
								style={{
									flex: 16,
									marginRight: 8,
									width: 120,
									alignItems: "center"
								}}
							/>
						)}
						<div
							style={{
								flex: 16,
								marginRight: 8,
								width: 120,
								alignItems: "center"
							}}
						>
							{this.renderCourseState(course)}
						</div>
						<div
							style={{
								flex: 16,
								width: 120,
								marginRight: 8,
								alignItems: "center"
							}}
						>
							{this.renderSubscribe(course)}
						</div>
					</div>
				));
			return { filtered: courseItems, unfiltered: unfilteredCourseItems };
		}
	}

	renderSubscribe(course) {
		if (
			!this.checkCourseExistsOnBand(course) &&
			!this.checkCourseExistsOnUser(course)
		)
			return null;
		else
			return (
				<div style={styles.columnRowContent}>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						{this.checkCourseExistsOnBand(course) ? (
							this.props.band.pLTimestamp > 0 ? (
								<div>Subscribed</div>
							) : this.checkCourseExistsOnUser(course) ? (
								<div>Remove</div>
							) : null
						) : (
							<div>Cancel Sync</div>
						)}
					</div>
					{this.checkCourseExistsOnUser(course) &&
					this.checkCourseExistsOnBand(course) ? (
						this.props.band.pLTimestamp > 0 ? (
							<img
								style={{ cursor: "pointer" }}
								onClick={() => {
									this.removeCourse(course.id);
								}}
								src={require("../../resources/check_on.png")}
							/>
						) : (
							<img
								style={{ cursor: "pointer" }}
								onClick={() => {
									this.removeCourse(course.id);
								}}
								src={require("../../resources/download.png")}
							/>
						)
					) : this.checkCourseExistsOnUser(course) ? (
						<img
							style={{ cursor: "pointer" }}
							onClick={() => {
								this.removeCourse(course.id);
							}}
							src={require("../../resources/download.png")}
						/>
					) : this.props.band.pLTimestamp > 0 ? (
						<img
							style={{ cursor: "pointer" }}
							onClick={() => {
								!this.state.syncLimitReached
									? this.addCourse(course.id)
									: this.setState({ displaySyncLimitReached: true });
							}}
							src={require("../../resources/emptyBox.png")}
						/>
					) : null}
				</div>
			);
	}

	renderCourseState(course) {
		if (this.checkCourseExistsOnBand(course))
			if (this.props.band.pLTimestamp > 0)
				return (
					<div style={styles.columnRowContent}>
						<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
							On device
						</div>
						<img src={require("../../resources/GreenCheck.png")} />
					</div>
				);
			else if (!this.checkCourseExistsOnUser(course))
				return (
					<div style={styles.columnRowContent}>
						<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
							Add
						</div>{" "}
						<div
							onClick={() => {
								!this.state.syncLimitReached
									? this.addCourse(course.id)
									: this.setState({ displaySyncLimitReached: true });
							}}
							style={{ cursor: "pointer" }}
						>
							<img src={require("../../resources/+.png")} />
						</div>
					</div>
				);
			else
				return (
					<div style={styles.columnRowContent}>
						<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
							On device
						</div>
						<img src={require("../../resources/GreenCheck.png")} />
					</div>
				);
		else if (
			!this.checkCourseExistsOnBand(course) &&
			this.checkCourseExistsOnUser(course)
		)
			return (
				<div style={styles.columnRowContent}>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						Ready to Sync
					</div>{" "}
					<div>
						<img src={require("../../resources/Update.png")} />
					</div>
				</div>
			);
		else
			return (
				<div style={styles.columnRowContent}>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						Add
					</div>{" "}
					<div
						onClick={() => {
							!this.state.syncLimitReached
								? this.addCourse(course.id)
								: this.setState({ displaySyncLimitReached: true });
						}}
						style={{ cursor: "pointer" }}
					>
						<img src={require("../../resources/+.png")} />
					</div>
				</div>
			);
	}

	async addCourse(courseID) {
		await WebAPIHandler.addCourse(courseID).then(response => {
			if (response.ok) {
				WebAPIHandler.getUserCoursesLite();
			}
		});
	}

	async removeCourse(courseID) {
		await WebAPIHandler.deleteCourse(courseID).then(response => {
			if (response.ok) {
				var newCourseArray = this.props.dataReducer.userCourses.slice();
				for (var userCourse of newCourseArray) {
					if (courseID == userCourse.courseID) {
						newCourseArray.splice(newCourseArray.indexOf(userCourse), 1);
						break;
					}
				}
				this.props.setUserCoursesLite(newCourseArray);
			}
		});
	}

	changeDropDownSelection(key, selection) {
		console.log(key, selection);
		this.setState({
			dropDownSelection: selection
		});
	}

	changeCountryDropDownSelection(key, selection) {
		this.setState({
			countryFilter: selection
		});
	}

	changeStateDropDownSelection(key, selection) {
		console.log(selection);
		this.setState({
			stateFilter: selection
		});
	}

	async refreshCourses() {
		console.log("refresh");
		WebAPIHandler.getUserCoursesLite().then(response => {
			console.log(WebAPIHandler.store.getState().dataReducer);
			for (var course of this.props.dataReducer.userCourses) {
				if (this.checkCourseUpdateAvailable(course)) {
					course.updateAvailable = true;
					for (var persistentCourse of this.props.persistent.courses)
						if (persistentCourse.id == course.courseID) {
							persistentCourse.updateAvailable = true;
							break;
						}
				} else {
					course.updateAvailable = false;
					for (var persistentCourse of this.props.persistent.courses)
						if (persistentCourse.id == course.courseID) {
							persistentCourse.updateAvailable = false;
							break;
						}
				}
			}
		});
	}

	renderCourses() {
		return (
			<div style={{ height: "100%" }}>
				<div
					style={{
						display: "flex",
						width: "100%",
						verticalAlign: "middle"
					}}
				>
					<DropDown
						changeDropDownSelection={this.changeDropDownSelection}
						initialSelection={0}
						data={this.state.dropDownValues}
						onRef={ref => {
							this.courseFilterRef = ref;
						}}
						entriesLimit={100}
					/>
					<div
						style={Object.assign({}, styles.itemContainer, {
							flexGrow: 1,
							height: 44,
							minHeight: 44,
							display: "flex",
							alignItems: "center"
						})}
					>
						<img
							style={{ marginRight: 15 }}
							src={require("../../resources/search.png")}
						/>
						<input
							name="courseFilter"
							value={this.state.courseFilter}
							onChange={this.handleChange}
							style={{
								flexGrow: 1,
								height: "100%",
								margin: 0,
								padding: 0,
								border: 0,
								borderRadius: 4
							}}
						/>
					</div>
				</div>
				<div
					style={{
						display: "flex",
						width: "100%",
						justifyContent: "flex-end",
						alignItems: "center",
						marginTop: 11,
						marginBottom: 17,
						fontSize: 12,
						fontWeight: 500,
						color: "#8C8C8C"
					}}
				>
					{this.state.currentWindow == "Courses" &&
					(this.state.courseFilter != "" ||
						this.state.countryFilter != "" ||
						this.state.stateFilter != "" ||
						this.state.displayNearby != "") ? (
						<div
							style={{ cursor: "pointer", marginRight: 16 }}
							onClick={() => {
								this.clearFilters();
							}}
						>
							<div style={{ display: "flex", alignItems: "center" }}>
								<div
									style={Object.assign({}, styles.mainContentHeader, {
										marginRight: 8,
										marginBottom: 0
									})}
								>
									Clear Filters
								</div>
								<img src={require("../../resources/download.png")} />
							</div>
						</div>
					) : null}
					<DropDown
						changeDropDownSelection={this.changeCountryDropDownSelection}
						data={this.props.persistent.countries}
						showFilter={true}
						onRef={ref => {
							this.countryFilterRef = ref;
						}}
						placeholder={"Country"}
					/>
					{this.state.countryFilter == "United States" ? (
						<div style={{ marginRight: -10 }}>
							<DropDown
								changeDropDownSelection={this.changeStateDropDownSelection}
								data={this.props.persistent.USStates}
								onRef={ref => {
									this.stateFilterRef = ref;
								}}
								showFilter={true}
								placeholder={"State"}
							/>
						</div>
					) : (
						<div style={{ marginRight: -10 }}>
							<DropDown
								changeDropDownSelection={this.changeStateDropDownSelection}
								data={this.props.persistent.USStates}
								onRef={ref => {
									this.stateFilterRef = ref;
								}}
								disabled={
									this.state.countryFilter == "United States" ? false : true
								}
								showFilter={true}
								placeholder={"State"}
							/>
						</div>
					)}
				</div>
				<div
					style={Object.assign({}, styles.mainContentHeader, {
						display: "flex",
						justifyContent: "space-between"
					})}
				>
					<div style={{ display: "flex", justifyContent: "center" }}>
						<div>
							Displaying {this.renderBandCourses().filtered.length} of{" "}
							{this.renderBandCourses().unfiltered.length} courses
						</div>
						<div
							style={{ marginLeft: 6 }}
							onClick={() => this.refreshCourses()}
						>
							<img src={require("../../resources/Syncing/UpdateGrey.png")} />
						</div>
					</div>
					<div
						style={{ cursor: "pointer", marginRight: 20 }}
						onClick={() => {
							if (this.props.user.homeCourseID != null) {
								this.clearFilters();
								this.setState({ displayNearby: !this.state.displayNearby });
							} else {
								this.setState({ displayNoHomeCourseSelected: true });
							}
						}}
					>
						{this.state.displayNearby ? (
							<div>View All</div>
						) : (
							<div style={{ display: "flex", alignItems: "center" }}>
								<img src={require("../../resources/Location.png")} />
								<div style={{ marginLeft: 8 }}>Nearby Location</div>
							</div>
						)}
					</div>
				</div>
				<div
					style={{
						display: "flex",
						flexDirection: "column",
						left: 250,
						bottom: 0,
						right: 40,
						top: 320,
						position: "absolute",
						marginBottom: 30
					}}
				>
					<div style={styles.mainContent}>
						{this.renderBandCourses().filtered}
					</div>
				</div>
			</div>
		);
	}

	renderConnectBand() {
		return (
			<div
				style={{
					marginLeft: "auto",
					width: "100",
					justifyContent: "center",
					marginRight: "auto",
					display: "flex",
					paddingBottom: 120
				}}
			>
				<img
					style={{ alignSelf: "center", marginRight: 13 }}
					src={require("../../resources/Band.png")}
				/>
				<img
					style={{ alignSelf: "center" }}
					src={require("../../resources/USB.png")}
				/>
				<img
					style={{ alignSelf: "center" }}
					src={require("../../resources/Laptop.png")}
				/>
			</div>
		);
	}

	renderSync() {
		return !this.state.gettingCourseData ? (
			<div>
				<div
					style={{
						marginBottom: 8,
						display: "flex",
						justifyContent: "space-between"
					}}
				>
					<div
						style={{
							fontSize: 14,
							fontWeight: "bold",
							color: "#8C8C8C"
						}}
					>
						Ready to sync
					</div>
					{this.props.band.connected &&
					this.props.band.pLTimestamp > 0 &&
					false ? (
						<div>Select all</div>
					) : null}
				</div>
				{this.props.band.connected ? (
					<div
						style={{
							display: "flex",
							flexDirection: "column",
							left: 250,
							bottom: 0,
							right: 40,
							top: 200,
							position: "absolute",
							marginBottom: 30
						}}
					>
						<div
							style={{
								width: "100%",
								display: "flex",
								justifyContent: "space-between",
								backgroundColor: "#5B5B5B",
								height: 30,
								color: "white",
								fontSize: 12,
								fontWeight: "bold",
								alignItems: "center"
							}}
						>
							<div style={styles.tableHeader}>Pending</div>
							<div style={styles.tableHeader}>Updates</div>
							<div style={styles.tableHeader}>Requested</div>
						</div>
						<div
							style={{
								width: "100%",
								backgroundColor: "white",
								marginBottom: 12,
								height: "100%",
								overflowY: "scroll"
							}}
						>
							<div
								style={{
									padding: 12,
									display: "flex"
								}}
							>
								<div style={styles.courseEntryColumn}>
									{!this.state.busy && this.renderPendingCourses()}
								</div>
								<div
									style={Object.assign({}, styles.courseEntryColumn, {
										paddingLeft: 12
									})}
								>
									{!this.state.busy && this.renderUpdateCourses()}
								</div>
								<div
									style={Object.assign({}, styles.courseEntryColumn, {
										border: "none",
										paddingRight: 0,
										marginLeft: 12
									})}
								>
									{!this.state.busy && this.renderUnmappedCourses()}
								</div>
							</div>
						</div>
						<div
							style={{
								width: "100%",
								display: "flex",
								justifyContent: "flex-end"
							}}
						>
							<button
								style={
									!this.enableSync()
										? Object.assign(
												{},
												{ width: 120, backgroundColor: "#999999" }
										  )
										: Object.assign(
												{},
												{
													width: 120,
													backgroundColor: "#26CA85",
													cursor: "pointer"
												}
										  )
								}
								onClick={() => {
									this.setState({ busy: true });
									this.syncCourses();
								}}
								disabled={!this.enableSync()}
							>
								SYNC
							</button>
						</div>
					</div>
				) : (
					this.renderConnectBand()
				)}
			</div>
		) : (
			<div
				style={{
					height: "100%",
					alignItems: "center",
					display: "flex",
					justifyContent: "center"
				}}
			>
				<div
					style={{
						display: "flex",
						justifyContent: "center",
						marginBottom: 180
					}}
				>
					<div className="actionSpinnerImage">
						<img
							style={{ width: 100, height: 100 }}
							src={require("../../resources/Syncing/Sync.png")}
						/>
					</div>
				</div>
			</div>
		);
	}

	toggleDisplaySyncFailed() {
		this.setState({ renderSyncFailed: !this.state.renderSyncFailed });
	}

	syncCompletionCallback(success) {
		this.setState({ busy: false });
		if (success == true) {
			this.setState({ renderSyncSuccess: true, currentWindow: "Courses" });
			this.props.clearDownloadedCourseData();
			this.props.setLastCourseSyncDateTime(new Date(Date.now()).toISOString());
			SerialHandler.parseUART(GET_TIMESTAMP);
			SerialHandler.getAllBandCourses();
			for (var course of this.props.persistent.courses) {
				course.updateAvailable = false;
			}
		} else if (success == false) this.toggleDisplaySyncFailed();
	}

	async syncCourses() {
		// Remove all courses that the user has selected not to sync
		for (var course of this.state.selectedPendingCourses) {
			await WebAPIHandler.deleteCourse(course.id);
		}
		if (this.props.dataReducer.downloadedCourses.length > 0) {
			var coursesToBand = [];
			for (var downloadedCourse of this.props.dataReducer.downloadedCourses) {
				if (
					!this.checkCourseExistsOnBand(downloadedCourse) ||
					this.checkCourseUpdateAvailable(downloadedCourse)
				) {
					coursesToBand.push(downloadedCourse);
				}
			}
			if (this.props.band.pLTimestamp == 0) {
				SerialHandler.loadCourses(
					coursesToBand,
					this.props.dataReducer.downloadedCoursesIndex,
					this.syncCompletionCallback
				);
			} else {
				SerialHandler.loadCourses(
					coursesToBand,
					null,
					this.syncCompletionCallback
				);
			}
		}
	}

	renderPendingCourses() {
		var _pendingCourses = [];
		for (var course of this.props.persistent.courses) {
			for (var userCourse of this.props.dataReducer.userCourses)
				if (
					course.id == userCourse.courseID &&
					!this.checkCourseExistsOnBand(course) &&
					this.checkExistsInDownloadedCourses(
						course,
						this.props.dataReducer.downloadedCourses
					)
				) {
					_pendingCourses.push(course);
				}
		}

		const pendingCourses = _pendingCourses.map(course => (
			<div
				key={course.name}
				// onClick={() => this.handlePendingCourseSelect(course)}
				style={
					this.state.selectedPendingCourses.indexOf(course) == -1
						? Object.assign({}, styles.courseEntryContainerSelected)
						: Object.assign({}, styles.courseEntryContainer)
				}
			>
				<div key={course.name} style={styles.courseEntry}>
					<div>{course.name}</div>
					{/* {this.props.band.pLTimestamp > 0 &&
					this.state.selectedPendingCourses.indexOf(course) == -1 ? (
						<img
							style={{ marginRight: 6, marginLeft: 6 }}
							src={require("../../resources/Line 2.png")}
						/>
					) : this.props.band.pLTimestamp > 0 &&
					  this.state.selectedPendingCourses.indexOf(course) != -1 ? (
						<img
							style={{ marginRight: 6, marginLeft: 6 }}
							src={require("../../resources/+.png")}
						/>
					) : null} */}
				</div>
			</div>
		));

		return pendingCourses;
	}

	renderUpdateCourses() {
		var _updateCourses = [];
		for (var course of this.props.persistent.courses)
			if (this.checkCourseUpdateAvailable(course)) {
				_updateCourses.push(course);
			}
		const updateCourses = _updateCourses.map(course => (
			<div
				key={course.name}
				// onClick={() => this.handlePendingCourseSelect(course)}
				style={
					this.state.selectedPendingCourses.indexOf(course) == -1
						? Object.assign({}, styles.courseEntryContainerSelected)
						: Object.assign({}, styles.courseEntryContainer)
				}
			>
				<div key={course.name} style={styles.courseEntry}>
					{course.name}
				</div>
			</div>
		));

		return updateCourses;
	}

	checkExistsInDownloadedCourses(_course, dataList) {
		for (var course of dataList) {
			if (_course.id == course.courseID) return true;
		}
		return false;
	}

	renderUnmappedCourses() {
		var _unmappedCourses = [];
		for (var course of this.props.persistent.courses) {
			for (var userCourse of this.props.dataReducer.userCourses)
				if (
					course.id == userCourse.courseID &&
					!this.checkCourseExistsOnBand(course) &&
					this.checkCourseExistsOnUser(course) &&
					!this.checkExistsInDownloadedCourses(
						course,
						this.props.dataReducer.downloadedCourses
					)
				) {
					_unmappedCourses.push(course);
				}
		}
		const unmappedCourses = _unmappedCourses.map(course => (
			<div
				key={course.name}
				style={Object.assign({}, styles.courseEntryContainer)}
			>
				<div key={course.name} style={styles.courseEntry}>
					<div>{course.name}</div>
					<img
						style={{ marginRight: 6, marginLeft: 6 }}
						src={require("../../resources/iconmonstr-map-5.png")}
					/>
				</div>
			</div>
		));

		return unmappedCourses;
	}

	handlePendingCourseSelect(course) {
		if (this.props.band.pLTimestamp > 0) {
			var selectedPendingCoursesState = this.state.selectedPendingCourses;
			var index = selectedPendingCoursesState.indexOf(course);
			if (index == -1) {
				this.state.selectedPendingCourses.push(course);
			} else this.state.selectedPendingCourses.pop(course);
		}
	}

	handleChange(event) {
		const target = event.target;
		const name = target.name;
		this.setState({ [name]: target.value });
	}

	onRequestedCourseSubmit(event) {
		var requestCourseObj = {
			requestCourseCity: this.state.requestCourseCity,
			requestCourseHoles: this.state.requestCourseHoles,
			requestCourseState: this.state.requestCourseState,
			requestCourseURL: this.state.requestCourseURL,
			requestCourseName: this.state.requestCourseName,
			userEmail: this.props.user.email
		};
		event.preventDefault();
		console.log("HELLO");
		Mailer.requestCourse(requestCourseObj);

		this.setState({
			requestCourseCity: "",
			requestCourseHoles: 18,
			requestCourseState: "",
			requestCourseURL: "",
			requestCourseName: "",
			showRequestCourseModal: false
		});
	}

	renderRequestCourseModal() {
		return (
			<div style={styles.requestCourseModalBackground}>
				<div className="requestCourseModal">
					<div
						style={{
							display: "flex",
							width: "100%",
							justifyContent: "flex-end"
						}}
					>
						<img
							style={{ cursor: "pointer" }}
							src={require("../../resources/download.png")}
							onClick={() => {
								this.setState({ showRequestCourseModal: false });
							}}
						/>
					</div>
					<div style={styles.requestCourseModalBody}>
						<div style={{ height: 28, fontSize: 18, fontWeight: "bold" }}>
							Request a course
						</div>
						<form
							style={styles.requestCourseModalForm}
							onSubmit={this.onRequestedCourseSubmit}
						>
							<p style={styles.requestCourseModalHeaders}>Course Name</p>
							<input
								style={styles.requestCourseModalInput}
								type="text"
								name="requestCourseName"
								placeholder="Course Name"
								required
								value={this.state.requestCourseName}
								onChange={this.handleChange}
							/>
							<p style={styles.requestCourseModalHeaders}>Course URL</p>
							<input
								style={styles.requestCourseModalInput}
								type="text"
								name="requestCourseURL"
								placeholder="Course URL (Optional)"
								value={this.state.requestCourseURL}
								onChange={this.handleChange}
							/>
							<p style={styles.requestCourseModalHeaders}>City</p>
							<input
								style={styles.requestCourseModalInput}
								type="text"
								name="requestCourseCity"
								placeholder="Course City"
								required
								value={this.state.requestCourseCity}
								onChange={this.handleChange}
							/>
							<p style={styles.requestCourseModalHeaders}>Country/State</p>
							<input
								style={styles.requestCourseModalInput}
								type="text"
								name="requestCourseState"
								placeholder="Country/State"
								required
								value={this.state.requestCourseState}
								onChange={this.handleChange}
							/>
							<p style={styles.requestCourseModalHeaders}>Holes</p>
							<input
								style={styles.requestCourseModalInput}
								type="number"
								name="requestCourseHoles"
								placeholder="Holes"
								required
								value={this.state.requestCourseHoles}
								onChange={this.handleChange}
							/>
							<input
								style={
									!(
										this.state.requestCourseHoles > 0 &&
										this.state.requestCourseHoles <= 18
									) ||
									(this.state.requestCourseState == "" ||
										this.state.requestCourseCity == "" ||
										this.state.requestCourseName == "")
										? Object.assign(
												{},
												styles.requestCourseModalFormSubmitDisabled
										  )
										: Object.assign({}, styles.requestCourseModalFormSubmit)
								}
								disabled={
									!(
										this.state.requestCourseHoles > 0 &&
										this.state.requestCourseHoles <= 18
									) ||
									(this.state.requestCourseState == "" ||
										this.state.requestCourseCity == "" ||
										this.state.requestCourseName == "")
								}
								type="submit"
								value="Request"
							/>
						</form>
					</div>
				</div>
			</div>
		);
	}

	clearFilters() {
		this.setState({
			courseFilter: "",
			displayNearby: false
		});
		this.courseFilterRef.changeDropDownSelection("All Courses");
		this.stateFilterRef.changeDropDownSelection("");
		this.countryFilterRef.changeDropDownSelection("");
	}

	render() {
		if (!this.props.band.connected)
			return (
				<div style={{ fontSize: 12, height: "100%" }}>
					{this.state.showRequestCourseModal
						? this.renderRequestCourseModal()
						: null}
					{this.state.busy ? (
						<AlertDialog
							dismissCallback={() => {
								SerialHandler.cancel();
								this.setState({ busy: false });
							}}
							header="Syncing courses to your band"
							content={
								<div
									style={{
										display: "flex",
										justifyContent: "center"
									}}
								>
									<div className="actionSpinnerImage">
										<img
											style={{ width: 100, height: 100 }}
											src={require("../../resources/Syncing/Sync.png")}
										/>
									</div>
								</div>
							}
						/>
					) : null}
					{this.state.renderSyncFailed ? (
						<AlertDialog
							dismissCallback={() => {
								this.toggleDisplaySyncFailed();
								SerialHandler.progressCallback({
									percent: 0,
									action: ""
								});
							}}
							header="Course Sync Failed"
							content="Something went wrong transferring data to your Shot Scope band. Please restart your band and try again. If the problem persists contact support."
						/>
					) : null}
					{this.state.displaySyncLimitReached ? (
						<AlertDialog
							dismissCallback={() => {
								this.setState({ displaySyncLimitReached: false });
							}}
							header="Sync limit exceeded"
							content="You may only sync up to 20 courses at a time. Please sync your queued courses before adding more."
						/>
					) : null}
					{this.state.renderSyncSuccess ? (
						<AlertDialog
							dismissCallback={() => {
								this.setState({ renderSyncSuccess: false });
								SerialHandler.progressCallback({
									percent: 0,
									action: ""
								});
							}}
							header={
								<div>
									<img
										style={{ marginRight: 8 }}
										src={require("../../resources/GreenCheck.png")}
									/>
									Course Sync Complete
								</div>
							}
							content="Your courses have successfully been synced to your Shot Scope band."
						/>
					) : null}
					{this.state.displayNoHomeCourseSelected ? (
						<AlertDialog
							dismissCallback={() => {
								this.setState({ displayNoHomeCourseSelected: false });
							}}
							header={<div>No Home Course Selected</div>}
							content="You do not have a home course selected. Please set your home course in your profile to view nearby courses."
						/>
					) : null}
					<div
						style={{
							fontWeight: "bold",
							fontSize: 18,
							color: "#8C8C8C",
							width: "100%",
							marginBottom: 19
						}}
					>
						<div>Courses</div>
					</div>
					<div
						style={{
							height: "100%",
							display: "flex",
							justifyContent: "center"
						}}
					>
						{this.renderConnectBand()}
					</div>
				</div>
			);
		else
			return (
				<div style={{ fontSize: 12, height: "100%" }}>
					{this.state.showRequestCourseModal
						? this.renderRequestCourseModal()
						: null}
					{this.state.busy ? (
						<AlertDialog
							dismissCallback={() => {
								SerialHandler.cancel();
								this.setState({ busy: false });
							}}
							header="Syncing courses to your band"
							content={
								<div
									style={{
										display: "flex",
										justifyContent: "center"
									}}
								>
									<div className="actionSpinnerImage">
										<img
											style={{ width: 100, height: 100 }}
											src={require("../../resources/Syncing/Sync.png")}
										/>
									</div>
								</div>
							}
						/>
					) : null}
					{this.state.renderSyncFailed ? (
						<AlertDialog
							dismissCallback={() => {
								this.toggleDisplaySyncFailed();
								SerialHandler.progressCallback({
									percent: 0,
									action: ""
								});
							}}
							header="Course Sync Failed"
							content="Something went wrong transferring data to your Shot Scope band. Please restart your band and try again. If the problem persists contact support."
						/>
					) : null}
					{this.state.displaySyncLimitReached ? (
						<AlertDialog
							dismissCallback={() => {
								this.setState({ displaySyncLimitReached: false });
							}}
							header="Sync limit exceeded"
							content="You may only sync up to 20 courses at a time. Please sync your queued courses before adding more."
						/>
					) : null}
					{this.state.renderSyncSuccess ? (
						<AlertDialog
							dismissCallback={() => {
								this.setState({ renderSyncSuccess: false });
								SerialHandler.progressCallback({
									percent: 0,
									action: ""
								});
							}}
							header={
								<div>
									<img
										style={{ marginRight: 8 }}
										src={require("../../resources/GreenCheck.png")}
									/>
									Course Sync Complete
								</div>
							}
							content="Your courses have successfully been synced to your Shot Scope band."
						/>
					) : null}
					<div
						style={{
							fontWeight: "bold",
							fontSize: 18,
							color: "#8C8C8C",
							display: "flex",
							justifyContent: "space-between",
							width: "100%",
							marginBottom: 19
						}}
					>
						<div>Courses</div>
						<div style={{ fontSize: 14, alignSelf: "flex-end" }}>
							Can't find a course?{" "}
							<button
								style={{ marginLeft: 8, cursor: "pointer" }}
								onClick={() => {
									this.setState({ showRequestCourseModal: true });
								}}
							>
								Request Course
							</button>
						</div>
					</div>
					<div
						style={{
							display: "flex",
							marginBottom: 17,
							justifyContent: "space-between",
							alignItems: "center"
						}}
					>
						<div>
							<button
								onClick={() => {
									this.setState({
										currentWindow: "Courses",
										syncComplete: false
									});
								}}
								style={
									this.state.currentWindow == "Courses"
										? Object.assign({}, styles.tabButtonSelected, {
												marginRight: 8
										  })
										: Object.assign({}, styles.tabButton, { marginRight: 8 })
								}
							>
								Courses
							</button>
							<button
								onClick={() => {
									this.setState({
										currentWindow: "Sync",
										gettingCourseData: true
									});
									this.getUserCourseData();
								}}
								style={
									this.state.currentWindow == "Sync"
										? Object.assign({}, styles.tabButtonSelected)
										: Object.assign({}, styles.tabButton)
								}
							>
								<div
									style={{
										display: "flex",
										justifyContent: "center",
										alignItems: "center",
										alignSelf: "flex-end",
										alignItems: "center"
									}}
								>
									<div>Ready to Sync</div>
									<div style={{ marginLeft: 6 }}>{this.renderBadge()}</div>
								</div>
							</button>
						</div>
					</div>
					{this.state.currentWindow == "Courses"
						? this.renderCourses()
						: this.renderSync()}
				</div>
			);
	}
	async getUserCourseData() {
		await WebAPIHandler.getUserCourses().then(response => {
			this.setState({ gettingCourseData: false });
		});
	}
}

const styles = {
	tabButton: {
		backgroundColor: "#EBEBEB",
		border: "2px solid #979797",
		color: "#979797",
		fontWeight: "bold",
		boxShadow: "1px 1px 1px #9E9E9E",
		cursor: "pointer"
	},
	tabButtonSelected: {
		backgroundColor: "#999999",
		border: "2px solid #979797",
		color: "#EBEBEB",
		fontWeight: "bold",
		cursor: "pointer"
	},
	itemContainer: {
		display: "flex",
		backgroundColor: "white",
		minHeight: 60,
		alignItems: "center",
		paddingLeft: 18,
		marginBottom: 4,
		marginRight: 18,
		fontSize: 12,
		fontFamily: "Roboto",
		border: "none",
		boxShadow: "1px 1px 1px #9E9E9E",
		borderRadius: 4
	},
	courseEntryContainer: {
		flex: 1,
		display: "flex",
		alignItems: "center",
		backgroundColor: "#E7E7E7",
		minHeight: 40,
		maxHeight: 60,
		paddingLeft: 6,
		marginBottom: 4,
		alignItems: "center",
		marginRight: 6,
		marginBottom: 6,
		boxShadow: "1px 1px 1px #9E9E9E",
		borderRadius: 4
	},
	courseEntryContainerSelected: {
		flex: 1,
		backgroundColor: "#26CA85",
		minHeight: 40,
		maxHeight: 60,
		paddingLeft: 6,
		display: "flex",
		alignItems: "center",
		marginBottom: 4,
		alignItems: "center",
		marginRight: 6,
		marginBottom: 6,
		boxShadow: "1px 1px 1px #9E9E9E",
		borderRadius: 4
	},
	courseEntry: {
		paddingTop: 3,
		paddingLeft: 6,
		color: "white",
		fontWeight: "normal",
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%"
	},
	courseEntrySelected: {
		paddingTop: 3,
		paddingLeft: 6,
		color: "black",
		fontWeight: "normal",
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%"
	},
	courseEntryColumn: {
		borderRightStyle: "solid",
		borderWidth: 1,
		paddingRight: 12,
		borderColor: "#8C8C8C",
		flex: 1
	},
	tableHeader: {
		paddingLeft: 18,
		paddingRight: 18
	},
	mainContent: {
		overflowY: "scroll",
		flex: "1 1 auto"
	},
	mainContentHeader: {
		marginBottom: 8,
		fontSize: 14,
		fontWeight: "bold",
		color: "#8C8C8C"
	},
	requestCourseModalBackground: {
		position: "fixed",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: "rgba(255,255,255,0.7)",
		paddingTop: 45,
		zIndex: 3
	},
	requestCourseModalBody: {
		paddingTop: 16,
		paddingRight: 60,
		color: "#8C8C8C",
		borderRadius: 4
	},
	requestCourseModalHeaders: {
		marginTop: 12,
		color: "#8C8C8C",
		fontSize: 14,
		fontWeight: "bold",
		marginBottom: 0
	},
	requestCourseModalForm: {
		display: "flex",
		paddingTop: 8,
		flexDirection: "column",
		textAlign: "left",
		justifyContent: "space-between"
	},
	requestCourseModalInput: {
		height: 39,
		border: "1px solid rgba(151,151,151,0.32)",
		color: "#000000",
		paddingLeft: 14,
		marginTop: 5,
		marginBottom: 0,
		borderRadius: 4
	},
	requestCourseModalFormSubmit: {
		height: 30,
		width: 126,
		borderRadius: 2,
		backgroundColor: "#37A8DF",
		color: "#FFFFFF",
		fontFamily: "Roboto",
		fontSize: 14,
		fontWeight: "bold",
		textAlign: "center",
		border: "none",
		marginTop: 19
	},
	requestCourseModalFormSubmitDisabled: {
		height: 30,
		width: 126,
		borderRadius: 2,
		backgroundColor: "gray",
		color: "#FFFFFF",
		fontFamily: "Roboto",
		fontSize: 14,
		fontWeight: "bold",
		textAlign: "center",
		border: "none",
		marginTop: 19
	},
	columnRowContent: {
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		width: 120
	}
};

function mapStateToProps(state, props) {
	return {
		user: state.userReducer,
		band: state.bandReducer,
		dataReducer: state.dataReducer,
		persistent: state.persistentReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

Courses = connect(
	mapStateToProps,
	mapDispatchToProps
)(Courses);
module.exports = Courses;
