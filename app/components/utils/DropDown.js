import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions";

class DropDown extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			stateKey: props.stateKey ? props.stateKey : null,
			dropDownEnabled: "none",
			dropDownValues: props.data,
			dropDownSelection: "",
			entriesLimit: props.entriesLimit ? props.entriesLimit : -1,
			disabled: props.disabled ? props.disabled : false,
			showFilter: props.showFilter ? props.showFilter : false,
			initialSelection:
				props.initialSelection != null ? props.initialSelection : -1,
			filterValue: "",
			placeholder: props.placeholder ? props.placeholder : ""
		};

		this.toggleDropDown = this.toggleDropDown.bind(this);
		this.changeDropDownSelection = this.changeDropDownSelection.bind(this);
		this.setWrapperRef = this.setWrapperRef.bind(this);
		this.setFilterRef = this.setFilterRef.bind(this);
		this.handleClickOutside = this.handleClickOutside.bind(this);
		this.renderStyle = this.renderStyle.bind(this);
		this.filterRef = React.createRef();
		this.wrapperRef = React.createRef();
		this.handleFilterChange = this.handleFilterChange.bind(this);
	}

	toggleDropDown() {
		if (this.state.dropDownEnabled == "none")
			this.setState({ dropDownEnabled: "block" }, () => {
				if (this.props.showFilter) this.filterRef.focus();
			});
		else this.setState({ dropDownEnabled: "none" });
	}

	changeDropDownSelection(selection) {
		console.log(selection);
		this.setState({
			dropDownSelection: selection
		});
		this.props.changeDropDownSelection(this.state.stateKey, selection);
		this.setState({ dropDownEnabled: "none" });
	}

	handleFilterChange(event) {
		const target = event.target;
		this.setState({ filterValue: target.value });
	}

	renderItem(item) {
		if (item < 0) return item.toString().substring(1, item.length);
		else if (item > 0) return "+" + item;
		else return 0;
	}

	renderDropDownItems() {
		if (this.state.dropDownValues) {
			const dropDownItems = this.state.dropDownValues
				.filter(item => {
					if (item && item.name)
						return item.name
							.toString()
							.toLowerCase()
							.includes(this.state.filterValue.toLowerCase());
					else if (item)
						return item
							.toString()
							.toLowerCase()
							.includes(this.state.filterValue.toLowerCase());
					else return true;
				})
				.map((item, i) => (
					<div
						name={item}
						onClick={() => this.changeDropDownSelection(item)}
						style={styles.itemContainer}
						key={i}
					>
						{this.props.displayNumberSign ? this.renderItem(item) : item}
					</div>
				));
			return dropDownItems;
		}
	}

	componentDidMount() {
		document.addEventListener("mouseup", this.handleClickOutside);
		if (this.props.onRef) this.props.onRef(this);
		if (
			this.props.initialSelection != undefined &&
			this.props.initialSelection != -1 &&
			this.state.dropDownValues
		) {
			this.changeDropDownSelection(
				this.state.dropDownValues[this.props.initialSelection]
			);
		}
	}

	componentWillUnmount() {
		document.removeEventListener("mouseup", this.handleClickOutside);
		if (this.props.onRef) this.props.onRef(undefined);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.disabled != this.state.disabled) {
			this.setState({ disabled: nextProps.disabled });
		}
		if (nextProps.data != this.state.dropDownValues) {
			this.setState({ dropDownValues: nextProps.data }, () => {
				if (
					this.state.initialSelection != undefined &&
					this.state.initialSelection != -1
				) {
					this.changeDropDownSelection(
						this.state.dropDownValues[this.props.initialSelection]
					);
				}
			});
		}
		if (nextProps.placeholder != this.state.placeholder) {
			this.setState({ placeholder: nextProps.placeholder });
		}
	}

	handleClickOutside(event) {
		if (this.state.dropDownEnabled == "block")
			if (
				this.filterRef != { current: null } &&
				this.state.showFilter == true
			) {
				if (
					!this.filterRef.contains(event.target) &&
					!this.wrapperRef.contains(event.target)
				)
					this.setState({ dropDownEnabled: "none" });
			} else if (!this.wrapperRef.contains(event.target))
				this.setState({ dropDownEnabled: "none" });
	}

	setWrapperRef(node) {
		this.wrapperRef = node;
	}

	setFilterRef(node) {
		this.filterRef = node;
	}

	renderStyle() {
		if (!this.props.disabled) {
			if (this.props.containerStyle)
				return Object.assign({}, this.props.containerStyle);
			else
				return Object.assign({}, styles.itemContainer, {
					marginRight: 28,
					minWidth: 184,
					whiteSpace: "nowrap",
					borderRadius: 4
				});
		} else {
			return Object.assign({}, styles.itemContainerDisabled, {
				marginRight: 28,
				minWidth: 184,
				whiteSpace: "nowrap",
				borderRadius: 4
			});
		}
	}

	render() {
		return (
			<div
				className="dropdown"
				onClick={
					!this.state.disabled && this.state.dropDownEnabled == "none"
						? this.toggleDropDown
						: null
				}
				style={this.renderStyle()}
			>
				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						width: "100%"
					}}
				>
					{this.state.dropDownSelection != "" ? "" : this.state.placeholder}
					<div
						style={
							this.props.contentStyle
								? Object.assign({}, this.props.contentStyle)
								: {}
						}
					>
						{this.state.dropDownValues
							? this.props.displayNumberSign
								? this.renderItem(
										this.props.data[
											this.props.data.indexOf(this.state.dropDownSelection)
										]
								  )
								: this.state.dropDownValues[
										this.state.dropDownValues.indexOf(
											this.state.dropDownSelection
										)
								  ]
							: null}
					</div>
					<div style={{ paddingRight: 12 }}>
						<img
							src={require("../../../resources/arrow drop down vertical.png")}
						/>
					</div>
				</div>
				<div
					className="dropdown-content"
					ref={this.setWrapperRef}
					style={Object.assign({}, styles.dropdownContent, {
						display: this.state.dropDownEnabled
					})}
				>
					{this.state.showFilter ? (
						<div
							style={{
								height: 60
							}}
						>
							<input
								ref={this.setFilterRef}
								value={this.state.filterValue}
								onChange={this.handleFilterChange}
								style={Object.assign({}, styles.itemContainer, {
									border: "1px solid lightGrey",
									marginRight: 0
								})}
							/>
							<hr />
						</div>
					) : null}

					<div
						style={{
							position: "absolute",
							overflowY: "scroll",
							height: 370
						}}
					>
						<div style={{ position: "relative" }}>
							{this.state.entriesLimit != -1
								? this.renderDropDownItems().splice(0, this.state.entriesLimit)
								: this.renderDropDownItems()}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const styles = {
	itemContainer: {
		display: "flex",
		backgroundColor: "white",
		height: 44,
		alignItems: "center",
		paddingLeft: 18,
		marginRight: 18,
		width: 160,
		marginBottom: 4,
		fontSize: 12,
		fontFamily: "Roboto",
		border: "none",
		borderRadius: 4
	},
	itemContainerDisabled: {
		display: "flex",
		backgroundColor: "lightGrey",
		height: 44,
		alignItems: "center",
		paddingLeft: 18,
		marginBottom: 4,
		fontSize: 12,
		fontFamily: "Roboto",
		border: "none",
		borderRadius: 4
	},
	dropdownContent: {
		position: "absolute",
		backgroundColor: "#f9f9f9",
		minWidth: 184,
		height: 300,
		overflowY: "hidden",
		boxShadow: "0px 8px 16px 0px rgba(0, 0, 0, 0.2)",
		padding: "12px 16px",
		zIndex: 1
	}
};
function mapStateToProps(state, props) {
	return {};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

DropDown = connect(
	mapStateToProps,
	mapDispatchToProps
)(DropDown);
module.exports = DropDown;
