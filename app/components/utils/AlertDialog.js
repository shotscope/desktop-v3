import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions";

class AlertDialog extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderPrimaryButton = this.renderPrimaryButton.bind(this);
		this.renderSecondaryButton = this.renderSecondaryButton.bind(this);
		this.renderDismiss = this.renderDismiss.bind(this);
	}

	render() {
		return (
			<div className="alertDialogBackground">
				<div className="alertDialogBody">
					<div
						style={{
							display: "flex",
							width: "100%",
							justifyContent: "flex-end"
						}}
					>
						{!this.props.noCancel ? this.renderDismiss() : null}
					</div>
					<div style={styles.dialogHeader}>{this.props.header}</div>
					<div style={styles.dialogContent}>{this.props.content}</div>
					<div style={styles.dialogButtons}>
						{this.props.secondaryAction ? this.renderSecondaryButton() : null}
						{!this.props.noCancel ? this.renderPrimaryButton() : null}
					</div>
				</div>
			</div>
		);
	}

	renderDismiss() {
		return (
			<img
				style={{ cursor: "pointer" }}
				src={require("../../../resources/download.png")}
				onClick={this.props.dismissCallback}
			/>
		);
	}

	renderSecondaryButton() {
		return (
			<button onClick={this.props.secondaryAction} style={styles.dialogButton}>
				Confirm
			</button>
		);
	}

	renderPrimaryButton() {
		return (
			<button onClick={this.props.dismissCallback} style={styles.dialogButton}>
				Close
			</button>
		);
	}
}

const styles = {
	dialogHeader: {
		height: 28,
		fontSize: 18,
		marginTop: 16
	},
	dialogContent: {
		marginTop: 20,
		fontSize: 14
	},
	dialogButtons: {
		position: "absolute",
		bottom: 0,
		marginBottom: 30,
		display: "flex",
		width: 330,
		justifyContent: "space-between"
	},
	dialogButton: {
		flex: 1,
		maxWidth: 100,
		cursor: "pointer"
	}
};
function mapStateToProps(state, props) {
	return {};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

AlertDialog = connect(
	mapStateToProps,
	mapDispatchToProps
)(AlertDialog);
module.exports = AlertDialog;
