import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions";

class Badge extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div style={styles.badgeContainer}>
				<div style={styles.badgeContent}>{this.props.content}</div>
			</div>
		);
	}
}

const styles = {
	badgeContainer: {
		width: 20,
		height: 20,
		minWidth: 20,
		minHeight: 20,
		display: "flex",
		backgroundColor: "red",
		alignItems: "center",
		justifyContent: "center",
		borderRadius: 10
	},
	badgeBackground: {
		backgroundColor: "red",
		textAlign: "center",
		borderRadius: 10,
		width: 20,
		height: 20
	},
	badgeContent: {
		color: "white",
		fontSize: 14,
		display: "inline-block",
		verticalAlign: "middle"
	}
};

function mapStateToProps(state, props) {
	return {};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

Badge = connect(
	mapStateToProps,
	mapDispatchToProps
)(Badge);
module.exports = Badge;
