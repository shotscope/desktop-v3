import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import DropDown from "./utils/DropDown";
import WebAPIHandler from "../utils/WebAPIHandler";
import ReactCrop from "react-image-crop";
import DatePicker from "react-datepicker";
import * as Actions from "../actions";
import * as util from "../utils/util";
import { url } from "../utils/WebAPIHandler";
import AlertDialog from "./utils/AlertDialog";
import LoadingPage from "./LoadingPage";
import Jimp from "jimp";

class Profile extends React.PureComponent {
	constructor(props) {
		super(props);
		props.setLoading(true);

		var handicapArray = [];
		for (var i = -54; i < 8; i++) handicapArray.push(i);
		this.state = {
			courseNames: [],
			firstName: this.props.user.firstName,
			optInPublic: this.props.user.optInPublic,
			lastName: this.props.user.lastName,
			leftHanded: this.props.user.leftHanded,
			band1: this.props.user.band1,
			band2: this.props.user.band2,
			city: this.props.user.city,
			country: this.props.user.country,
			betaAccess: this.props.user.betaAccess,
			email: this.props.user.email,
			dob: new Date(this.props.user.dob),
			gender: this.props.user.gender,
			handicap: this.props.user.handicap,
			handicapArray: handicapArray.reverse(),
			units: this.props.user.units,
			src: null,
			crop: {
				aspect: 1,
				width: 50,
				height: 50,
				x: 0,
				y: 0
			},
			homeCourse: this.props.user.homeCourse,
			homeCourseID: this.props.user.homeCourseID,
			nationality: this.props.user.nationality,
			nationalitiesArray: this.props.persistent.nationalities,
			nationalitiesNamesArray: [],
			photoData: this.props.user.photoData,
			genderArray: ["Male", "Female", "Other"],
			imageRef: null,
			croppedImageUrl: null,
			croppedImageData: null,
			playerType: this.props.user.playerType,
			playerTypeArray: ["Amateur", "College", "Professional"],
			suggestions: [],
			renderImageCropModal: false,
			displayUpdateSuccess: false,
			displayUpdateFailed: false,
			displayInvalidEmail: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleDropDownChange = this.handleDropDownChange.bind(this);
		this.handleImageSelect = this.handleImageSelect.bind(this);
		this.renderImageCropModal = this.renderImageCropModal.bind(this);
		this.imageSelect = React.createRef();
		this.showImageSelect = this.showImageSelect.bind(this);
		this.handleSaveChanges = this.handleSaveChanges.bind(this);
	}

	async componentDidMount() {
		await Promise.all([await WebAPIHandler.getUser()]).then(result => {
			var tempCoursesArray = [];
			var tempNationalitiesArray = [];
			for (var course of this.props.persistent.courses) {
				tempCoursesArray.push(course.name);
			}
			for (var nationality of this.props.persistent.nationalities) {
				tempNationalitiesArray.push(nationality.name);
			}
			this.setState(
				{
					courseNames: tempCoursesArray,
					nationalitiesNamesArray: tempNationalitiesArray
				},
				() => {
					console.log("Profile ready", result);
					this.props.setLoading(false);
				}
			);
			for (var results of result) {
				if (results != true) this.props.setToken("");
			}
		});
	}

	handleChange(event) {
		console.log(event.target);
		if (event.target) {
			const target = event.target;
			const name = target.name;
			this.setState({ [name]: target.value });
		} else this.setState({ dob: new Date(event) });
	}

	handleDropDownChange(key, value) {
		console.log("HELLO", key, value);
		var stateObj = { [key]: value };
		console.log(stateObj);
		if (key == "homeCourse")
			for (var course of this.props.persistent.courses) {
				if (course.name == value) {
					this.setState({ [key]: value, homeCourseID: course.id });
				}
			}
		else this.setState(stateObj);
	}

	async handleSaveChanges(event) {
		event.preventDefault();

		if (util.validateEmail(this.state.email)) {
			var userObj = {
				OptInPublic: this.state.optInPublic,
				HomeCourse: this.state.homeCourse,
				HomeCourseID: this.state.homeCourseID,
				LeftHanded: this.state.leftHanded,
				Band1: this.state.band1,
				Band2: this.state.band2,
				City: this.state.city,
				Country: this.state.country,
				Units: this.state.units,
				BetaAccess: this.state.betaAccess,
				FirstName: this.state.firstName,
				LastName: this.state.lastName,
				Handicap: this.state.handicap,
				Gender: this.state.gender,
				Nationality: this.state.nationality,
				Dob: this.state.dob,
				PlayerType: this.state.playerType,
				PhotoData: this.state.photoData,
				Email: this.state.email
			};
			console.log(userObj);
			await WebAPIHandler.updateUser(userObj).then(response => {
				if (response.status == 200) {
					WebAPIHandler.getUser();
					WebAPIHandler.getNearbyCourses(this.state.homeCourseID).then(
						response => {
							console.log(response);
							this.props.setNearbyCourses(response);
						}
					);
					this.setState({ displayUpdateSuccess: true });
				} else {
					this.setState({ displayUpdateFailed: true });
				}
			});
		} else this.setState({ displayInvalidEmail: true });
	}

	async handleImageSelect(e) {
		e.persist();
		if (e.target.files && e.target.files.length > 0) {
			const reader = new FileReader();
			await reader.addEventListener("load", async () => {
				const canvas = document.createElement("canvas");
				const ctx = canvas.getContext("2d");
				var image = new Image();
				const load = (url, image) =>
					new Promise(resolve => {
						image.onload = () =>
							resolve({ url, ratio: image.naturalWidth / image.naturalHeight });
						image.src = url;
					});
				await load(reader.result, image);
				e.target.value = null;
				canvas.width = image.naturalWidth;
				canvas.height = image.naturalHeight;
				ctx.drawImage(image, 0, 0);

				var convertedImg = await this.getDrawnCanvas(canvas);
				this.setState({ src: convertedImg, renderImageCropModal: true }, () => {
					Jimp.read(this.state.src).then(image => {
						image.getBase64Async("image/jpeg").then(processedImage => {
							this.setState({
								src: processedImage
							});
						});
					});
				});
			});
			reader.readAsDataURL(e.target.files[0]);
		}
	}

	async getDrawnCanvas(canvas) {
		return canvas.toDataURL("image/jpeg");
	}

	onCropChange = crop => {
		this.setState({ crop });
	};

	onCropComplete = (crop, pixelCrop) => {
		this.makeClientCrop(crop, pixelCrop);
	};

	getCroppedImg(image, pixelCrop, fileName) {
		const canvas = document.createElement("canvas");
		canvas.width = pixelCrop.width;
		canvas.height = pixelCrop.height;
		const ctx = canvas.getContext("2d");
		ctx.drawImage(
			image,
			pixelCrop.x,
			pixelCrop.y,
			pixelCrop.width,
			pixelCrop.height,
			0,
			0,
			pixelCrop.width,
			pixelCrop.height
		);

		return canvas.toDataURL("image/jpeg");
	}

	onImageLoaded = (image, pixelCrop) => {
		this.imageRef = image;
		// Make the library regenerate aspect crops if loading new images.
		const { crop } = this.state;
		if (crop.aspect && crop.height && crop.width) {
			this.setState({
				crop: { ...crop, height: null }
			});
		} else {
			this.makeClientCrop(crop, pixelCrop);
		}
	};

	async makeClientCrop(crop, pixelCrop) {
		if (this.imageRef && crop.width && crop.height) {
			const croppedImageUrl = await this.getCroppedImg(
				this.imageRef,
				pixelCrop,
				"newFile.jpeg"
			);
			this.setState({ croppedImageUrl });
		}
	}

	showImageSelect() {
		this.imageSelect.current.click();
	}

	render() {
		const { crop, croppedImageUrl, src } = this.state;
		if (this.state.courseNames.length > 0)
			return (
				<div
					style={{
						left: 250,
						bottom: 0,
						overflowY: "scroll",
						right: 20,
						top: 50,
						paddingRight: 12,
						fontFamily: "Roboto",
						position: "absolute",
						marginBottom: 60
					}}
				>
					{this.state.displayUpdateSuccess ? (
						<AlertDialog
							header="Profile Updated"
							content="Your profile details have successfully been updated."
							dismissCallback={() => {
								this.setState({ displayUpdateSuccess: false });
							}}
						/>
					) : null}
					{this.state.displayUpdateFailed ? (
						<AlertDialog
							header="Profile Update Failed"
							content="Something went wrong updating your profile details. Ensure that you are connected to the internet and try again. If the problem persists, contact support using the link on the navigation panel."
							dismissCallback={() => {
								this.setState({ displayUpdateFailed: false });
							}}
						/>
					) : null}
					{this.state.displayInvalidEmail ? (
						<AlertDialog
							header="Profile Update Failed"
							content="An invalid email was entered, please make sure that you have entered a valid email. If the problem persists, contact support using the link on the navigation panel."
							dismissCallback={() => {
								this.setState({ displayInvalidEmail: false });
							}}
						/>
					) : null}
					{this.state.renderImageCropModal &&
						this.renderImageCropModal(
							src,
							crop,
							this.onImageLoaded,
							this.onCropComplete,
							this.onCropChange
						)}
					<div
						style={{
							fontSize: 18,
							height: 100,
							fontWeight: "bold",
							display: "flex"
						}}
					>
						<div style={{ width: 0 }}>Profile</div>
						<input
							type="file"
							onChange={event => {
								this.handleImageSelect(event);
							}}
							ref={this.imageSelect}
							style={{ display: "none" }}
						/>
						<div
							style={{
								width: "100%",
								display: "flex",
								justifyContent: "center"
							}}
						>
							<div
								style={{
									display: "block",
									position: "absolute"
								}}
							>
								<a
									onClick={event => {
										this.showImageSelect(event);
									}}
								>
									<img
										style={{
											display: "block",
											margin: "auto",
											width: 80,
											height: 80,
											zIndex: 1,
											borderRadius: "50%"
										}}
										src={
											this.state.photoData &&
											this.state.photoData != "/Content/default-avatar.png"
												? this.state.photoData
												: require("../../resources/Avatar_PlaceholderLogo.png")
										}
									/>
									<img
										style={{
											position: "absolute",
											width: "auto",
											bottom: 4,
											zIndex: 2,
											right: 4
										}}
										src={require("../../resources/plus.png")}
									/>
								</a>
							</div>
						</div>
					</div>
					<div>
						<form onSubmit={this.handleSaveChanges}>
							<div style={styles.row}>
								<div style={styles.title}>Name</div>
								<input
									name="firstName"
									value={this.state.firstName}
									onChange={this.handleChange}
									style={Object.assign({}, styles.itemContainer, {
										flexGrow: 1
									})}
								/>{" "}
								<input
									name="lastName"
									value={this.state.lastName}
									onChange={this.handleChange}
									style={Object.assign({}, styles.itemContainer, {
										flexGrow: 1
									})}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Date of Birth</div>
								<DatePicker
									selected={new Date(this.state.dob)}
									peekNextMonth
									showMonthDropdown
									showYearDropdown
									dropdownMode="select"
									onChange={this.handleChange}
									maxTime={new Date(Date.now())}
									dateFormat="dd/MM/yyyy"
									style={Object.assign({}, styles.itemContainer, {
										flexGrow: 1
									})}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Gender</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="gender"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={1}
											defaultChecked={this.state.gender == 1 ? "checked" : null}
										/>
										{this.state.gender == 1 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>Male</div>
								</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="gender"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={0}
											defaultChecked={this.state.gender == 0 ? "checked" : null}
										/>
										{this.state.gender == 0 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>Female</div>
								</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="gender"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={2}
											defaultChecked={this.state.gender == 2 ? "checked" : null}
										/>
										{this.state.gender == 2 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									Other
								</div>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Nationality</div>
								<DropDown
									data={this.state.nationalitiesNamesArray}
									showFilter
									stateKey={"nationality"}
									changeDropDownSelection={this.handleDropDownChange}
									initialSelection={util.getNationalityIndexFromID(
										this.props.persistent.nationalities,
										this.props.user.nationality
									)}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Email</div>
								<input
									name="email"
									value={this.state.email}
									onChange={this.handleChange}
									style={Object.assign({}, styles.itemContainer, {
										flexGrow: 1
									})}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Player Type</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="playerType"
											type="radio"
											onClick={this.handleChange}
											value={0}
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											defaultChecked={
												this.state.playerType == 0 ? "checked" : null
											}
										/>
										{this.state.playerType == 0 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>Amateur</div>
								</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="playerType"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={1}
											defaultChecked={
												this.state.playerType == 1 ? "checked" : null
											}
										/>
										{this.state.playerType == 1 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>College</div>
								</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="playerType"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={2}
											defaultChecked={
												this.state.playerType == 2 ? "checked" : null
											}
										/>
										{this.state.playerType == 2 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									Professional
								</div>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Handicap</div>
								<DropDown
									data={this.state.handicapArray}
									containerStyle={styles.itemContainer}
									stateKey="handicap"
									displayNumberSign="true"
									initialSelection={
										this.state.handicapArray.indexOf(this.state.handicap) != -1
											? this.state.handicapArray.indexOf(this.state.handicap)
											: this.state.handicapArray.indexOf(0)
									}
									changeDropDownSelection={this.handleDropDownChange}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Home Course</div>
								<DropDown
									changeDropDownSelection={this.handleDropDownChange}
									initialSelection={this.state.courseNames.indexOf(
										this.state.homeCourse
									)}
									data={this.state.courseNames}
									containerStyle={{
										display: "flex",
										backgroundColor: "white",
										height: 44,
										alignItems: "center",
										paddingLeft: 18,
										marginRight: 18,
										width: 300,
										marginBottom: 4,
										fontSize: 12,
										fontFamily: "Roboto",
										border: "none",
										borderRadius: 4
									}}
									showFilter
									stateKey="homeCourse"
									entriesLimit={100}
								/>
							</div>
							<div style={styles.row}>
								<div style={styles.title}>Distance</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="units"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={1}
											defaultChecked={this.state.units == 1 ? "checked" : null}
										/>
										{this.state.units == 1 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>Yards</div>
								</div>
								<div style={{ display: "flex", alignItems: "center" }}>
									<label style={{ marginRight: 4, cursor: "pointer" }}>
										<input
											name="units"
											type="radio"
											style={{
												opacity: 0,
												width: 0,
												height: 0,
												position: "absolute"
											}}
											onClick={this.handleChange}
											value={0}
											defaultChecked={this.state.units == 0 ? "checked" : null}
										/>
										{this.state.units == 0 ? (
											<img src={require("../../resources/Toggleon.png")} />
										) : (
											<img src={require("../../resources/Toggleoff.png")} />
										)}
									</label>
									<div style={{ marginRight: 20 }}>Meters</div>
								</div>
							</div>
							<div
								style={{ position: "fixed", right: 35, bottom: 0, height: 60 }}
							>
								<input
									style={{ height: 30, marginTop: 15, cursor: "pointer" }}
									type="submit"
									value="Save Changes"
								/>
							</div>
						</form>
					</div>
				</div>
			);
		else return <LoadingPage />;
	}
	renderImageCropModal(src, crop, onImageLoaded, onCropComplete, onCropChange) {
		return (
			<div style={styles.imageCropModalBackground}>
				<div className="requestCourseModal">
					<div
						onClick={() => {
							this.setState({
								renderImageCropModal: !this.state.renderImageCropModal
							});
						}}
					>
						X
					</div>
					<div
						style={{
							height: 430,
							width: 480
						}}
					>
						<ReactCrop
							src={src}
							crop={crop}
							onImageLoaded={onImageLoaded}
							onComplete={onCropComplete}
							onChange={onCropChange}
						/>
					</div>
					<button
						onClick={() => {
							Jimp.read(this.state.croppedImageUrl).then(image => {
								image.scaleToFit(256, 256);
								image.getBase64Async("image/jpeg").then(base64Img => {
									image
										.quality(util.compressToLimit(image))
										.getBase64Async("image/jpeg")
										.then(processedImage => {
											this.setState({
												photoData: processedImage
											});
											this.setState({ renderImageCropModal: false });
										});
								});
							});
						}}
					>
						Save
					</button>
				</div>
			</div>
		);
	}
}

const styles = {
	itemContainer: {
		display: "flex",
		backgroundColor: "white",
		height: 44,
		color: "#8C8C8C",
		minWidth: 158,
		alignItems: "center",
		paddingLeft: 18,
		marginRight: 14,
		marginBottom: 0,
		fontSize: 12,
		fontFamily: "Roboto",
		border: "none"
	},
	row: {
		display: "flex",
		height: 67,
		alignItems: "center",
		borderTop: "1px solid #979797",
		alignItems: "center"
	},
	title: {
		width: 103,
		fontSize: 14
	},
	imageCropModalBackground: {
		position: "fixed",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: "rgba(255,255,255,0.7)",
		paddingTop: 45,
		zIndex: 3
	}
};

function mapStateToProps(state, props) {
	return {
		user: state.userReducer,
		dataReducer: state.dataReducer,
		band: state.bandReducer,
		persistent: state.persistentReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

Profile = connect(
	mapStateToProps,
	mapDispatchToProps
)(Profile);
module.exports = Profile;
