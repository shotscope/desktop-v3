import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { shell } from "electron";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions
import { url } from "../utils/WebAPIHandler";
import Badge from "./utils/Badge";
import AlertDialog from "./utils/AlertDialog";

class NavPanel extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			myShotScopeCollapsed: false,
			renderLogout: false
		};
		this.renderBadge = this.renderBadge.bind(this);
		this.checkCourseExistsOnBand = this.checkCourseExistsOnBand.bind(this);
		this.searchArray = this.searchArray.bind(this);
		this.checkCourseUpdateAvailable = this.checkCourseUpdateAvailable.bind(
			this
		);
		this.checkCourseExistsOnUser = this.checkCourseExistsOnUser.bind(this);
	}

	async componentDidMount() {}

	render() {
		return (
			<div
				style={{
					height: "100vh",
					width: 220,
					top: 0,
					backgroundColor: "#181818"
				}}
			>
				{this.state.renderLogout ? (
					<AlertDialog
						header="Log Out"
						dismissCallback={() => {
							this.setState({ renderLogout: false });
						}}
						secondaryAction={() => {
							this.setState({ renderLogout: false });
							this.props.logout();
						}}
						content="Are you sure you would like to log out?"
					/>
				) : null}
				<div style={{ height: 150, textAlign: "center" }}>
					<a href="#" onClick={() => this.props.navCallback("Profile")}>
						<img
							style={{
								width: 50,
								height: 50,
								borderRadius: "50%",
								marginTop: 30,
								marginBottom: 7
							}}
							src={
								this.props.user.photoData &&
								this.props.user.photoData != "/Content/default-avatar.png"
									? this.props.user.photoData
									: require("../../resources/Avatar_PlaceholderLogo.png")
							}
						/>
						<div
							style={{
								color: "#FFFFFF",
								fontSize: 18,
								fontWeight: "bold"
							}}
						>
							{this.props.user.firstName} {this.props.user.lastName}
						</div>
						<div
							style={{
								color: "#FFFFFF",
								fontSize: 12
							}}
						>
							{this.props.user.email}
						</div>
					</a>
				</div>
				<div>
					<div>
						<div style={styles.menuHeader}>
							<a
								href="#"
								onClick={
									() => this.props.navCallback("MyShotScope", "Connect")
									// this.setState({
									// 	// Not doing collapsible menu anymore
									// 	// myShotScopeCollapsed: !this.state.myShotScopeCollapsed
									// })
								}
							>
								My Shot Scope
							</a>
						</div>
						<div
							style={
								this.state.myShotScopeCollapsed
									? styles.collapsed
									: styles.expanded
							}
						>
							<a
								href="#"
								onClick={() => this.props.navCallback("MyShotScope", "Connect")}
							>
								<div
									style={Object.assign(
										{},
										styles.menuItem,
										this.props.currentComponent == "MyShotScope" &&
											this.props.currentMyShotScopeWindow == "Connect"
											? styles.selected
											: {}
									)}
								>
									Connect
								</div>
							</a>
							<a
								href="#"
								onClick={() =>
									this.props.navCallback("MyShotScope", "Settings")
								}
							>
								<div
									style={Object.assign(
										{},
										styles.menuItem,
										this.props.currentComponent == "MyShotScope" &&
											this.props.currentMyShotScopeWindow == "Settings"
											? styles.selected
											: {}
									)}
								>
									Settings
								</div>
							</a>
							{/* <a
								href="#"
								onClick={() => this.props.navCallback("MyShotScope", "About")}
							>
								<div
									style={Object.assign(
										{},
										styles.menuItem,
										this.props.currentComponent == "MyShotScope" &&
										this.props.currentMyShotScopeWindow == "About"
											? styles.selected
											: {}
									)}
								>
									About
								</div>
							</a> */}
						</div>
					</div>
					<a href="#" onClick={() => this.props.navCallback("Courses")}>
						{" "}
						<div
							style={Object.assign(
								{},
								styles.menuHeader,
								this.props.currentComponent == "Courses" ? styles.selected : {}
							)}
						>
							<div>Courses</div>
							<div style={{ marginLeft: 6 }}>{this.renderBadge()}</div>
						</div>
					</a>
					<div>
						<a href="#" onClick={() => shell.openExternal(url)}>
							<div
								style={Object.assign(
									{},
									styles.menuHeader,
									this.props.currentComponent == "FindCourses"
										? styles.selected
										: {}
								)}
							>
								Performance Dashboard
							</div>
						</a>
					</div>
					<div>
						<a
							href="#"
							// onClick={() => this.props.navCallback("Support")}
							onClick={() => {
								this.props.user.betaAccess
									? shell.openExternal(
											"https://shotscope.com/beta-program/feedback/"
									  )
									: shell.openExternal("https://www.shotscope.com/support/");
							}}
						>
							<div
								style={Object.assign(
									{},
									styles.menuHeader,
									this.props.currentComponent == "Feedback"
										? styles.selected
										: {}
								)}
							>
								{this.props.user.betaAccess ? (
									<div>Feedback</div>
								) : (
									<div>Support</div>
								)}
							</div>
						</a>
					</div>
				</div>
				<div
					style={{
						position: "absolute",
						bottom: 0,
						marginLeft: 28,
						color: "#A5A4A4",
						fontWeight: "bold",
						paddingBottom: 28,
						textAlign: "left"
					}}
				>
					<div style={{ width: 183, height: 80 }}>
						<img
							style={{
								display: "block"
							}}
							src={require("../../resources/ShotScopeLogo_White.png")}
						/>
					</div>
					<a
						href="#"
						onClick={() => {
							this.setState({
								renderLogout: true
							});
						}}
					>
						<img
							style={{
								width: 14,
								height: 12
							}}
							src={require("../../resources/Log_Out.png")}
						/>{" "}
						Log Out
					</a>
				</div>
			</div>
		);
	}

	checkCourseExistsOnUser(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (
					this.searchArray(courseItem.id, this.props.dataReducer.userCourses) !=
					-1
				)
					return true;
				else return false;
			else if (
				this.searchArray(
					courseItem.courseID,
					this.props.dataReducer.userCourses
				) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	renderBadge() {
		if (this.props.band.connected) {
			var pendingCount = 0;

			for (var course of this.props.dataReducer.userCourses) {
				if (
					!this.checkCourseExistsOnBand(course) ||
					this.checkCourseUpdateAvailable(course)
				)
					pendingCount++;
			}

			if (
				pendingCount > 0 &&
				this.props.band.courseIDsCount == this.props.band.courseIDs.length &&
				this.props.band.courseIDsCount != 0
			)
				return <Badge content={pendingCount} />;
		}
	}

	checkCourseUpdateAvailable(courseItem) {
		if (this.props.band.connected) {
			if (this.props.band.pLTimestamp == 0) {
				if (
					this.checkCourseExistsOnBand(courseItem) &&
					this.checkCourseExistsOnUser(courseItem)
				) {
					if (courseItem.id) {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (courseItem.id == userCourse.courseID) {
								if (
									Date.parse(new Date(userCourse.lastUpdateDate)) >
									this.props.band.timestamp * 1000 + 1000
								)
									return true;
							}
						}
						return false;
					} else {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (courseItem.courseID == userCourse.courseID) {
								if (
									Date.parse(new Date(userCourse.lastUpdateDate)) >
									this.props.band.timestamp * 1000 + 1000
								)
									return true;
							}
						}
						return false;
					}
				} else return false;
			} else {
				// Preloaded bands
				if (
					this.checkCourseExistsOnBand(courseItem) &&
					this.checkCourseExistsOnUser(courseItem)
				) {
					if (courseItem.id) {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (userCourse.courseID == courseItem.id) {
								for (var bandCourse of this.props.band.courseIDs) {
									if (userCourse.courseID == bandCourse.id) {
										if (
											Date.parse(new Date(userCourse.lastUpdateDate)) >
											bandCourse.lastUpdated * 1000 + 1000
										)
											return true;
										else return false;
									}
								}
								// check preloaded courses if the course is not on the band
								for (var pLCourse of this.props.band.pLCourses) {
									if (pLCourse == courseItem.id) {
										for (var serverPLCourse of this.props.persistent
											.pLCourses) {
											if (serverPLCourse.preLoaded != null) {
												if (
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														Date.parse(new Date(serverPLCourse.preLoaded)) &&
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														this.props.band.pLTimestamp * 1000 + 1000
												)
													return true;
											}
										}
									}
								}
							}
						}
						return false;
					} else {
						for (var userCourse of this.props.dataReducer.userCourses) {
							if (userCourse.courseID == courseItem.courseID) {
								for (var bandCourse of this.props.band.courseIDs) {
									if (userCourse.courseID == bandCourse.id) {
										if (
											Date.parse(new Date(userCourse.lastUpdateDate)) >
											bandCourse.lastUpdated * 1000 + 1000
										)
											return true;
										else return false;
									}
								}
								// check preloaded courses if the course is not on the band
								for (var pLCourse of this.props.band.pLCourses) {
									if (pLCourse == courseItem.courseID) {
										for (var course of this.props.persistent.courses) {
											if (course.preLoaded != null) {
												if (
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														Date.parse(new Date(course.preLoaded)) &&
													Date.parse(new Date(userCourse.lastUpdateDate)) >
														this.props.band.pLTimestamp * 1000 + 1000
												)
													return true;
											}
										}
									}
								}
							}
						}
						return false;
					}
				} else return false;
			}
		} else return false;
	}

	checkCourseExistsOnBand(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (
					this.searchArray(courseItem.id, this.props.band.courseIDs) != -1 ||
					this.props.band.pLCourses.indexOf(courseItem.id) != -1
				)
					return true;
				else return false;
			else if (
				this.searchArray(courseItem.courseID, this.props.band.courseIDs) !=
					-1 ||
				this.props.band.pLCourses.indexOf(courseItem.courseID) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	searchArray(searchElement, array) {
		if (array != undefined) {
			for (var item of array) {
				if (item.id != undefined) {
					if (searchElement == item.id) return true;
					else if (searchElement == item.courseID) return true;
					else if (searchElement.id != undefined) {
						if (searchElement.id == item.id) return true;
					} else if (searchElement == item.courseID) return true;
				} else if (searchElement == item || searchElement == item.courseID)
					return true;
			}
			return -1;
		} else return -1;
	}
}

const styles = {
	menuHeader: {
		marginLeft: 14,
		paddingLeft: 13,
		marginTop: 10,
		fontSize: 14,
		height: 36,
		marginRight: 14,
		fontWeight: "bold",
		display: "flex",
		alignItems: "center"
	},
	menuItem: {
		height: 24,
		marginLeft: 14,
		paddingLeft: 13,
		fontSize: 14,
		marginRight: 20,
		display: "flex",
		alignItems: "center"
	},
	selected: {
		borderRadius: 2,
		backgroundColor: "#505050",
		opacity: 0.48
	},
	collapsed: {
		height: 0,
		display: "none"
	},
	expanded: {
		height: 60
	}
};
function mapStateToProps(state, props) {
	return {
		user: state.userReducer,
		dataReducer: state.dataReducer,
		band: state.bandReducer,
		persistent: state.persistentReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

NavPanel = connect(
	mapStateToProps,
	mapDispatchToProps
)(NavPanel);
module.exports = NavPanel;
