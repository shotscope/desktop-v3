import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; // Import your actions

class UpdatePage extends React.PureComponent {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div
				style={{
					position: "absolute",
					top: 0,
					left: 0,
					height: "100%",
					width: "100%",
					backgroundColor: "red",
					justifyContent: "center",
					display: "flex",
					flexDirection: "column",
					zIndex: 99,
					fontSize: 24
				}}
			>
				<div style={{ alignSelf: "center" }}>
					<img
						style={{ width: 230, height: 50 }}
						src={require("../../resources/ShotScopeLogo_White.png")}
					/>
				</div>
				<div style={{ alignSelf: "center", marginTop: 12 }}>Updating...</div>
			</div>
		);
	}
}

function mapStateToProps(state, props) {
	return {};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

UpdatePage = connect(
	mapStateToProps,
	mapDispatchToProps
)(UpdatePage);
module.exports = UpdatePage;
