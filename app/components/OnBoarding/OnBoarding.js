import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import WebAPIHandler from "../../utils/WebAPIHandler";
import DropDown from "../utils/DropDown";
import SerialHandler, { SET_HAND } from "../../utils/SerialHandler";
import BandStatusBar from "../BandStatusBar";
import SidePanel from "./SidePanel";
import Badge from "../utils/Badge";
import url from "../../utils/WebAPIHandler";
import { shell } from "electron";

import * as Actions from "../../actions"; // Import your actions
class OnBoarding extends React.PureComponent {
	constructor(props) {
		super(props);

		var timeZonesArray = [];
		for (var i = -11; i < 13; i++) {
			timeZonesArray.push(i);
		}
		this.state = {
			title: "Welcome",
			currentPage: "Connect",
			timezones: timeZonesArray,
			timezoneSelection: 11,
			courseFilter: "",
			countryFilter: "",
			stateFilter: "",
			displayNearby: false,
			selectedPendingCourses: [],
			connect: { selected: true, complete: false },
			settings: { selected: false, complete: false },
			courses: { selected: false, complete: false },
			tags: { selected: false, complete: false },
			myBag: { selected: false, complete: false }
		};
		this.changeDropDownSelection = this.changeDropDownSelection.bind(this);
		this.checkCourseUpdateAvailable = this.checkCourseUpdateAvailable.bind(
			this
		);
		this.checkCourseExistsNearby = this.checkCourseExistsNearby.bind(this);
		this.checkCourseExistsOnUser = this.checkCourseExistsOnUser.bind(this);
		this.checkCourseExistsOnBand = this.checkCourseExistsOnBand.bind(this);
		this.renderPendingCourses = this.renderPendingCourses.bind(this);
		this.renderUpdateCourses = this.renderUpdateCourses.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.addCourse = this.addCourse.bind(this);
		this.handlePendingCourseSelect = this.handlePendingCourseSelect.bind(this);
		this.syncCourses = this.syncCourses.bind(this);
		this.syncCompletionCallback = this.syncCompletionCallback.bind(this);
	}

	componentDidMount() {}

	syncCompletionCallback(success) {
		if (success) {
			this.props.setLastCourseSyncDateTime(new Date(Date.now()).toISOString());
			SerialHandler.getAllBandCourses();
		} else this.toggleDisplaySyncFailed();
	}

	handlePendingCourseSelect(course) {
		var selectedPendingCoursesState = this.state.selectedPendingCourses;
		var index = selectedPendingCoursesState.indexOf(course);
		if (index == -1) {
			this.state.selectedPendingCourses.push(course);
		} else this.state.selectedPendingCourses.pop(course);
	}

	async syncCourses() {
		for (var course of this.state.selectedPendingCourses) {
			await WebAPIHandler.deleteCourse(course.id);
		}
		await WebAPIHandler.getUserCoursesLite();
		await WebAPIHandler.getUserCourses();
		if (this.props.dataReducer.downloadedCourses.length > 0) {
			var uniqueCourses = [];
			// if (this.props.band.courseIDs.indexOf(course.courseID) == -1)
			for (var course of this.props.dataReducer.downloadedCourses)
				uniqueCourses.push(course);

			if (uniqueCourses.length > 0)
				SerialHandler.loadCourses(
					uniqueCourses,
					this.props.dataReducer.downloadedCoursesIndex,
					this.syncCompletionCallback
				);
		}
	}

	renderCourseState(course) {
		if (this.checkCourseExistsOnBand(course))
			return (
				<div style={styles.columnRowContent}>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						On device
					</div>
					<img src={require("../../../resources/GreenCheck.png")} />
				</div>
			);
		else if (
			!this.checkCourseExistsOnBand(course) &&
			this.checkCourseExistsOnUser(course)
		)
			return (
				<div style={styles.columnRowContent}>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						Ready to Sync
					</div>{" "}
					<div>
						<img src={require("../../../resources/Update.png")} />
					</div>
				</div>
			);
		else
			return (
				<div
					style={styles.columnRowContent}
					onClick={() => this.addCourse(course.id)}
				>
					<div style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}>
						Add
					</div>{" "}
					<div
						onClick={() => this.addCourse(course.id)}
						style={{ cursor: "pointer" }}
					>
						<img src={require("../../../resources/+.png")} />
					</div>
				</div>
			);
	}

	checkCourseExistsOnBand(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (this.searchArray(courseItem.id, this.props.band.courseIDs) != -1)
					return true;
				else return false;
			else if (
				this.searchArray(courseItem.courseID, this.props.band.courseIDs) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	checkCourseExistsOnUser(courseItem) {
		if (this.props.band.connected) {
			if (courseItem.id)
				if (
					this.searchArray(courseItem.id, this.props.dataReducer.userCourses) !=
					-1
				)
					return true;
				else return false;
			else if (
				this.searchArray(
					courseItem.courseID,
					this.props.dataReducer.userCourses
				) != -1
			)
				return true;
			else return false;
		} else return false;
	}

	searchArray(searchElement, array) {
		if (array != undefined) {
			for (var item of array) {
				if (item.id != undefined) {
					if (searchElement == item.id) return true;
					else if (searchElement == item.courseID) return true;
					else if (searchElement.id != undefined) {
						if (searchElement.id == item.id) return true;
					} else if (searchElement == item.courseID) return true;
				} else if (searchElement == item || searchElement == item.courseID)
					return true;
			}
			return -1;
		} else return -1;
	}

	checkCourseExistsNearby(courseItem) {
		if (this.props.band.connected) {
			if (this.searchArray(courseItem, this.state.nearbyCourses) != -1) {
				return true;
			} else return false;
		} else return false;
	}

	changeDropDownSelection(selection) {
		var bandOffset = 0;
		this.setState(
			{
				timezoneSelection: selection
			},
			() => {
				bandOffset = this.state.timezoneSelection * 4 - 4 * 11;
				// SerialHandler.setDateTime(bandOffset);
			}
		);
	}

	renderInfoText() {
		if (!this.props.band.connected) {
			return (
				<p style={{ fontSize: 14 }}>
					Please connect your Shot Scope using the USB cable provided.
				</p>
			);
		} else
			return (
				<button
					onClick={() =>
						this.setState({
							currentPage: "Settings",
							connect: { selected: true, complete: true },
							settings: { selected: true, complete: false },
							title: "Settings"
						})
					}
				>
					Continue
				</button>
			);
	}

	renderImages() {
		if (this.props.band.connected) {
			return (
				<div style={{ marginLeft: "auto", marginRight: "auto" }}>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/Band.png")}
					/>
				</div>
			);
		} else {
			return (
				<div
					style={{
						marginLeft: "auto",
						width: "100",
						justifyContent: "center",
						marginRight: "auto",
						display: "flex"
					}}
				>
					<img
						style={{ alignSelf: "center", marginRight: 13 }}
						src={require("../../../resources/Band.png")}
					/>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/USB.png")}
					/>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/Laptop.png")}
					/>
				</div>
			);
		}
	}

	renderCurrentWindow() {
		switch (this.state.currentPage) {
			case "Connect":
				return this.renderConnect();
				break;
			case "Settings":
				return this.renderSettings();
				break;
			case "Courses":
				return this.renderCourses();
				break;
			case "Sync":
				return this.renderSync();
				break;
			case "Tags":
				return this.renderTags();
				break;
			case "My Bag":
				return this.renderMyBag();
			default:
				return this.renderConnect();
		}
	}

	renderMyBag() {
		return (
			<div>
				<img
					style={{ marginTop: 23, width: 40, height: 40 }}
					src={require("../../../resources/GreenCheck.png")}
				/>
				<div
					style={{
						marginTop: 18,
						width: 440,
						marginLeft: "auto",
						marginRight: "auto"
					}}
				>
					We suggest that you set up your My Bag before playing your first
					round.
					<br />
					My Bag allows you to create and compare the performance of an
					unlimited number of clubs with no restrictions. Now, when you reach
					into your bag, there are no surprises. Base your club selection on
					pure data and ensure that you have what you need to play your best
					round.
					<br />
					Know your clubs, build your bag and maximise your performance!
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						marginTop: 42,
						width: 360,
						marginLeft: "auto",
						marginRight: "auto"
					}}
				>
					{" "}
					<button
						style={{ width: 200 }}
						onClick={() => {
							this.props.setOnBoarding(false);
							shell.openExternal(url + "/Manage/MyBag");
						}}
					>
						Finish and setup My Bag
					</button>
					<button
						style={{ width: 127 }}
						onClick={() => this.props.setOnBoarding(false)}
					>
						Finish setup
					</button>
				</div>
			</div>
		);
	}

	renderTags() {
		return (
			<div style={{ marginTop: 25 }}>
				<img src={require("../../../resources/tags.png")} />
				<div
					style={{
						marginTop: 40,
						width: 470,
						marginLeft: "auto",
						marginRight: "auto"
					}}
				>
					<div>
						<Badge content={1} /> The tags received with your Shot Scope must be
						screwed into the top of your grips in order to track your game.
					</div>
					<div style={{ marginTop: 14 }}>
						<Badge content={2} />
						Tags are labelled and we recomment that you insert the tag in the
						corresponding club, for example the "D" tag in your driver.
					</div>
					<div style={{ marginTop: 14 }}>
						<Badge content={3} />
						The "P" tag must be inserted into your putter.
					</div>
				</div>
				<button
					onClick={() =>
						this.setState({
							currentPage: "My Bag",
							tags: { selected: true, complete: true },
							myBag: { selected: true, complete: false },
							title: "Setup Complete"
						})
					}
				>
					Continue
				</button>
			</div>
		);
	}

	renderHeader() {
		if (this.props.currentOnBoardingWindow == "Connect") {
			if (this.props.band.connected) {
				return "Band Connected";
			} else return "Band Disconnected";
		} else return this.props.currentOnBoardingWindow;
	}

	renderConnect() {
		return (
			<div>
				<div
					style={{
						textAlign: "center",
						height: "100%",
						alignItems: "center",
						width: "100%"
					}}
				>
					<div
						style={{
							marginLeft: "auto",
							marginRight: "auto",
							height: 189,
							marginTop: 33,
							marginBottom: 43
						}}
					>
						{this.renderImages()}
					</div>
				</div>
				{this.renderInfoText()}
			</div>
		);
	}

	render() {
		return (
			<div
				style={{
					color: "#8C8C8C",
					fontSize: 18,
					fontWeight: "bold",
					height: "100vh",
					width: "100%",
					textAlign: "center",
					fontFamily: "Roboto"
				}}
			>
				<BandStatusBar />
				<div
					style={{
						display: "flex",
						width: "100%",
						height: "100%"
					}}
				>
					<SidePanel
						connect={this.state.connect}
						settings={this.state.settings}
						courses={this.state.courses}
						tags={this.state.tags}
						myBag={this.state.myBag}
					/>
					<div
						style={{
							textAlign: "center",
							flex: 1,
							height: "100%",
							alignItems: "center",
							width: "100%"
						}}
					>
						<div
							style={{
								marginTop: 60,
								fontSize: 25,
								fontWeight: "bold"
							}}
						>
							{this.state.title}
						</div>
						{this.renderCurrentWindow()}
					</div>
				</div>
			</div>
		);
	}

	handleChange(event) {
		const target = event.target;
		const name = target.name;
		this.setState({ [name]: target.value });
	}

	async addCourse(courseID) {
		await WebAPIHandler.addCourse(courseID).then(response => {
			if (response.ok) {
				var newCourseArray = this.props.dataReducer.userCourses.slice();
				newCourseArray.push(courseID);
				this.props.setUserCoursesLite(newCourseArray);
			}
		});
	}

	renderCourses() {
		return (
			<div>
				<div style={{ marginTop: 8 }}>
					Check courses, add courses and subscribe to auto updates.
				</div>
				<div style={{ marginTop: 20 }}>
					<div
						style={Object.assign({}, styles.itemContainer, {
							flexGrow: 1,
							height: 44,
							display: "flex",
							marginLeft: "auto",
							marginRight: "auto",
							width: 295,
							alignItems: "center"
						})}
					>
						<img
							style={{ marginRight: 15 }}
							src={require("../../../resources/search.png")}
						/>
						<input
							name="courseFilter"
							value={this.state.courseFilter}
							onChange={this.handleChange}
							style={{
								flexGrow: 1,
								height: "100%",
								margin: 0,
								padding: 0,
								border: 0
							}}
						/>
					</div>
				</div>
				<div style={{ marginTop: 11 }}>Placeholder</div>
				<div
					style={{
						display: "flex",
						flexDirection: "column",
						left: 230,
						bottom: 30,
						right: 40,
						top: 250,
						position: "absolute"
					}}
				>
					<div style={styles.courseContent}>
						{this.props.band.connected ? this.renderBandCourses() : <div />}
					</div>
					<div style={{ marginTop: 24 }}>
						<button
							onClick={() =>
								this.setState({
									currentPage: "Sync",
									title: "Ready to sync"
								})
							}
						>
							Continue
						</button>
					</div>
				</div>
			</div>
		);
	}

	renderSync() {
		return (
			<div>
				<div style={{ marginTop: 12 }}>
					Courses below will be synced to your band.
				</div>
				<div
					style={{
						display: "flex",
						flexDirection: "column",
						left: 230,
						bottom: 30,
						right: 40,
						top: 170,
						position: "absolute"
					}}
				>
					<div
						style={{
							width: "100%",
							display: "flex",
							justifyContent: "space-between",
							backgroundColor: "#5B5B5B",
							height: 30,
							color: "white",
							fontSize: 12,
							fontWeight: "bold"
						}}
					>
						<div style={styles.tableHeader}>Pending</div>
						<div style={styles.tableHeader}>Updates</div>
						<div style={styles.tableHeader}>Requested</div>
					</div>
					<div
						style={{
							width: "100%",
							backgroundColor: "white",
							marginBottom: 12,
							height: "100%",
							overflowY: "scroll"
						}}
					>
						<div
							style={{
								padding: 12,
								display: "flex"
							}}
						>
							<div style={styles.courseEntryColumn}>
								{this.renderPendingCourses()}
							</div>
							<div style={styles.courseEntryColumn}>
								{this.renderUpdateCourses()}
							</div>
							<div
								style={Object.assign({}, styles.courseEntryColumn, {
									border: "none"
								})}
							/>
						</div>
					</div>
					<div
						style={{
							width: "100%",
							display: "flex",
							justifyContent: "flex-end"
						}}
					>
						<button
							style={{ width: 120, backgroundColor: "#26CA85" }}
							onClick={() => {
								this.syncCourses();
							}}
						>
							SYNC
						</button>
						<button
							style={{ width: 120, backgroundColor: "#26CA85" }}
							onClick={() => {
								this.setState({
									currentPage: "Tags",
									courses: { selected: true, complete: true },
									tags: { selected: true, complete: false },
									title: "Setting up your tags"
								});
							}}
						>
							Continue
						</button>
					</div>
				</div>
			</div>
		);
	}

	renderPendingCourses() {
		var _pendingCourses = [];
		for (var course of this.props.persistent.courses) {
			for (var userCourse of this.props.dataReducer.userCourses)
				if (
					course.id == userCourse.courseID &&
					!this.checkCourseExistsOnBand(course)
				) {
					_pendingCourses.push(course);
				}
		}
		const pendingCourses = _pendingCourses.map(course => (
			<div
				key={course.name}
				onClick={() => this.handlePendingCourseSelect(course)}
				style={
					this.state.selectedPendingCourses.indexOf(course) == -1
						? Object.assign({}, styles.courseEntryContainerSelected)
						: Object.assign({}, styles.courseEntryContainer)
				}
			>
				<div key={course.name} style={styles.courseEntry}>
					{course.name}
				</div>
			</div>
		));

		return pendingCourses;
	}

	renderUpdateCourses() {
		var _updateCourses = [];
		for (var course of this.props.persistent.courses)
			if (this.checkCourseUpdateAvailable(course)) {
				_updateCourses.push(course);
			}
		const updateCourses = _updateCourses.map(course => (
			<div
				key={course.name}
				onClick={() => this.handlePendingCourseSelect(course)}
				style={
					this.state.selectedPendingCourses.indexOf(course) == -1
						? Object.assign({}, styles.courseEntryContainerSelected)
						: Object.assign({}, styles.courseEntryContainer)
				}
			>
				<div key={course.name} style={styles.courseEntry}>
					{course.name}
				</div>
			</div>
		));

		return updateCourses;
	}

	renderBandCourses() {
		if (this.props.persistent.courses != []) {
			const courseItems = this.props.persistent.courses
				.filter(course =>
					course.name
						.toLowerCase()
						.includes(this.state.courseFilter.toLowerCase())
				)
				.filter(course => {
					if (
						this.state.countryFilter == "United States" &&
						course.countryName == "United States"
					) {
						// There are null course states in the data for some reason
						if (course.state) {
							if (course.state.includes(this.state.stateFilter)) return true;
						}
					} else if (
						course.countryName
							.toLowerCase()
							.includes(this.state.countryFilter.toLowerCase())
					)
						return true;
				})
				.filter(course => {
					if (!this.state.displayNearby) {
						return true;
					} else if (this.checkCourseExistsNearby(course)) {
						return true;
					} else return false;
				})
				.splice(0, 100)
				.map((course, i) => (
					<div key={i} style={styles.itemContainer}>
						<div style={{ flex: 50, marginRight: 8, textAlign: "left" }}>
							{course.name}
							<br />
							{course.address}
						</div>

						{this.checkCourseUpdateAvailable(course) ? (
							<div style={{ flex: 16, marginRight: 8, width: 120 }}>
								Update available
							</div>
						) : null}
						<div style={{ flex: 16, marginRight: 8, width: 120 }}>
							{this.renderCourseState(course)}
						</div>
						<div style={{ flex: 16, width: 120, marginRight: 8 }}>
							<div style={styles.columnRowContent}>
								<div
									style={{ flexGrow: 1, marginRight: 8, textAlign: "right" }}
								>
									Subscribed
								</div>
								{this.checkCourseExistsOnUser(course) ? (
									<img src={require("../../../resources/check_on.png")} />
								) : (
									<img src={require("../../../resources/emptyBox.png")} />
								)}
							</div>
						</div>
					</div>
				));
			return courseItems;
		}
	}

	checkCourseUpdateAvailable(courseItem) {
		if (this.props.band.connected) {
			if (
				this.checkCourseExistsOnBand(courseItem) &&
				this.checkCourseExistsOnUser(courseItem)
			) {
				if (courseItem.id)
					for (var userCourse of this.props.dataReducer.userCourses) {
						if (courseItem.id == userCourse.courseID) {
							if (
								Date.parse(new Date(userCourse.lastUpdateDate)) >
								Date.parse(this.props.band.timestamp)
							)
								return true;
						}
					}
				else
					for (var userCourse of this.props.dataReducer.userCourses) {
						if (courseItem.courseID == userCourse.courseID) {
							if (
								Date.parse(new Date(userCourse.lastUpdateDate)) >
								Date.parse(this.props.band.timestamp)
							)
								return true;
						}
					}
				return false;
			} else return false;
		} else return false;
	}

	renderSettings() {
		return (
			<div style={{ width: 400, margin: "auto", marginTop: 25 }}>
				<div style={styles.settingsRowContainer}>
					<div style={styles.settingsText}>Hand Settings</div>
					<div
						style={Object.assign({}, styles.settingsText, {
							textAlign: "right"
						})}
					>
						Left handed
					</div>
					<div style={styles.settingsText}>
						<label
							style={{ marginLeft: 20, marginRight: 20 }}
							className="switch"
						>
							<input
								type="checkbox"
								disabled={!this.props.band.connected}
								checked={this.props.band.hand == "RIGHT" ? true : false}
								onChange={() => {
									this.props.band.hand == "RIGHT"
										? SerialHandler.parseUART(SET_HAND, "LEFT")
										: SerialHandler.parseUART(SET_HAND, "RIGHT");
								}}
							/>
							<span className="slider round" />
						</label>
					</div>
					<div style={styles.settingsText}>Right handed</div>
				</div>
				{/* <div style={styles.settingsRowContainer}>
					<div style={styles.settingsText}>Manual Putt Mode</div>
					<div
						style={Object.assign({}, styles.settingsText, {
							textAlign: "right"
						})}
					>
						Off
					</div>
					<div style={styles.settingsText}>
						<label
							style={{ marginLeft: 20, marginRight: 20 }}
							className="switch"
						>
							<input
								type="checkbox"
								disabled={!this.props.band.connected}
								checked={this.props.user.units == 0 ? true : false}
								onChange={() => {
									//TODO: update user hand
									console.log(this.props.user.units);
								}}
							/>
							<span className="slider round" />
						</label>
					</div>
					<div style={styles.settingsText}>On</div>
				</div> */}
				<button
					onClick={() =>
						this.setState({
							currentPage: "Courses",
							settings: { selected: true, complete: true },
							courses: { selected: true, complete: false },
							title:
								this.props.band.courseIDsCount + " active courses on your watch"
						})
					}
				>
					Continue
				</button>
			</div>
		);
	}
}

const styles = {
	aboutText: {
		color: "#8C8C8C",
		fontSize: 14,
		fontWeight: "bold",
		fontFamily: "Roboto",
		textAlign: "left"
	},
	courseEntry: {
		paddingTop: 3,
		paddingLeft: 6,
		color: "black",
		fontWeight: "normal"
	},
	courseEntrySelected: {
		paddingTop: 3,
		paddingLeft: 6,
		color: "white",
		fontWeight: "normal"
	},
	courseEntryContainer: {
		flex: 1,
		display: "flex",
		alignItems: "center",
		backgroundColor: "#E7E7E7",
		height: 40,
		maxHeight: 60,
		paddingLeft: 6,
		marginBottom: 4,
		alignItems: "center",
		marginRight: 6,
		marginBottom: 6,
		boxShadow: "1px 1px 1px #9E9E9E"
	},
	courseEntryContainerSelected: {
		flex: 1,
		backgroundColor: "#26CA85",
		height: 40,
		maxHeight: 60,
		paddingLeft: 6,
		display: "flex",
		alignItems: "center",
		marginBottom: 4,
		alignItems: "center",
		marginRight: 6,
		marginBottom: 6,
		boxShadow: "1px 1px 1px #9E9E9E"
	},
	settingsText: {
		color: "#8C8C8C",
		fontSize: 14,
		fontWeight: "bold",
		fontFamily: "Roboto",
		textAlign: "left",
		flex: 1
	},
	settingsRowContainer: {
		display: "flex",
		width: "100%",
		marginBottom: 30
	},
	itemContainer: {
		display: "flex",
		backgroundColor: "white",
		height: 60,
		alignItems: "center",
		paddingLeft: 18,
		marginBottom: 4,
		marginRight: 18,
		fontSize: 12,
		fontFamily: "Roboto",
		border: "none",
		boxShadow: "1px 1px 1px #9E9E9E"
	},
	columnRowContent: {
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		width: 120
	},
	tableHeader: {
		paddingLeft: 18,
		paddingTop: 8,
		paddingRight: 18
	},
	courseEntryColumn: {
		borderRightStyle: "solid",
		borderWidth: 1,
		paddingRight: 12,
		borderColor: "#8C8C8C",
		flex: 1
	},
	courseContent: {
		overflowY: "scroll",
		flex: "1 1 auto"
	}
};

function mapStateToProps(state, props) {
	return {
		band: state.bandReducer,
		user: state.userReducer,
		dataReducer: state.dataReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

OnBoarding = connect(
	mapStateToProps,
	mapDispatchToProps
)(OnBoarding);
module.exports = OnBoarding;
