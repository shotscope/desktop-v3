import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { shell } from "electron";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; // Import your actions
import { url } from "../../utils/WebAPIHandler";

class SidePanel extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			connect: props.connect,
			settings: props.settings,
			courses: props.courses,
			tags: props.tags,
			myBag: props.myBag
		};
	}

	componentDidMount() {}

	componentWillReceiveProps(nextProps) {
		console.log(nextProps);
		this.setState({
			connect: nextProps.connect,
			settings: nextProps.settings,
			myBag: nextProps.myBag,
			tags: nextProps.tags,
			courses: nextProps.courses
		});
	}

	render() {
		return (
			<div
				style={{
					height: "100vh",
					width: 200,
					top: 0,
					backgroundColor: "#181818",
					color: "white",
					fontFamily: "Roboto"
				}}
			>
				<div style={{ marginLeft: 21, marginRight: 21, marginTop: 30 }}>
					<div style={{ height: 120 }}>
						<img
							src={require("../../../resources/Avatar_PlaceholderLogo.png")}
						/>
						<div
							style={{
								fontSize: 18,
								fontWeight: "bold"
							}}
						>
							{this.props.user.firstName} {this.props.user.lastName}
						</div>
						<div style={{ fontSize: 12 }}>{this.props.user.email}</div>
					</div>
					<hr />
					<div style={{ marginTop: 30 }}>
						<div style={styles.taskContainer}>
							<div
								style={
									this.state.connect.selected
										? styles.selectedTask
										: styles.task
								}
							>
								Connect
							</div>
							<div>
								{this.state.connect.complete ? (
									<img
										style={{ marginLeft: 9 }}
										src={require("../../../resources/GreenCheck.png")}
									/>
								) : null}
							</div>
						</div>
						<div style={styles.taskContainer}>
							<div
								style={
									this.state.settings.selected
										? styles.selectedTask
										: styles.task
								}
							>
								Settings
							</div>
							<div>
								{this.state.settings.complete ? (
									<img
										style={{ marginLeft: 9 }}
										src={require("../../../resources/GreenCheck.png")}
									/>
								) : null}
							</div>
						</div>
						<div style={styles.taskContainer}>
							<div
								style={
									this.state.courses.selected
										? styles.selectedTask
										: styles.task
								}
							>
								Courses
							</div>
							{this.state.courses.complete ? (
								<img
									style={{ marginLeft: 9 }}
									src={require("../../../resources/GreenCheck.png")}
								/>
							) : null}
						</div>
						<div style={styles.taskContainer}>
							<div
								style={
									this.state.tags.selected ? styles.selectedTask : styles.task
								}
							>
								Tags
							</div>
							{this.state.tags.complete ? (
								<img
									style={{ marginLeft: 9 }}
									src={require("../../../resources/GreenCheck.png")}
								/>
							) : null}
						</div>
						<div style={styles.taskContainer}>
							<div
								style={
									this.state.myBag.selected ? styles.selectedTask : styles.task
								}
							>
								My Bag
							</div>
							{this.state.myBag.complete ? (
								<img
									style={{ marginLeft: 9 }}
									src={require("../../../resources/GreenCheck.png")}
								/>
							) : null}
						</div>
					</div>
				</div>

				<div
					style={{
						position: "absolute",
						bottom: 0,
						marginLeft: 28,
						paddingBottom: 28
					}}
				>
					<div style={{ width: 183, height: 80 }}>
						<img
							style={{
								display: "block"
							}}
							src={require("../../../resources/ShotScopeLogo_White.png")}
						/>
					</div>
				</div>
			</div>
		);
	}
}

const styles = {
	taskContainer: {
		display: "flex",
		height: 24,
		alignItems: "center",
		marginBottom: 18
	},
	task: {
		textAlign: "left",
		fontSize: 20
	},
	selectedTask: {
		textAlign: "left",
		fontSize: 20,
		fontWeight: "bold",
		color: "#26CA85"
	}
};

function mapStateToProps(state, props) {
	return {
		user: state.userReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

SidePanel = connect(
	mapStateToProps,
	mapDispatchToProps
)(SidePanel);

module.exports = SidePanel;
