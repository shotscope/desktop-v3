import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { shell } from "electron";
import WebAPIHandler, { url } from "../../utils/WebAPIHandler";
import DropDown from "../utils/DropDown";
import AlertDialog from "../utils/AlertDialog";
import packageJson from "../../package";
import { isHigherFirmwareVersion } from "../../utils/util";
import SerialHandler, {
	SET_HAND,
	PRODUCT_ID_V2
} from "../../utils/SerialHandler";

import * as Actions from "../../actions"; // Import your actions

class MyShotScope extends React.PureComponent {
	constructor(props) {
		super(props);
		var timeZonesArray = [];
		for (var i = -11; i < 13; i++) {
			timeZonesArray.push(i);
		}
		this.state = {
			checked: true,
			timezones: timeZonesArray,
			timezoneSelection: 11,
			renderSyncFailed: false,
			renderSyncSuccess: false,
			renderFirmwareFailed: false,
			renderFirmwareSuccess: false,
			renderInvalidRound: false,
			busy: false,
			actionText: "",
			firmwareUpdating: false,
			syncing: false
		};
		this.changeDropDownSelection = this.changeDropDownSelection.bind(this);
		this.toggleDisplaySyncFailed = this.toggleDisplaySyncFailed.bind(this);
		this.toggleDisplaySyncSuccess = this.toggleDisplaySyncSuccess.bind(this);
		this.toggleDisplaySyncDupe = this.toggleDisplaySyncDupe.bind(this);
		this.toggleDisplayInvalidRound = this.toggleDisplayInvalidRound.bind(this);
		this.syncCompletionCallback = this.syncCompletionCallback.bind(this);
		this.firmwareCompletionCallback = this.firmwareCompletionCallback.bind(
			this
		);
		this.loadFirmware = this.loadFirmware.bind(this);
		this.clearProgress = this.clearProgress.bind(this);
		this.getBandModelFromProductId = this.getBandModelFromProductId.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	async componentDidMount() {
		await WebAPIHandler.getFirmwareVersion();
	}

	renderInfoText() {
		if (!this.props.band.connected) {
			return (
				<p style={{ fontSize: 14 }}>
					Please connect your Shot Scope using the USB cable provided.
				</p>
			);
		} else if (!this.props.performance.performanceEntriesCount > 0) {
			return (
				<p style={{ fontSize: 14 }}>
					No new data to Sync. Head over to your performance dashboard to see
					your stats.
				</p>
			);
		}
	}

	loadFirmware() {
		WebAPIHandler.getFirmware(
			this.props.dataReducer.serverFirmwareVersion
		).then(result => {
			this.setState({ firmwareUpdating: true });
			SerialHandler.loadFirmware(result, this.firmwareCompletionCallback);
		});
	}

	toggleDisplaySyncSuccess() {
		this.setState({ renderSyncSuccess: !this.state.renderSyncSuccess });
	}

	toggleDisplayFirmwareSuccess() {
		this.setState({ renderFirmwareSuccess: !this.state.renderFirmwareSuccess });
	}

	toggleDisplaySyncDupe() {
		this.setState({ renderSyncDupe: !this.state.renderSyncDupe });
	}

	toggleDisplayInvalidRound() {
		this.setState({ renderInvalidRound: !this.state.renderInvalidRound });
	}

	syncCompletionCallback(success) {
		this.setState({ busy: false, syncing: false });
		if (success == true) this.toggleDisplaySyncSuccess();
		else if (success == "CANCEL") this.setState({ syncing: false });
		else if (success == 202) this.toggleDisplaySyncDupe();
		else if (success == "INVALID_ROUND") this.toggleDisplayInvalidRound();
		else this.toggleDisplaySyncFailed();
	}

	firmwareCompletionCallback(success) {
		this.setState({ busy: false, firmwareUpdating: false });
		if (success == true) {
			this.props.setDisableFirmwareAlertForSession(true);
			this.toggleDisplayFirmwareSuccess();
			this.props.setFirmwareAlert(true);
			this.setState({
				renderFirmwareSuccess: true,
				firmwareUpdating: false
			});
		} else if (success == "CANCEL") {
			this.setState({ firmwareUpdating: false });
		} else
			this.setState({ renderFirmwareFailed: true, firmwareUpdating: false });
	}

	renderFirmwareInfo() {
		if (!this.props.band.connected) {
			return;
		} else {
			return (
				<div style={{ marginTop: 56 }}>
					<p style={{ fontSize: 14, fontWeight: "bold", marginBottom: 0 }}>
						Firmware Version: {this.props.band.info.firmwareVersion}
					</p>
					<button
						style={
							!isHigherFirmwareVersion(
								this.props.band.info.firmwareVersion,
								this.props.dataReducer.serverFirmwareVersion
							) || this.props.band.batteryLevel < 25
								? Object.assign({}, { marginTop: 10, width: 156 })
								: Object.assign(
										{},
										{ marginTop: 10, width: 156, cursor: "pointer" }
								  )
						}
						disabled={
							!isHigherFirmwareVersion(
								this.props.band.info.firmwareVersion,
								this.props.dataReducer.serverFirmwareVersion
							) || this.props.band.batteryLevel < 25
						}
						onClick={() => {
							this.setState({ busy: true, actionText: "Updating Firmware" });
							this.loadFirmware();
						}}
					>
						{isHigherFirmwareVersion(
							this.props.band.info.firmwareVersion,
							this.props.dataReducer.serverFirmwareVersion
						)
							? "Update " + this.props.dataReducer.serverFirmwareVersion
							: "No Update Required"}
					</button>
					<p
						style={{
							fontSize: 14,
							fontWeight: "bold",
							color: "red",
							marginTop: 10,
							opacity: 0.7
						}}
					>
						{this.props.band.batteryLevel < 25
							? "Battery level must be at least 25% to sync / update"
							: ""}
					</p>
				</div>
			);
		}
	}

	renderImages() {
		if (this.props.band.connected) {
			return (
				<div style={{ marginLeft: "auto", marginRight: "auto" }}>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/Band.png")}
					/>
				</div>
			);
		} else {
			return (
				<div
					style={{
						marginLeft: "auto",
						width: "100",
						justifyContent: "center",
						marginRight: "auto",
						display: "flex"
					}}
				>
					<img
						style={{ alignSelf: "center", marginRight: 13 }}
						src={require("../../../resources/Band.png")}
					/>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/USB.png")}
					/>
					<img
						style={{ alignSelf: "center" }}
						src={require("../../../resources/Laptop.png")}
					/>
				</div>
			);
		}
	}

	renderCurrentWindow() {
		if (this.props.currentMyShotScopeWindow == "Connect") {
			return this.renderConnect();
		} else if (this.props.currentMyShotScopeWindow == "About") {
			return this.renderAbout();
		} else if (this.props.currentMyShotScopeWindow == "Settings") {
			return this.renderSettings();
		}
	}

	renderAbout() {
		return (
			<div
				style={{
					marginTop: 50,
					display: "flex",
					marginLeft: "auto",
					flexDirection: "column",
					justifyContent: "center"
				}}
			>
				<div
					style={{ display: "flex", width: "100%", justifyContent: "center" }}
				>
					<div style={{ width: 200 }}>
						<p style={styles.aboutText}>
							Firmware Version:{" "}
							{this.props.band.info.firmwareVersion != ""
								? this.props.band.info.firmwareVersion
								: "N/A"}
						</p>
					</div>
					<div style={{ width: 300 }}>
						<p style={styles.aboutText}>
							Serial No:{" "}
							{this.props.band.info.serialNo != ""
								? this.props.band.info.serialNo
								: "N/A"}
						</p>
					</div>
				</div>
				<div
					style={{
						display: "flex",
						width: "100%",
						marginTop: 20,
						justifyContent: "center"
					}}
				>
					<div style={{ width: 200 }}>
						<p style={styles.aboutText}>
							Model:{" "}
							{this.props.band.productId != ""
								? this.getBandModelFromProductId()
								: "N/A"}
						</p>
					</div>
					<div style={{ width: 300 }}>
						<p style={styles.aboutText}>App Version: {packageJson.version}</p>
					</div>
				</div>
			</div>
		);
	}

	getBandModelFromProductId() {
		if (this.props.band.productId == PRODUCT_ID_V2) {
			return "Shot Scope V2";
		}
	}

	changeDropDownSelection(selection) {
		var bandOffset = 0;
		this.setState(
			{
				timezoneSelection: selection
			},
			() => {
				bandOffset = this.state.timezoneSelection * 4 - 4 * 11;
				// SerialHandler.setDateTime(bandOffset);
			}
		);
	}

	async handleChange(event) {
		const target = event.target;
		const name = target.name;
		var userObj = { ...this.props.user, leftHanded: target.value };
		await WebAPIHandler.updateUser(userObj).then(response => {
			WebAPIHandler.getUser().then(response => {
				if (this.props.band.connected) {
					this.props.user.leftHanded
						? SerialHandler.parseUART(SET_HAND, "LEFT")
						: SerialHandler.parseUART(SET_HAND, "RIGHT");
				}
			});
		});
	}

	renderSettings() {
		return (
			<div style={{ width: 400, margin: "auto", marginTop: 25 }}>
				{/* <div style={styles.settingsRowContainer}>
					<div style={styles.settingsText}>Timezone</div>
					<DropDown
						changeDropDownSelection={this.changeDropDownSelection}
						initialSelection={this.state.timezoneSelection}
						data={this.state.timezones}
						displayNumberSign="true"
					/>
				</div> */}
				<div style={styles.settingsRowContainer}>
					<div style={styles.settingsText}>Hand Settings</div>
					<div
						style={{ display: "flex", width: 300, alignItems: "flex-start" }}
					>
						<div style={{ display: "flex", alignItems: "center" }}>
							<label style={{ marginRight: 4, cursor: "pointer" }}>
								<input
									name="hand"
									type="radio"
									style={{
										opacity: 0,
										width: 0,
										height: 0,
										position: "absolute"
									}}
									onClick={this.handleChange}
									value={true}
									defaultChecked={this.props.user.leftHanded ? "checked" : null}
								/>
								{this.props.user.leftHanded ? (
									<img src={require("../../../resources/Toggleon.png")} />
								) : (
									<img src={require("../../../resources/Toggleoff.png")} />
								)}
							</label>
							<div style={{ marginRight: 20 }}>Left</div>
						</div>
						<div style={{ display: "flex", alignItems: "center" }}>
							<label style={{ marginRight: 4, cursor: "pointer" }}>
								<input
									name="hand"
									type="radio"
									style={{
										opacity: 0,
										width: 0,
										height: 0,
										position: "absolute"
									}}
									onClick={this.handleChange}
									value={false}
									defaultChecked={
										!this.props.user.leftHanded ? "checked" : null
									}
								/>
								{!this.props.user.leftHanded ? (
									<img src={require("../../../resources/Toggleon.png")} />
								) : (
									<img src={require("../../../resources/Toggleoff.png")} />
								)}
							</label>
							<div style={{ marginRight: 20 }}>Right</div>
						</div>
					</div>
				</div>
				<div
					style={{
						marginTop: 50,
						display: "flex",
						marginLeft: "auto",
						flexDirection: "column",
						justifyContent: "center"
					}}
				>
					<div
						style={{ display: "flex", width: "100%", justifyContent: "center" }}
					>
						<div style={{ width: 200 }}>
							<p style={styles.aboutText}>
								Firmware Version:{" "}
								{this.props.band.info.firmwareVersion != ""
									? this.props.band.info.firmwareVersion
									: "N/A"}
							</p>
						</div>
						<div style={{ width: 300 }}>
							<p style={styles.aboutText}>
								Serial No:{" "}
								{this.props.band.info.serialNo != ""
									? this.props.band.info.serialNo
									: "N/A"}
							</p>
						</div>
					</div>
					<div
						style={{
							display: "flex",
							width: "100%",
							marginTop: 20,
							justifyContent: "center"
						}}
					>
						<div style={{ width: 200 }}>
							<p style={styles.aboutText}>
								Model:{" "}
								{this.props.band.productId != ""
									? this.getBandModelFromProductId()
									: "N/A"}
							</p>
						</div>
						<div style={{ width: 300 }}>
							<p style={styles.aboutText}>App Version: {packageJson.version}</p>
						</div>
					</div>
				</div>

				{/* <div style={styles.settingsRowContainer}>
					<div style={styles.settingsText}>Manual Putt Mode</div>
					<div
						style={Object.assign({}, styles.settingsText, {
							textAlign: "right"
						})}
					>
						Off
					</div>
					<div style={styles.settingsText}>
						<label
							style={{ marginLeft: 20, marginRight: 20 }}
							className="switch"
						>
							<input
								type="checkbox"
								disabled={!this.props.band.connected}
								checked={this.props.user.units == 0 ? true : false}
								onChange={() => {
									//TODO: update user hand
									console.log(this.props.user.units);
								}}
							/>
							<span className="slider round" />
						</label>
					</div>
					<div style={styles.settingsText}>On</div>
				</div> */}
			</div>
		);
	}

	renderConnect() {
		if (!this.state.busy) {
			return (
				<div>
					<div style={{ height: 16, marginTop: 14 }}>
						{this.renderInfoText()}
					</div>
					<div
						style={{
							width: 332,
							marginTop: 21,
							display: "flex",
							marginLeft: "auto",
							marginRight: "auto",
							justifyContent: "space-between"
						}}
					>
						<button
							style={
								!this.props.band.connected ||
								this.props.performance.performanceEntriesCount == 0
									? Object.assign({}, { width: 126 })
									: Object.assign({}, { width: 126, cursor: "pointer" })
							}
							disabled={
								!this.props.band.connected ||
								this.props.performance.performanceEntriesCount == 0
							}
							onClick={() => {
								this.setState({
									busy: true,
									syncing: true,
									actionText: "Syncing Data"
								});
								SerialHandler.getAllBandEntries(this.syncCompletionCallback);
							}}
						>
							Sync Data
						</button>
						<button
							style={{ cursor: "pointer" }}
							onClick={() => {
								shell.openExternal(url);
							}}
						>
							Performance Dashboard
						</button>
					</div>
					{this.renderFirmwareInfo()}
				</div>
			);
		} else {
			return (
				<div
					style={{
						marginTop: 21,
						display: "flex",
						flexDirection: "column",
						justifyContent: "center"
					}}
				>
					<div className="actionSpinnerImage">
						<img
							style={{ width: 100, height: 100 }}
							src={require("../../../resources/Syncing/Sync.png")}
						/>
					</div>
					<div style={{ marginTop: 20 }}>{this.state.actionText}</div>
				</div>
			);
		}
	}

	renderHeader() {
		if (this.props.currentMyShotScopeWindow == "Connect") {
			if (this.props.band.connected) {
				return "Band Connected";
			} else return "Band Disconnected";
		} else return this.props.currentMyShotScopeWindow;
	}

	toggleDisplaySyncFailed() {
		this.setState({ renderSyncFailed: !this.state.renderSyncFailed });
	}

	toggleDisplayFirmwareFailed() {
		this.setState({ renderFirmwareFailed: !this.state.renderFirmwareFailed });
	}

	clearProgress() {
		SerialHandler.progressCallback({
			percent: 0,
			action: ""
		});
	}

	render() {
		return (
			<div
				style={{
					color: "#8C8C8C",
					fontSize: 18,
					fontWeight: "bold",
					fontFamily: "Roboto"
				}}
			>
				{this.state.firmwareUpdating ? (
					<AlertDialog
						dismissCallback={() => {
							SerialHandler.cancel();
							this.setState({ firmwareUpdating: false });
						}}
						header="Updating the firmware on your band"
						content={
							<div
								style={{
									display: "flex",
									justifyContent: "center"
								}}
							>
								<div className="actionSpinnerImage">
									<img
										style={{ width: 100, height: 100 }}
										src={require("../../../resources/Syncing/Sync.png")}
									/>
								</div>
							</div>
						}
					/>
				) : null}
				{this.state.syncing ? (
					<AlertDialog
						dismissCallback={() => {
							SerialHandler.cancel();
							this.clearProgress();
							this.setState({ syncing: false });
						}}
						header="Syncing round data"
						content={
							<div
								style={{
									display: "flex",
									justifyContent: "center"
								}}
							>
								<div className="actionSpinnerImage">
									<img
										style={{ width: 100, height: 100 }}
										src={require("../../../resources/Syncing/Sync.png")}
									/>
								</div>
							</div>
						}
					/>
				) : null}
				{this.state.renderFirmwareFailed ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplayFirmwareFailed();
						}}
						header="Firmware Update Failed"
						content="Something went wrong transferring data to your Shot Scope band. Please restart your band and try again. If the problem persists contact support."
					/>
				) : null}
				{this.state.renderInvalidRound ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplayInvalidRound();
						}}
						header="Invalid Performance Data"
						content="Not enough data was present on the band to upload your round. The data has been removed from your Shot Scope band. If this should not have happened, contact support."
					/>
				) : null}
				{this.state.renderFirmwareSuccess ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplayFirmwareSuccess();
						}}
						header={
							<div>
								<img
									style={{ marginRight: 8 }}
									src={require("../../../resources/GreenCheck.png")}
								/>
								Firmware Update Complete
							</div>
						}
						content="Please wait for the band to finish updating and unplug and replug your band."
					/>
				) : null}
				{this.state.renderSyncFailed ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplaySyncFailed();
						}}
						header="Sync Failed"
						content="Something went wrong syncing data from your band. Please restart your band, check that you are connected to the internet and try again. If the problem persists contact support."
					/>
				) : null}
				{this.state.renderSyncSuccess ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplaySyncSuccess();
						}}
						header="Sync Complete"
						content="Your performance data has successfully been uploaded to your performance dashboard. You may view your performance data online by clicking on 'Performance Dashboard' in the navigation panel."
					/>
				) : null}
				{this.state.renderSyncDupe ? (
					<AlertDialog
						dismissCallback={() => {
							this.clearProgress();
							this.toggleDisplaySyncDupe();
						}}
						header="Sync Complete"
						content="Your performance data has successfully been uploaded to your performance dashboard, however one or more of your rounds may have already been previously uploaded. You may view your performance data online by clicking on 'Performance Dashboard' in the navigation panel. If one of your rounds are missing please contact support using the link provided."
					/>
				) : null}
				{this.renderHeader()}
				<div
					style={{
						textAlign: "center",
						height: "100%",
						alignItems: "center",
						width: "100%"
					}}
				>
					<div
						style={{
							marginLeft: "auto",
							marginRight: "auto",
							height: 189,
							marginTop: 33
						}}
					>
						{this.renderImages()}
					</div>
					{this.renderCurrentWindow()}
				</div>
			</div>
		);
	}
}

const styles = {
	aboutText: {
		color: "#8C8C8C",
		fontSize: 14,
		fontWeight: "bold",
		fontFamily: "Roboto",
		textAlign: "left"
	},
	settingsText: {
		color: "#8C8C8C",
		fontSize: 14,
		fontWeight: "bold",
		fontFamily: "Roboto",
		textAlign: "left",
		width: 200
	},
	settingsRowContainer: {
		display: "flex",
		width: "100%",
		marginBottom: 30
	}
};

function mapStateToProps(state, props) {
	return {
		band: state.bandReducer,
		user: state.userReducer,
		dataReducer: state.dataReducer,
		performance: state.performanceReducer
	};
}

// Doing this merges our actions into the componentâ€™s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
	return bindActionCreators(Actions, dispatch);
}

MyShotScope = connect(
	mapStateToProps,
	mapDispatchToProps
)(MyShotScope);
module.exports = MyShotScope;
